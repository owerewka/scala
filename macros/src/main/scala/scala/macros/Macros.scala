package scala.macros

/*
https://scalac.io/def-hello-macro-world/

you have to compile this module first before the rest of the application
sbt macros/compile

SBT: Macro Projects Introduction
https://www.scala-sbt.org/1.x/docs/Macro-Projects.html
*/


object Macros {

  import scala.language.experimental.macros
  import scala.reflect.macros.blackbox

  def hello: Unit = macro helloImpl

  def helloImpl(c: blackbox.Context): c.Expr[Unit] = {
    import c.universe._
    c.Expr(q"""println("hello!")""")
  }

}
