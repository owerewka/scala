name := "Scala"
version := "1.0"
val akkaVersion = "2.6.1"
val circeVersion = "0.14.0-M3"

resolvers += "Codahale" at "https://mvnrepository.com/artifact/com.codahale.metrics/metrics-core/"
resolvers += "Typesafe" at "https://repo.typesafe.com/"
resolvers += Resolver.mavenLocal

lazy val commonSettings = Seq(scalaVersion := "2.13.6")

parallelExecution in Test := true

lazy val macros = (project in file("macros"))
  .settings(commonSettings)
  .settings(libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value)

lazy val root = (project in file("."))
  .dependsOn(macros)
  //.aggregate(macros) cannot aggregate macros, they have to be compiled separately
  .settings( //macro setting only
    // include the macro classes and resources in the main jar
    Compile / packageBin / mappings ++= (macros / Compile / packageBin / mappings).value,
    // include the macro sources in the main source jar
    Compile / packageSrc / mappings ++= (macros / Compile / packageSrc / mappings).value
  )
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster-metrics" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster-sharding" % akkaVersion,
      "com.typesafe.akka" %% "akka-cluster-tools" % akkaVersion,
      "com.typesafe.akka" %% "akka-multi-node-testkit" % akkaVersion,
      "com.typesafe.akka" %% "akka-osgi" % akkaVersion,
      "com.typesafe.akka" %% "akka-persistence" % akkaVersion,
      "com.typesafe.akka" %% "akka-persistence-tck" % akkaVersion,
      "com.typesafe.akka" %% "akka-remote" % akkaVersion,
      "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream" % akkaVersion,
      "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion,
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
      "io.dropwizard.metrics" % "metrics-core" % "3.1.2",
      "io.dropwizard.metrics" % "metrics-json" % "3.1.2",
      "io.dropwizard.metrics" % "metrics-jvm" % "3.1.2",
      "com.drewnoakes" % "metadata-extractor" % "2.9.1",
      "joda-time" % "joda-time" % "2.9.7",

      //Scala parallel collections are now a separate module maintained by the community
      "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.0",

      //Redis example dependencies
      "com.github.kstyrc" % "embedded-redis" % "0.6",
      "redis.clients" % "jedis" % "2.9.0",

      //circe
      "io.circe" %% "circe-core" % circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-parser" % circeVersion,

      //sttp
      "com.softwaremill.sttp.client3" %% "core" % "3.0.0",
      "com.softwaremill.sttp.client3" %% "akka-http-backend" % "3.0.0",
      "com.softwaremill.sttp.client3" %% "json4s" % "3.0.0",
      "org.json4s" %% "json4s-native" % "4.0.2",
      "org.json4s" %% "json4s-jackson" % "4.0.2",
      "org.gnieh" %% "diffson-circe" % "4.1.1",

      //Property based testing the FP book
      "org.scalacheck" %% "scalacheck" % "1.15.4",

      //scala test in main
      "org.scalactic" %% "scalactic" % "3.2.10",
      "org.scalatest" %% "scalatest" % "3.2.10",
      "org.scalatest" %% "scalatest-flatspec" % "3.2.10",

      "com.github.scopt" %% "scopt" % "4.0.1"
    )
  )