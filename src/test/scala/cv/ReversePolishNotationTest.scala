package cv

import cv.ReversePolishNotation.eval
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ReversePolishNotationTest extends AnyFlatSpec with Matchers {

  "" should "" in {

    eval("0") shouldBe 0
    eval("1") shouldBe 1
    eval("2 1 +") shouldBe 3
    eval("2 3 +") shouldBe 5
    eval("2 3 + 5 +") shouldBe 10
    eval("2 3 + 5 + 2 +") shouldBe 12

    eval("2 3 *") shouldBe 6
    eval("2 3 * 3 *") shouldBe 18
    eval("2 3 * 3 * 7 *") shouldBe 126

    eval("1 2 + 3 *") shouldBe 9

    eval("1 2 -") shouldBe -1
    eval("1 2 - 3 -") shouldBe -4

  }
}
