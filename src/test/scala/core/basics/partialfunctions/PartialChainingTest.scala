package core.basics.partialfunctions

import core.basics.partialfunctions.Functions.isEvil
import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers


class PartialChainingTest extends AnyFlatSpec with Matchers with TableDrivenPropertyChecks {

  "isEvil" should "match only evil numbers" in {
    forAll(
      Table[Int, Option[String]](
        ("input", "expected output"),
        (0, None),
        (4, None),
        (5, Some("5 is Evil!")),
        (6, None),
        (7, Some("7 is Evil!")),
        (8, None)
      )
    ) { (input, output) =>
      output match {
        case None =>
          isEvil.isDefinedAt(input) should be(false)
        case Some(expectedOutput) =>
          isEvil.isDefinedAt(input) should be(true)
          isEvil(input) shouldBe expectedOutput
      }
    }
  }

}

