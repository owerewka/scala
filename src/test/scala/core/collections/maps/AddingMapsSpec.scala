package core.collections.maps

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class AddingMapsSpec extends AnyFlatSpec with Matchers {

  it should "add maps" in {
    val map1 = Map("key1" -> "value1")
    val map2 = Map("key2" -> "value2")
    map1 ++ map2 shouldBe Map("key1" -> "value1", "key2" -> "value2")
  }

  it should "compress identical maps" in {
    val map1 = Map("key" -> "value")
    val map2 = Map("key" -> "value")
    map1 ++ map2 shouldBe Map("key" -> "value")
  }

  it should "subtract elements" in {
    val map1 = Map("key1" -> "value1", "key2" -> "value2")
    val map2 = Map("key2" -> "value2")
    map1 -- map2.keys shouldBe Map("key1" -> "value1")
  }
}