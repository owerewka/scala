import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TableDrivenSpec extends AnyFlatSpec with Matchers {

  import org.scalatest.prop.TableDrivenPropertyChecks._

  "Test" should "test" in {
    forAll {
      Table[String, Int](
        ("raw", "expected"),
        ("1", 1),
        ("2", 2),
        ("3", 3))
    } { (raw: String, expected: Int) =>
      raw.toInt shouldBe expected
    }
  }

  "Another Test" should "test addition" in {
    forAll {
      Table[Int, Int, Int](
        ("left argument", "right argument", "result"),
        (1, 1, 2),
        (2, 2, 4),
        (4, 4, 8)
      )
    } { (left, right, result) =>
      left + right shouldBe result
    }
  }
}
