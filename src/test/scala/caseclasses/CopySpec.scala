package caseclasses

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class CopySpec extends AnyFlatSpec with Matchers {

  it should "copy class" in {
    val person = Person("Oskar", 39)
    person.toString shouldBe "Person(Oskar,39)"

    val olderPerson = person.copy(age = (person.age + 1).toByte)
    olderPerson.toString shouldBe "Person(Oskar,40)"
  }

}

//val newPersona = existingPersona.copy(sentMessages =
//existingPersona.sentMessages + newMessage)

case class Person(name: String, age: Byte)