CREATE TABLE AIRPORTS
(
    ID       INTEGER PRIMARY KEY AUTOINCREMENT,
    timezone INTEGER,
    offset   INTEGER
);

INSERT INTO AIRPORTS (timezone, offset)
VALUES (0, 0);
INSERT INTO AIRPORTS (timezone, offset)
VALUES (0, 1);
INSERT INTO AIRPORTS (timezone, offset)
VALUES (0, 1);
INSERT INTO AIRPORTS (timezone, offset)
VALUES (0, 2);
INSERT INTO AIRPORTS (timezone, offset)
VALUES (0, 2);
INSERT INTO AIRPORTS (timezone, offset)
VALUES (0, 2);

CREATE TABLE AIRLINES
(
    id     INTEGER PRIMARY KEY AUTOINCREMENT,
    name   VARCHAR(50),
    active CHAR
);

INSERT INTO AIRLINES (name, active)
VALUES ("LH", "Y");
INSERT INTO AIRLINES (name, active)
VALUES ("AA", "Y");
INSERT INTO AIRLINES (name, active)
VALUES ("LO", "N");

CREATE TABLE ROUTES
(
    idx        INTEGER PRIMARY KEY AUTOINCREMENT,
    airline_id INTEGER,
    source_id  INTEGER,
    dest_id    INTEGER,
    FOREIGN KEY (airline_id) REFERENCES AIRLINES (id),
    FOREIGN KEY (source_id) REFERENCES AIRPORTS (id),
    FOREIGN KEY (dest_id) REFERENCES AIRPORTS (id)
);

INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (1, 1, 2);
INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (1, 2, 3);
INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (1, 3, 4);

INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (2, 1, 2);
INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (2, 2, 1);
INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (2, 3, 4);
INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (2, 4, 3);
INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (2, 5, 6);
INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (2, 6, 5);
INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (2, 1, 2);

INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (3, 1, 3);
INSERT INTO ROUTES (airline_id, source_id, dest_id)
VALUES (3, 4, 5);

SELECT AVG(COUNT)
FROM (
         SELECT COUNT(A.name) AS COUNT
         FROM ROUTES AS R
             INNER JOIN AIRPORTS AS P1 ON R.source_id = P1.ID
             INNER JOIN AIRPORTS AS P2 ON R.dest_id = P2.ID
             INNER JOIN AIRLINES AS A ON R.airline_id = A.id
         WHERE A.active = "Y" AND P1.offset=P2.offset
         GROUP BY A.name
     )
