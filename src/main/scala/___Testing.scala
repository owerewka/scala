import cats.data._

object StackWithSate extends App {

  type Stack = List[Int]

  val pop = State[Stack, Int] {
    case x :: xs => (xs, x)
    case Nil => sys.error("stack is empty")
  }

  def push(a: Int): State[Stack, Unit] = State[Stack, Unit] { xs =>
    (a :: xs, ())
  }

  def stackManipulation: State[Stack, Int] =
    for {
      _ <- push(3)
      _ <- pop
      b <- pop
    } yield b

  println {
    stackManipulation.run(List(1, 2, 3)).value
  }
}



