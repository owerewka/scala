import scala.annotation.tailrec
object ___Sliding extends App {

  //  Seq(1, 2, 3, 4, 5).sliding(3).map {
  //    case Seq(l, r, _) => r - l
  //    case Seq(l, r) => r - l
  //  }.foreach(println)

  //  Seq(1, 2, 3, 4, 5).sliding(2).foreach(println)

  def f(list: Seq[Int]): Seq[(Int, Int)] = {
    @tailrec
    def go(list: Seq[Int], acc: Seq[(Int, Int)]): Seq[(Int, Int)] = {
      println(s"l=$list acc=$acc")
      list match {
        case Nil => acc
        case Seq(last) => acc :+ (0, last)
        case l :: tail => go(tail, acc :+ (l - tail.head, l))
      }
    }
    go(list, Nil)
  }

  f(Seq(100, 90, 70, 40, 1)).foreach(println)
}
