import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ___IterableOnce extends AnyFlatSpec with Matchers {

  "IterableOnce" should "have forall" in {

    (1 to 10).forall(_ > 5) shouldBe false
    (1 to 10).forall(_ < 20) shouldBe true

    (1 to 10).exists(_ > 10) shouldBe false
    (1 to 10).exists(_ == 4) shouldBe true

    List(1, 1, 1).scanLeft(0)(_ + _) shouldBe List(0, 1, 2, 3)

  }
}