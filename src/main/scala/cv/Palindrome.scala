package cv

import cv.Palindrome.palindrome
import org.scalatest.OptionValues
import org.scalatest.funspec.AnyFunSpec
import org.scalatest.matchers.should.Matchers
import scala.annotation.tailrec

/**
 * The requirement was to find out if a list is a palindrome without using List.get()
 * If reverse is used, it should also be implemented.
 */
object Palindrome extends App {

  def palindrome(list: List[Int]): Boolean = {
    @tailrec
    def go(l: List[Int], r: List[Int]): Boolean =
      (l, r) match {
        case (Nil, Nil) => true
        case (hl :: tl, hr :: tr) =>
          if (hl == hr) go(tl, tr)
          else false
      }
    go(list, reverse(list))
  }

  private def reverse[T](list: List[T]): List[T] = {
    @tailrec
    def go(l: List[T], acc: List[T]): List[T] =
      l match {
        case Nil => acc
        case h :: t => go(t, acc.prepended(h))
      }
    go(list, Nil)
  }
}

class PalindromeTest extends AnyFunSpec with Matchers with OptionValues {

  describe("palindrome") {
    palindrome(List(1, 2)) shouldBe false
    palindrome(List(1, 2, 3)) shouldBe false
    palindrome(List()) shouldBe true
    palindrome(List(0)) shouldBe true
    palindrome(List(1)) shouldBe true
    palindrome(List(2, 2)) shouldBe true
    palindrome(List(2, 1, 2)) shouldBe true
    palindrome(List(3, 2, 1, 2, 3)) shouldBe true
  }
}