package cv

import scala.annotation.tailrec

object ReversePolishNotation extends App {

  def eval(expression: String): Int = {

    case class OpArity(symbol: String, arity: Int)

    val config = Map(
      "+" -> Some(2),
      "abs" -> Some(1),
      "max" -> None
    )

    //1 2 3 4 max

    @tailrec
    def loop(tokens: List[String], stack: List[Int]): Int = {
      tokens match {
        case h :: Nil =>
          if (stack.isEmpty) h.toInt
          else h match {
            case "+" =>
              val x :: y :: Nil = stack
              x + y
            case "-" =>
              val x :: y :: Nil = stack
              y - x
            case "*" =>
              val x :: y :: Nil = stack
              x * y
          }
        case h :: t =>
          h match {
            case "+" =>
              val x :: y :: s = stack
              loop(t, (x + y) :: s)
            case "-" =>
              val x :: y :: s = stack
              loop(t, (y - x) :: s)
            case "*" =>
              val x :: y :: s = stack
              loop(t, (x * y) :: s)
            case _ =>
              loop(t, h.toInt :: stack)
          }
      }
    }
    loop(expression.split(" ").toList, List[Int]())
  }

  def isOperator(a: String): Boolean = a == "+" || a == "*"
}

object Test extends App {
  println {
    "3".split(" ").toList
  }
}