package core.functions

import util.TypePrinter.printType

object Sample1 extends App {

  def foo = ???
  def bar() = ???

  printType(foo _)
  printType(bar _)
}

object Sample2 extends App {

  def f(): Unit = () => ()
  def g(): Unit = () => {
    ()
  }
  def h: Unit = ()

  printType(f _)
  printType(g _)
  printType(h _)
}

object Sample3 extends App {

  val f = () => ()
  printType(f)
  printType(f())

  val g = f _
  printType(g)
  printType(g())

}