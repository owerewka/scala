package core.functions

import util.TypePrinter.printType


object FunctionValue extends App {

  println("f:")
  val f: Int => Int = (i: Int) => i
  printType(f)
  printType(f(1))

  println("\ng:")
  val g: Function0[Function1[Int, Int]] = f _
  printType(g)
  printType(g())
  printType(g()(1))
  printType(g.apply()(1))

}

object MethodAsValue extends App {

  // doing x _ on a def and function value gives a different type

  def f(i: Int): Int = i
  printType(f _)

  def g = 5
  printType(g)
  printType(g _)

  def h = (i: Int) => i
  printType(h)

}