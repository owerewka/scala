package core.functions

object Application extends App {

  val f: Int => Int = _ * 2

  object g {
    def apply(a: Int): Int = a * 3
  }

  class H {
    def apply(a: Int): Int = a * 4
  }
  val h: H = new H

  println(f(2))
  println(g(2))
  println(h(2))

}
