package core.functions

object ReplaceExpressionWithAnonymousFunction extends App {

  def f(g: Int => String): Unit = {
    println(g(4))
    println(g(5))
    println(g(6))
  }

  f {
    case a => s"$a"
  }

  f {
    case 5 => "for 5"
  }
}
