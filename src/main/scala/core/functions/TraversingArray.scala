package core.functions

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks
import scala.annotation.tailrec

class TraversingArray extends AnyFlatSpec with Matchers with TableDrivenPropertyChecks {

  def isSorted1[A](as: Array[A], ordered: (A, A) => Boolean): Boolean = {
    @tailrec
    def go(position: Int): Boolean =
      if (position + 1 < as.length) {
        if (ordered(as(position), as(position + 1)))
          go(position = position + 1)
        else false
      } else true
    go(position = 0)
  }

  def isSorted2[A](as: Array[A], ordered: (A, A) => Boolean): Boolean =
    as.sliding(2).forall {
      case Array(a, b) => ordered(a, b)
      case _ => true
    }

  forAll {
    Table[(Array[Int], (Int, Int) => Boolean) => Boolean, Array[Int], Boolean](
      ("function", "Array", "is sorted"),
      (isSorted1, Array(), true),
      (isSorted1, Array(1), true),
      (isSorted1, Array(1, 2), true),
      (isSorted1, Array(2, 1), false),
      (isSorted1, Array(1, 2, 3), true),
      (isSorted1, Array(2, 2, 1), false),
      (isSorted2, Array(), true),
      (isSorted2, Array(1), true),
      (isSorted2, Array(1, 2), true),
      (isSorted2, Array(2, 1), false),
      (isSorted2, Array(1, 2, 3), true),
      (isSorted2, Array(2, 2, 1), false)
    )
  } { case (f, ar, expected) =>
    val result = f(ar, _ < _)
    val sorted = if (result) "sorted" else "not sorted"
    println(s"Array(${ar.mkString(",")}) is $sorted")
    result should be(expected)
  }
}