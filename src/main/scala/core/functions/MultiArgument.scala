package core.functions

object MultiArgument extends App {

  val f: Int => (Int => (Int => Int)) =
    (a: Int) =>
      (b: Int) =>
        (c: Int) => a + b + c

  println(f(1)(2)(3))

}
