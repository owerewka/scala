package core.functions

object FunctionSugar extends App {
  val f1: Function2[Int, Int, String] = (x, y) => s"$x + $y"
  val f2: (Int, Int) => String = f1
  println(f1(1, 2))
  println(f2(1, 2))

  val x: Function0[Function2[Int, Int, String]] = f1 _
  val y: () => (Int, Int) => String = f1 _
  println(x()(1, 2))

  //this fails because of erasure, can use a type tag
  println(f1.isInstanceOf[Function2[Int, Int, Exception]])
  println(f1.isInstanceOf[(Int, Int) => Exception])
}

object MultiArgumentListSugar1 extends App {
  def f(a: Int)(b: Int): Int = a * b
  val g: Int => Int => Int = f _
  val h: Function1[Int, Function1[Int, Int]] = g
}

object MultiArgumentSugar extends App {

  val show1: IterableOnce[Int] => (Int => Int) => Unit =
    (range: IterableOnce[Int]) => (f: Int => Int) =>
      range.iterator
        .map(n => s"f($n)=${f(n)}")
        .foreach(println)

  val show2: IterableOnce[Int] => (Int => Int) => Unit =
    range => f => range.iterator
      .map(n => s"f($n)=${f(n)}")
      .foreach(println)

  val show3: Function1[IterableOnce[Int], Function1[Int => Int, Unit]] =
    range => f => range.iterator
      .map(n => s"f($n)=${f(n)}")
      .foreach(println)
}

object NoUsefulInformationInType extends App {
  List(
    (x: Int, y: Int) => x + y,
    (a: String) => println(a),
    (a: Any, b: Any) => (a, b)
  ).foreach(println)
}
