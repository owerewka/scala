package core.functions

import util.TypePrinter.printType

object CurryingSummary extends App {

  def f(a: Int)(b: Int): Long = a + b
  def h(a: Int): Int => Long = {
    "other expressions"
    (b: Int) => a + b
  }
  def g(a: Int, b: Int): Long = a + b

  printType(f _)
  printType(h _)
  printType(g _)

  val x: Function0[Unit] = () => ()
  val y: Function1[Unit, Unit] = _ => ()
  val z: Function2[Unit, Unit, Unit] = (_, _) => ()

}
