package core.functions

object CallByName extends App {

  def f(block: => String) {
    //the call by name parameter will be evaluated twice by the assigments
    val a = block
    val b = block
  }

  f {
    val time: Long = System.currentTimeMillis()
    Thread.sleep(100)
    println(s"Returning value: $time")
    time.toString
  }

}