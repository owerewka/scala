package scala.variance

//noinspection DuplicatedCode
object Covariance extends App {

  class Organism
  class Animal extends Organism
  class Dog extends Animal
  class Cat extends Animal

  class MyList[+A](data: List[A] = Nil) {
    def add[B >: A](a: B): MyList[B] = new MyList[B](a :: data)
  }

  val animals1: MyList[Animal] = new MyList[Dog]
  val animals2: MyList[Animal] = new MyList[Cat]

}

//noinspection DuplicatedCode
object JustTypes extends App {

  class Organism
  class Animal extends Organism
  class Dog extends Animal
  class Cat extends Animal

  class MyList[A](data: List[A] = Nil) {
    def add[B >: A](a: B): MyList[B] = new MyList[B](a :: data)
  }

  new MyList().add("value") //MyList[String]
  new MyList().add(new Cat) //MyList[Cat]
  new MyList().add(new Dog) //MyList[Dog]
  new MyList().add(new Organism) //MyList[Organism]
  new MyList().add(new Cat).add(new Dog) //MyList[Animal]
  new MyList().add(new Cat).add(new Organism) //MyList[Organism]
  new MyList().add(new Cat).add(new Dog).add(new Organism) //MyList[Organism]


  val result: Seq[Int] = for (i <- 1 to 10 if i % 2 == 0)
    yield i

  println(result)
}