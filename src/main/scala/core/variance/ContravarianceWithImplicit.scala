package scala.variance

object ContravarianceWithImplicit extends App {

  trait Animal
  case class Dog(name: String) extends Animal

  trait Printer[-A] {
    def print(a: A): String
  }
  implicit object AnimalPrinter extends Printer[Animal] {
    def print(a: Animal): String = s"[Animal : $a]"
  }

  def print[A](a: A)(implicit p: Printer[A]): String = p.print(a)

  println(print(Dog("Tom")))

}
