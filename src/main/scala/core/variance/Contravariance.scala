package scala.variance

object Contravariance extends App {

  sealed abstract class Animal {
    def name: String
  }
  case class Cat(override val name: String) extends Animal
  case class Dog(override val name: String) extends Animal

  abstract class Printer[-A] {
    def print(value: A): Unit
  }
  class AnimalPrinter extends Printer[Animal] {
    def print(animal: Animal): Unit = println("The animal's name is: " + animal.name)
  }
  class CatPrinter extends Printer[Cat] {
    def print(cat: Cat): Unit = println("The cat's name is: " + cat.name)
  }

  def printMyCat(printer: Printer[Cat], cat: Cat): Unit =
    printer.print(cat)

  val catPrinter: Printer[Cat] = new CatPrinter
  val animalPrinter: Printer[Animal] = new AnimalPrinter

  printMyCat(catPrinter, Cat("Pinky"))

  /*
   the method printMyCat expects the type Printer[Cat]
   with the invariant definition `abstract class Printer[A]`
   the `Printer[Cat]` and `Printer[Animal]` are two unrelated types.

   With contravariant the type `Printer[Cat]` is a supertype of `Printer[Animal]`.
   This means that we can apply `Printer[Animal]` in the place of `Printer[Cat]`
   */
  printMyCat(animalPrinter, Cat("Russet"))
  List
}