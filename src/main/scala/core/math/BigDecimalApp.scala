package scala.math


import java.math.MathContext

import scala.math.BigDecimal.RoundingMode

object BigDecimalApp extends App {

  val decimal = BigDecimal(123456789.01)

  println(decimal)
  println(decimal.setScale(4))
  println(decimal.setScale(3))
  println(decimal.setScale(2))
  println(decimal.setScale(1, RoundingMode.HALF_UP))

  println(BigDecimal(0.04).setScale(1, RoundingMode.HALF_UP))
  println(BigDecimal(0.05).setScale(1, RoundingMode.HALF_UP))
  println(BigDecimal(0.06).setScale(1, RoundingMode.HALF_UP))

  val `0.1` = BigDecimal(0.1)
  val `0.4` = BigDecimal(0.4)
  val `0.5` = BigDecimal(0.5)
  val `0.6` = BigDecimal(0.6)

  println(BigDecimal(0.04))
  println(BigDecimal(0.04).setScale(1, RoundingMode.HALF_UP))
  println(BigDecimal(0.05))
  println(BigDecimal(0.05).setScale(1, RoundingMode.HALF_UP))
  println(BigDecimal(0.06))
  println(BigDecimal(0.06).setScale(1, RoundingMode.HALF_UP))

  println(BigDecimal("0.5"))
  println(BigDecimal("0.5").setScale(0, RoundingMode.HALF_UP))

  println(`0.1` + `0.4`)
  println((`0.1` + `0.4`).setScale(0, RoundingMode.HALF_UP))
  println((`0.1` + `0.4`).setScale(0, RoundingMode.HALF_DOWN))

  println(BigDecimal("0.01234"))
  private val mc = new MathContext(2, java.math.RoundingMode.HALF_UP)
  println(BigDecimal("0010000000000.00001234").round(mc))

  println("sum:")
  val sum = d("0.98") + d("0.01")
  println(sum)


  def d(v: String): BigDecimal = {
    val bigDecimal = BigDecimal(v)
    println(s"   $bigDecimal")
    bigDecimal
  }
}