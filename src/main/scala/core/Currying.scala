package core

import scala.language.postfixOps

object Notation extends App {

  val f: Int => Int => Int => Int => Int => Int = a => b => c => d => e => a + b + c + d + e

  val g = (a: Int) => (b: Int) => (c: Int) => (d: Int) => (e: Int) => a + b + c + d + e

  def h(a: Int)(b: Int)(c: Int)(d: Int)(e: Int): Int = a + b + c + d + e

  def l(a: Int, b: Int, c: Int, d: Int, e: Int): Int = a + b + c + d + e
  val k = l _ curried

  println(f(1)(2)(3)(4)(5))
  println(g(1)(2)(3)(4)(5))
  println(h(1)(2)(3)(4)(5))
  println(k(1)(2)(3)(4)(5))

  println(sameType(f, g))
  println(sameType(f, h _))
  println(sameType(f, k))

  def sameType[T, U](a: T, b: U)(implicit evidence: T <:< U) = true

  class FriendlyGuy
  class Konrad extends FriendlyGuy
  sameType(new Konrad, new FriendlyGuy)
}

object ShortLongHand extends App {

  def shortHandCurried(a: Int)(b: Int): Int = a + b
  def longHandCurried1(a: Int): Int => Int = {
    //this is just a function lateral
    //all this does is to return a function
    //from a function, and this is all about currying
    //with some added sugar for the short notation
    (b: Int) => a + b
  }
  def longHandCurried2(a: Int): Int => Int = {
    b => a + b
  }


  def f(a: Int, b: Int): Int = a + b
  def longHandCurried3(a: Int): (Int => Int) = {
    b => f(a, b)
  }

  val l1: Int => Int = longHandCurried2(10)
  val l2: Int => Int = longHandCurried2(20)
  val l3: Int => Int = longHandCurried2(30)

  println(l1(100))
  println(l2(100))
  println(l3(100))
}

object Other extends App {

  def f(a: Int): Int => Int = {
    println("test")
    (b: Int) => a + b
  }

  def g(a: Int): Int => Int => Int = {
    println("A")
    (b: Int) => {
      println("B")
      (c: Int) =>
        println("C")
        a + b + c
    }
  }

  def h(a: Int): Int => Int => Int = {
    println("A")
    //the return type is just a function and this is how you get currying
    (b: Int) => println("B"); (c: Int) => println("C"); a + b + c
  }

  def p(a: Int): Int => Int => Int = (b: Int) => (c: Int) => a + b + c


  println(h(1)(2)(3))

  val m: (Int, Int, Int) => Int = (a: Int, b: Int, c: Int) => a + b + c
  val curried: Int => Int => Int => Int = m.curried

}
