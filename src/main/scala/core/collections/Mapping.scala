package scala.collections

object Mapping extends App {

  val numbers = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

  println(numbers)

  val map: List[Int] = numbers.map(_ * 2)

  println(map)

}

