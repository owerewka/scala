package scala.collections

object Find extends App {

  val data = Seq(1, 2, 3, 4, 3, 5)

  val found: Option[Int] = data.find(_ == 3)
  println(found)

  data.find { element => element == 3 }

}
