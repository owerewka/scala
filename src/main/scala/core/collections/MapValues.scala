package scala.collections

object MapValues extends App {

  val map: Map[Int, List[Int]] = Map(1 -> List(1, 3, 5), 0 -> List(2, 4))
  println(map.mapValues(_.size))

  println(Map(1 -> List(1, 3, 5), 0 -> List(2, 4)).mapValues(_.size))

  println(Map(1 -> 'a', 2 -> 'b', 3 -> 'c'))
  println(Map(1 -> 'a', 2 -> 'b', 3 -> 'c').mapValues(_.toUpper))

}
