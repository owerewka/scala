package scala.collections.sets

import scala.language.postfixOps


object Sets extends App {

  val low: Set[Int] = 1 to 5 toSet
  val med: Set[Int] = 3 to 7 toSet

  println(low.union(med).toSeq.sorted)
  println(low.intersect(med).toSeq.sorted)
  println(low.diff(med).toSeq.sorted)

}
