package scala.collections


object Zipped extends App {

  val n = Seq(1, 2, 3)
  val l = Seq("a", "b", "c")
  val b = Seq(true, false, true)

  val zipped: Seq[String] = (n, l, b).zipped.map {
    case (n, l, b) => s"$n-$l-$b"
  }

  println(zipped) //List(1-a-true, 2-b-false, 3-c-true)
}

object ZippedDifferentSizes extends App {

  val n = Seq(1)
  val l = Seq("a", "b")
  val b = Seq(true, false, true)

  val zipped: Seq[String] = (n, l, b).zipped.map {
    case (n, l, b) => s"$n-$l-$b"
  }

  println(zipped) //List(1-a-true)
}
