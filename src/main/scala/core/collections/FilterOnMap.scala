package scala.collections

object FilterOnMap extends App {

  import scala.collection.immutable.Map

  val map = Map("A" -> 1, "B" -> 2, "C" -> 3, "D" -> 4, "E" -> 5, "F" -> 6)

  println(map)

  println(map
    .filter(v => v._1 == "A" || v._1 == "B")
    .map(v => v._1 + "_suffix" -> v._2))

  println(map.filter {
    case (key, value) => key == 1 || value == "b"
  })

}