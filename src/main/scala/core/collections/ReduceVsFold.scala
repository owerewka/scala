package scala.collections

object ReduceVsFold extends App {

  //this is the same
  println(Seq(1, 2, 3).reduce(_ + _))
  println(Seq(1, 2, 3).fold(0)(_ + _))

  //a difference is with empty collection
  println(Seq.empty.fold(0)(_ + _))

  try {
    Seq.empty[Int].reduce(_ + _)
  } catch {
    case e: UnsupportedOperationException => println(e)
  }
}
