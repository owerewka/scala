package scala.collections

object ForEach extends App {

  val numbers = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

  println(numbers)

  //for each returns a Unit
  println(numbers.foreach(x => print(x * 2 + " ")))
  val foreach: Any = numbers.foreach(_ * 2)

  println(foreach)

}

object ForEachWithTuples extends App {
  //given
  val tuples = Seq[(Int, String)]((1, "a"), (2, "b"), (3, "c"), (4, "d"))
  //then
  tuples.foreach { case (key: Int, value: String) =>
    println(s"Key: $key, Value: $value")
  }
}