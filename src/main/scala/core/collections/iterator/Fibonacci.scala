package core.collections.iterator

object Fibonacci extends App {

  val a: Iterator[Int] = Iterator.unfold((0, 1)) {
    case (a, b) => Some(a, (b, a + b))
  }

  a.take(30).foreach(println)

}
