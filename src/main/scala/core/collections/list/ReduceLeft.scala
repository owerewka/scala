package scala.collections.list

object ReduceLeft extends App {

  val list = List("a", "b", "c")
  println(list.reduceLeft(_ + _))
  println(list.reduceRight(_ + _))
  println(list.reduce(_ + _))

  try {
    List.empty[Int].reduce(_ + _)
  } catch {
    case e: UnsupportedOperationException => println(e)
  }

}