package scala.collections

object Collect extends App {

  //Doesnt match Option[Int] this will be filtered out
  val convertFn: PartialFunction[Any, Int] = {
    case i: Int => i;
    case s: String => s.toInt;
    case Some(s: String) => s.toInt
  }

  //Some(4) will not be matched by the function above
  //and therefore eliminated from the result list
  val data: Seq[Any] = List(0, 1, "2", "3", Some(4), Some("5"))
  println(data)
  println(data.collect(convertFn))

}

object CollectFirst extends App {

  val data: Seq[Int] = Seq(10, 12, 32)
  val predicate: Int => Boolean = (i: Int) => i > 11
  val map = (i: Int) => i.toString

  val result: Option[String] = data.collectFirst {
    case i if predicate(i) => map(i)
  }

  println(result)

}

object FilterAndMap extends App {

  Seq("ala", " ", "ma", " ", "kota").collect {
    case v if v.size > 2 => v.toUpperCase
  }.foreach(println)

}