package scala.collections

object GroupByApp extends App {

  case class Charge(clientId: Long, amount: Double) {
    def combine(other: Charge): Charge = {
      other match {
        case Charge(cc, otherAmount) => Charge(cc, amount + otherAmount)
        case _ => throw new UnsupportedOperationException(
          s"Cannot combine $this with $other");
      }
    }
  }

  val grouped = Seq(
    Charge(1, 1.11), Charge(1, 1.22), Charge(1, 1.33),
    Charge(2, 2.22),
    Charge(3, 3.33), Charge(3, 3.44)
  ).groupBy(_.clientId)

  println(grouped)
  println(grouped.values.map(_.reduce(_.combine(_))))
  println(grouped.values.map(_.reduce { (c1, c2) =>
    Charge(c1.clientId, c1.amount + c2.amount)
  }))

}

object GroupValuesApp extends App {

  val grouped: Map[Int, IndexedSeq[Int]] = (1 to 10).groupBy(_ % 3)
  println(grouped)
  println(grouped.values.map(_.sum))

}