package scala.collections.queues

import java.util.PriorityQueue
import java.util.function.Consumer

object PriorityQueueSimpleApp extends App {

  val queue = new PriorityQueue[String]()

  queue.add("ala")
  queue.add("ma")
  queue.add("kota")
  queue.add("ale")
  queue.add("kot")
  queue.add("ma")
  queue.add("Alę")

  queue.forEach(new Consumer[String] {
    override def accept(t: String) = println(t)
  })

}
