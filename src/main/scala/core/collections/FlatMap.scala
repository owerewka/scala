package core.collections

object FlatMap extends App {
  /*
  flatMap works by applying a function that returns a
  sequence for each element in the list, and flattening
  the results into the original list.
   */
  println(List(1, 2, 3).flatMap(v => List(v, v, v)))
  println(List(1, 2, 3).flatMap(v => List.fill(3)(v)))
  println(List(1, 2).flatMap(a => List(a, a)))
}
