package core.collections

object Folds extends App {

  //get the total length of all Strings
  println(List("ala", "ma", "kota").foldLeft(0)(_ + _.length))

}

object FoldLeftAndRight extends App {

  implicit class StringOps(val s: String) {
    def @@(o: String): String = s"($s).->$o"
  }

  println(List("a", "b", "c").foldLeft("Start")((acc, a) => acc @@ a))
  println(List("a", "b", "c").foldLeft("Start")(_ @@ _))

  println(List("a", "b", "c").foldRight("Start")((a, acc) => acc @@ a))
  //if the type A == B you can mess this up like this
  //because the operator @@ will be applied to the value instead of to the accumulator
  println(List("a", "b", "c").foldRight("Start")(_ @@ _))

}