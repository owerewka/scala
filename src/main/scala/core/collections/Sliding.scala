package scala.collections

object Sliding extends App {

  Seq("a", "b", "c", "d", "e").sliding(2).map {
    case Seq(l, r) => l + r
  }.foreach(println)


  Seq("a", "b", "c", "d", "e").sliding(3).map {
    case Seq(l, m, r) => l + m + r
  }.foreach(println)

  (1 to 10)
    .sliding(3, 3)
    .map((s: Seq[Int]) => s"${s.mkString(",")}=${s.reduce(_ + _)}")
    .foreach(println)

  Seq(1, 2, 3, 4, 5).sliding(2).map(_.sum).foreach(println)
}

