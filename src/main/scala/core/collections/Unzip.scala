package scala.collections

object Unzip extends App {

  val zipped: Seq[(Int, String)] = Seq((1, "a"), (2, "b"), (3, "c"))
  val unzipped: (Seq[Int], Seq[String]) = zipped.unzip
  println(zipped)
  println(unzipped)
  println(unzipped._1)
  println(unzipped._2)

}
