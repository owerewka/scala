package scala.collections

object Filter extends App {

  val numbers = List(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

  println(numbers)

  println(numbers.filter(x => (x % 2) == 0))

}
