package core.collections.mutable

import scala.collection.mutable

object IntTest extends App {

  import Ordering.Int
  val queue = new mutable.PriorityQueue[Int]()

  queue.enqueue(1)
  queue.enqueue(3)
  queue.enqueue(6)
  queue.enqueue(2)
  queue.enqueue(9)
  queue.enqueue(4)

  //ah those mutable objects are married to while loops
  while (queue.nonEmpty) {
    println {
      queue.dequeue()
    }
  }

}

object TupleTest extends App {

  val f: ((Int, Int)) => Int = t => math.abs(t._1 - t._2)
  implicit val value: Ordering[(Int, Int)] = Ordering.by(f)

  val queue = new mutable.PriorityQueue[(Int, Int)]()

  queue.enqueue(2 -> 6)
  queue.enqueue(2 -> 3)
  queue.enqueue(2 -> 4)
  queue.enqueue(1 -> 1)
  queue.enqueue(1 -> 2)
  queue.enqueue(1 -> 4)
  queue.enqueue(1 -> 0)
  queue.enqueue(1 -> 3)

  queue.dequeueAll.foreach(println)

}