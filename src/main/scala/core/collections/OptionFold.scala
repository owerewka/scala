package scala.collections

object OptionFold extends App {

  println(Some("Ala").fold("Ali nie ma w domu")(name => s"Witam jestem $name"))
  println(None.fold("Ali nie ma w domu")(name => s"Witam jestem $name"))

}
