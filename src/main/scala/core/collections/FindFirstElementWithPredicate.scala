package scala.collections

object FindFirstElementWithPredicate extends App {

  val list = List("A", "B", "C", "D", "E")

  val result: Option[String] = list.collectFirst {
    case a if a == "D" => "D"
  }

  println(result)

}
