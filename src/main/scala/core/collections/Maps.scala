package scala.collections

object MapTake extends App {
  val map1: Map[Int, String] = Map(1 -> "a", 2 -> "b", 3 -> "c", 4 -> "d")
  val map2: Map[Int, String] = map1.take(2)
  println(map1)
  println(map2)
}

object MapFilter extends App {
  val map: Map[Int, String] = Map(1 -> "a", 2 -> "b", 3 -> "c")
  println(map.filterKeys((key: Int) => key == 2)) //Map(2 -> b)
  println(map.filter((t: (Int, String)) => t._1 == 2 || t._2 == "a")) //Map(1 -> a, 2 -> b)
}

object AddingMaps extends App {
  val map1: Map[String, String] = Map("key1" -> "value1")
  val map2: Map[String, String] = Map("key2" -> "value2")
  println(map1) //Map(key1 -> value1)
  println(map2) //Map(key2 -> value2)
  println(map1 ++ map2) //Map(key1 -> value1, key2 -> value2)
}

object ConvertingMapToListOfTuples extends App {
  val map: Map[Int, String] = Map(1 -> "a", 2 -> "b", 3 -> "c")
  val seq: Seq[(Int, String)] = map.toSeq
  println(map) //Map(1 -> a, 2 -> b, 3 -> c)
  println(seq) //Vector((1,a), (2,b), (3,c))
}

object ImmutableMapsWithVar extends App {
  var capital: scala.collection.immutable.Map[String, String] =
    Map("US" -> "Washington",
      "France" -> "Paris")
  println(capital) //Map(US -> Washington, France -> Paris)
  capital += ("Japan" -> "Tokyo")
  println(capital) //Map(US -> Washington, France -> Paris, Japan -> Tokyo)
  capital -= "Japan"
  println(capital) //Map(US -> Washington, France -> Paris)
  println(capital("France")) //Paris
}

object WithDefaultValue extends App {

  import scala.jdk.CollectionConverters._

  val jMap = new java.util.concurrent.ConcurrentHashMap[String, String]().asScala
  println(jMap.get("key")) //None
  println(jMap.getOrElseUpdate("key", "default")) // "default"
  println(jMap.get("key")) //Some("default")
}

object ReplaceValue extends App {
  val map = Map(1 -> "a", 2 -> "b", 3 -> "c")
  println(map.updated(2, "B")) //Map(1 -> a, 2 -> B, 3 -> c)
}

object ReplaceValueFunctional extends App {

  val map: Map[Int, String] = Map(1 -> "a", 2 -> "b", 3 -> "c")

  //for scala 2.12 (if you wan to work with Play...)
  def replace[K, V](map: Map[K, V], key: K, updateF: V => V): Map[K, V] =
    map.get(key).map(updateF) match {
      case Some(newValue) => map.updated(key, newValue)
      case None => map
    }

  println(replace[Int, String](map, 2, _.toUpperCase))
  //Map(1 -> a, 2 -> B, 3 -> c)

  println(map.updatedWith(2)(_.map(_.toUpperCase)))
}