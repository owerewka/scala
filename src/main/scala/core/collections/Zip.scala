package scala.collections

object ZipTwoLists extends App {

  val list1 = List("a", "b", "c")
  val list2 = List(1, 2, 3, 4, 5)

  println(list2.zip(list1))
  println(list1.zip(list2))

}

object ZipListWithStream extends App {

  val list = List("a", "b", "c")
  println(list.zip(LazyList from 1))
  println(list.zip(LazyList from 1000))

}

object ZipAll extends App {

  val strings = List("a", "b", "c", "d", "e")
  val numbers = List(100, 200)
  println(strings.zipAll(numbers, -1, "missing in strings"))
  println(numbers.zipAll(strings, -1, "missing in strings"))

}

object ZipWithIndex1 extends App {

  List("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    .zipWithIndex.foreach {
    case (day, count) => println(s"$count is $day")
  }

}

object ZipWithIndex2 extends App {

  List("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    .zipWithIndex
    .map {
      case (day, count) => s"$count is $day"
    }
    .foreach(println)

}

object ZipWithIndex3 extends App {

  val result = List("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
    .zipWithIndex.map(t => s"${t._1} is ${t._2}")
  println(result)

}