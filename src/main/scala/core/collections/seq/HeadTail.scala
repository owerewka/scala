package core.collections.seq

object HeadTail extends App {

  val seq = Seq(1, 2, 3)

  println(seq.head)
  println(seq.tail)

  println(seq.init)
  println(seq.last)

}