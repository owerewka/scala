package scala.collections.seq

object VariableArgumentList extends App {

  println(" - 1 - ")
  Seq(1, 2, 3) match {
    case Seq(first, _*) =>
      println(s"first: $first")
  }

  println(" - 2 - ")
  Seq(1, 2, 3) match {
    case Seq(first, rest@_*) =>
      println(s"first: $first")
      println(s"rest: $rest")
  }

  println(" - 3 - ")
  Seq(1, 2, 3) match {
    case head +: tail => println(s"$head +: $tail")
  }

  println(" - 4 - ")
  Seq(1, 2, 3) match {
    case init :+ last => println(s"$init :+ $last")
  }

  Seq(1, 2, 3).+:(1)
}
