package core.collections.seq

import scala.collection.GenSeq
import scala.collection.parallel.ParSeq

/**
 * Created by Oskar on 06-08-2015.
 */
object SeqTest extends App {

  case class Person(first: String, last: String, mi: String)

  val fred = Person("Fred", "Flintstone", "J")
  val wilma = Person("Wilma", "Flintstone", "A")
  val barney = Person("Barney", "Rubble", "J")
  val betty = Person("Betty", "Rubble", "A")

  val peeps = Seq(fred, wilma, barney, betty)

  > > peeps

  > > peeps.filter(_.last == "Flintstone").map(_.first)
  > > peeps.filter(person => person.last == "Flintstone").map(person => person.first)

  // get the head element
  > > peeps.head
}


object > {
  def >(any: Any) = println(any)

  val a = collection.Seq
  val b = collection.immutable.Seq
  val c = collection.mutable.Seq

}

