package scala.collections.seq

object PatternMatchLastElement extends App {

  Seq() match {
    case _ :+ last => println(last)
    case _ => println("none")
  }
  Seq(1) match {
    case _ :+ last => println(last)
    case _ => println("none")
  }
  Seq(1, 2) match {
    case _ :+ last => println(last)
    case _ => println("none")
  }
  Seq(1, 2, 3) match {
    case _ :+ last => println(last)
    case _ => println("none")
  }

}
