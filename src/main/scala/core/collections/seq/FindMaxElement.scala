package core.collections.seq

object FindMaxElement extends App {

  println(Seq(1, 2, 3).max)
  println(Seq[Int]().maxOption.getOrElse(1))

}
