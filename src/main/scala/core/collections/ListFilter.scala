package scala.collections

object ListFilter extends App {

  case class Account(name: String)

  case class Wrapper[Account](account: Account)

  val list = List(Account("1"), Account("2"), Account("3"))

  val result = list.map((account: Account) => {
    new Wrapper(account)
  })

  result.foreach(println)
}
