package scala.collections

object GroupByInts extends App {

  val data: List[Int] = 1 :: 2 :: 3 :: 1 :: 2 :: 3 :: Nil
  println(data)

  val byIdentity: Map[Int, List[Int]] = data.groupBy(identity)
  println(byIdentity)

  val byParity1: Map[Int, List[Int]] = data.groupBy((value: Int) => value % 2)
  val byParity2: Map[Int, List[Int]] = data.groupBy(_ % 3)
  println(byParity1)
  println(byParity2)
}

object GroupByStrings extends App {

  val data: List[String] = List("Ala", "Ula", "Qula", "Bula", "Pula", "Alula")
  println(data)

  val byFirstChar: Map[Char, List[String]] = data.groupBy(_ (0))
  println(byFirstChar)

  val byLength: Map[Int, List[String]] = data.groupBy(_.length)
  println(byLength)

  val byLengthParity: Map[Int, List[String]] = data.groupBy(_.length % 2)
  println(byLengthParity)
  val values: Iterable[List[String]] = byLengthParity.values
  val list: List[List[String]] = values.to(List)
  println(list)

  private val byHasU = data.groupBy(_.contains("u")).values.to(List)
  println(byHasU)
}