package core.forexpression

object CustomClass extends App {

  case class Numbers(v: Int*) {
    def map[B](f: Int => B): Seq[B] = v.map(f)
    def withFilter(p: Int => Boolean): Numbers = Numbers(v.filter(p): _*)
  }

  val res = for {
    a <- 1 to 2
    b <- Numbers(1, 2, 3) if b == 3
  } yield (a, b)

  println(res.mkString("\n"))

}
