package core.forexpression

object ForLoop extends App {

  for (i <- 1 to 5 if i != 4) {
    println(i)
  }

  for (i <- 1 to 5) {
    println(i)
  }

}
