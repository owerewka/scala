package core.forexpression

import util.Test

object Guards extends App {

  case class User(name: String, age: Int)

  val userBase = List(
    User("Travis", 28),
    User("Kelly", 33),
    User("Jennifer", 44),
    User("Dennis", 23))

  val twentySomethings: Seq[String] =
    for (user <- userBase if user.age >= 20 && user.age < 30)
      yield user.name // i.e. add this to a list

  twentySomethings.foreach(println)

}

object FlatMaps extends App {

  val result: Seq[String] =
    (1 to 2).flatMap { a =>
      Seq("a", "b").map { b =>
        s"$a-$b"
      }
    }

  result.foreach(println)
}

object ForComprehensions extends App {
  (1 to 100)
    .withFilter(i => i % 2 == 0)
    .map(i => s"$i is odd")

  for (i <- 1 to 3) yield i

  for (i <- 10 to 12; j <- 20 to 22; k <- 30 to 32) {
    println(s"$i $j $k")
  }

  for {
    i <- 10 to 12
    j <- 20 to 22
    k <- 30 to 32
  } {
    println(s"$i $j $k")
  }
}

class ForWithUnit extends Test {

  "For comprehension" should "return None" in {
    val result: Option[Unit] =
      for {
        _ <- None
      } yield ()
    result shouldBe None
  }

  it should "return Some" in {
    val result: Option[Unit] =
      for {
        _ <- Some(1)
      } yield ()
    result shouldBe Some(())
  }
}