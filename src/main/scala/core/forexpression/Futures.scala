package core.forexpression

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}


object Futures extends App {

  val job = (n: Int) => Future {
    Thread.sleep(100)
    println(n)
    n + 1
  }

  val eventualInt: Future[Int] = (
    for {
      f1 <- job(1)
      f2 <- job(f1)
      f3 <- job(f2).map(_ + 1000)
      f4 <- job(f3)
      f5 <- job(f4)
    } yield f5)
    .andThen {
      case Success(result) => println(s"Finished with result: $result")
      case Failure(exception) => println(s"Failure ${exception.getMessage}")
    }

  Await.ready(eventualInt, 1.minute)

}