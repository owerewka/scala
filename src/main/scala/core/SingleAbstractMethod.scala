package core

object SingleAbstractMethod extends App {

  trait Printer[A] {
    def print(a: A): String
  }

  val pi: Printer[Int] = _.toString
  val pb: Printer[Boolean] = _.toString

}
