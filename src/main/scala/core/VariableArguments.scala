package core

object Variable extends App {

  def f(a: Int*): Unit = {
    val _: Seq[Int] = a
    println(a)
  }

  f()
  f(1)
  f(1, 2)

}

object VariableWithGenerics extends App {

  def f[A](x: (A => Boolean)*): Unit = {
    val _: Seq[A => Boolean] = x
  }

  f()
  f[Int](_ == 0)
  f[Int](_ > 1, _ < 5)

}
