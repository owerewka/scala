package core

object AbstractTypeMembers extends App {

  trait Buffer {
    type T
    val element: T
  }

  class IntBuffer extends Buffer {
    override type T = Int
    override val element: T = 30
  }

  class StringBuffer extends Buffer {
    override type T = String
    override val element: T = "string"
  }

  println(new IntBuffer().element)
  println(new StringBuffer().element)
}
