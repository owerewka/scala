package core.implicits

import scala.language.implicitConversions

case class Value(v: String)

object ValueImplicits {
  implicit def fromString(s: String): Value = Value(s)
  implicit def fromDouble(d: Double): Value = Value(d.toString)
  implicit def fromBoolean(b: Boolean): Value = Value(b.toString)
}

object ImplicitConversionsApp extends App {

  import ValueImplicits._
  val s: Value = "Text"
  val d: Value = 1.1
  val b: Value = true
  println(s)
  println(d)
  println(b)

}


