package scala.implicits

object ExplicitNonImplicitParameters extends App {

  val name = "Oskar"
  val employed = true
  method(name, employed)

  def method(implicit name: String, employed: Boolean): Unit =
    println(s"name=$name, employed=$employed")
}

object ExplicitImplicitParameters extends App {

  implicit val name: String = "Oskar"
  implicit val employed: Boolean = true
  method(name, employed)

  def method(implicit name: String, employed: Boolean): Unit =
    println(s"name=$name, employed=$employed")
}

/* This doesn't work:
  Unspecified value parameters: name: String, employed: Boolean
  val name = "Oskar"
  val employed = true
  method()
 */

object ImplicitParameters extends App {

  implicit val name: String = "Oskar"
  implicit val employed: Boolean = true

  method

  def method(implicit name: String, employed: Boolean): Unit =
    println(s"name=$name, employed=$employed")
}


object ImplicitParametersScopes extends App {

  {
    implicit val value: String = "External"

    {
      implicit val value: String = "Internal"
      method
    }
  }

  def method(implicit value: String): Unit = println(s"value=$value")

}

object IntellijIsNotHelping extends App {

  implicit val string1: String = "1"
  implicit val string2: String = "2"

  //method

  /*
    Idea:
      No implicits found for parameters value1: String, value2: String
    Idea (after clicking on error parameter list):
      Ambitious implicits for parameter value1:String
      Ambitious implicits for parameter value2:String

    SBT
      [error] ...\scala\implicits\Parameters.scala:62:3: ambiguous implicit values:
      [error]  both value string1 in object IntellijIsNotHelping of type => String
      [error]  and value string2 in object IntellijIsNotHelping of type => String
      [error]  match expected type String
      [error]   method
      [error]   ^
   */

  def method(implicit value1: String, value2: String) = s"$value1 $value2"
}
