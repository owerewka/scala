package core

import util.Test

class MyProduct(i: Int, s: String) extends Product2[Int, String] {
  override def _1: Int = i
  override def _2: String = s
  override def canEqual(that: Any): Boolean =
    that.isInstanceOf[MyProduct]
  override def productElementName(n: Int): String =
    n match {
      case 0 => "i"
      case 1 => "s"
      case _ => super.productElementName(n)
    }
}
object MyProduct {
  def unapply(arg: MyProduct): Option[(Int, String)] =
    Some(arg._1, arg._2)
}

object ProductTest extends App {

  val p = new MyProduct(666, "aaa")

  println(p._1)
  println(p._2)
  println(p.canEqual("String"))
  println(p.productArity)
  println(p.productPrefix)
  println(p.productIterator.toSeq)
  println(p.productElementNames.toSeq)

  val y: MyProduct = p.ensuring {
    _ match {
      case MyProduct(i, s) => i == 666 && s == "aab"
    }
  }
}

object ArityCalculation extends App {

  sealed trait Animal extends Product with Serializable
  case class Cat(name: String) extends Animal
  case class Dog(name: String) extends Animal
  case class Lineage(data: String)
  case class PurebredDog(name: String, lineage: Lineage) extends Animal

  val pDog = PurebredDog("Ben", Lineage("Father/Mother"))

  //it doesn't count the arity of the constituents
  println(pDog.productArity)
}

class InstanceOf extends Test {

  val p: Product2[Int, String] = new Product2[Int, String] {
    override def _1: Int = ???
    override def _2: String = ???
    override def canEqual(that: Any): Boolean = ???
  }

  p.isInstanceOf[(Int, String)] shouldBe false
  (1, "a").isInstanceOf[Product2[Int, String]] shouldBe true
}