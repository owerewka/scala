package core

import core.Extractors.EMail

object Extractors extends App {

  object EMail {
    def unapply(str: String): Option[(String, String)] = {
      str split "@" match {
        case Array(user, domain) => Some(user, domain)
        case _ => None
      }
    }
  }

  "oskar@spaces.pl" match {
    case EMail(user, domain) => println(s"User: $user, Domain: $domain")
    case _ => println("Not an EMail!")
  }

  "basia@firma.pl" :: "kasia@gmail.com" :: Nil match {
    case EMail(u1, d1) :: EMail(u2, d2) :: Nil =>
      println(s"User: $u1, Domain: $d1")
      println(s"User: $u2, Domain: $d2")
  }
}

object CombiningExtractors extends App {

  object Country {
    def unapply(s: String): Option[String] = {
      if (s.contains(".")) {
        val country = s.substring(s.lastIndexOf(".") + 1, s.length)
        if (country.length == 2) Some(country)
        else None
      }
      else None
    }
  }

  object UpperCase {
    def unapply(s: String): Boolean = s.toUpperCase == s
  }

  def userTwiceUpper(s: String): String = s match {
    case EMail(user@UpperCase(), domain@Country("pl")) =>
      s"match: $s, user: $user, domain: $domain"
    case _ => "no match"
  }

  println(userTwiceUpper("INFO@job.pl"))
  println(userTwiceUpper("BIURO@firma.pl"))
  println(userTwiceUpper("BIURO@firma.de"))
  println(userTwiceUpper("spam@hotmail.pl"))

}





















