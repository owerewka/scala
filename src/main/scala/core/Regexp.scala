package core

import scala.util.matching.Regex

object Regexp extends App {
  val source: String = "A 10 B 20 C 30"
  val numberPattern: Regex = raw"[0-9]+".r
  numberPattern.findAllIn(source).foreach(println)
}

object RegexpOneLiner extends App {
  raw"ala".r.findAllIn("ala ma kota").toList
}