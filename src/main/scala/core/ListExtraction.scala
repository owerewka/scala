package core

object ListExtraction extends App {

  val myList = Seq(1, 2, 3)

  //case class ::[+A](head: A, var next: List[A]) extends List[A]
  //it uses the case class extractor with the notation illustrated below
  val head :: tail = myList

  println(head)
  println(tail)
}

object TwoArgumentExtraction extends App {

  case class V(a: String, b: String)
  val v = V("a", "b")

  val a V b = v
  println(a)
  println(b)

  val V(c, d) = v
  println(c)
  println(d)

}