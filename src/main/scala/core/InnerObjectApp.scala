package scala

object InnerObjectApp extends App {

  class Outer {
    object Inner {
    }
  }

  val o: Outer = new Outer
  val i: o.Inner.type = o.Inner

}
