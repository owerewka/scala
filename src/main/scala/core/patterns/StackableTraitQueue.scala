package core.patterns

import scala.collection.mutable.ArrayBuffer

/**
 * Created by oskar on 20.09.15.
 */
object Run extends App {
  val q = new ArrayBufferIntQueue with Incrementing with Doubling
  q.put(1)
  q.put(10)
  println(q.get())
  println(q.get())
}

trait IntQueue {
  def get(): Int
  def put(x: Int)
}

class ArrayBufferIntQueue extends IntQueue {
  private val buf = new ArrayBuffer[Int]
  override def get(): Int = buf.remove(0)
  override def put(x: Int): Unit = buf += x
}


trait Doubling extends IntQueue {
  abstract override def put(x: Int): Unit = super.put(2 * x)
}

trait Incrementing extends IntQueue {
  abstract override def put(x: Int): Unit = super.put(x + 1)
}