package core

import java.lang.System.currentTimeMillis
import scala.annotation.tailrec
import scala.collection.parallel.immutable.ParVector
import scala.math._

object ParallelCollections extends App {

  val t1 = currentTimeMillis()
  println {
    //replace ParVector with List for sequential
    ParVector(1 to 10000: _*).map((i: Int) => {
      @tailrec
      def go(i: Int, acc: Double): Double = {
        if (i <= 0) acc else
          go(i - 1, acc + work(i))
      }
      go(i, 0)
    }
    ).sum
  }
  val t2 = currentTimeMillis()
  println((t2 - t1) / 1000)

  def work(i: Int): Double = {
    sin(i) * cos(i) * math.tan(i)
  }

}
