package scala.futures

import java.lang.Thread.sleep

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Random, Success}

object FuturesSimple extends App {

  val f = Future {
    println("1 - starting calculation")
    sleep(Random.nextInt(500))
    42
  }

  println("2 - before onComplete")
  f.onComplete {
    case Success(value) => println(s"Got value $value")
    case Failure(e) => e.printStackTrace
  }

  (1 to 10).map { i =>
    print(".");
    sleep(100)
  }
}
