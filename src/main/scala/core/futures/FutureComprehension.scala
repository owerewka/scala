package scala.futures

import java.lang.System.currentTimeMillis
import java.lang.Thread.sleep

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.futures.Calculation.calculate
import scala.util.{Failure, Random, Success}

object Randoms {
  val r = new Random(currentTimeMillis())
  def random = r.nextInt(2000)
}

object Calculation {
  def calculate(name: String): Future[Int] = {
    Future {
      import scala.futures.Randoms.random
      println(s"$name starting processing")
      (1 to 5).foreach { i =>
        sleep(random)
        println(s"$name processing step: $i")
      }
      val result = random
      println(s"$name finished processing with value: $result")
      result
    }
  }
}


object FutureComprehensionSequentialExecution extends App {
  val res: Future[Int] = for {
    a: Int <- calculate("A")
    b: Int <- calculate("B")
    c: Int <- calculate("C")
  } yield a + b + c
  res onComplete {
    case Success(value) => println(s"Result of three serial scala.futures is: $value")
    case Failure(error) => println(s"Some fuck-up: $error")
  }
  Await.ready(res, Duration.Inf)
}

object FutureComprehensionParallelExecution extends App {
  val futureA: Future[Int] = calculate("A")
  val futureB: Future[Int] = calculate("B")
  val futureC: Future[Int] = calculate("C")
  val futureD: Future[Int] = calculate("D")
  val res: Future[Int] = for {
    a: Int <- futureA
    b: Int <- futureB
    c: Int <- futureC
    d: Int <- futureD
  } yield a + b + c + d
  res onComplete {
    case Success(result: Int) => println(s"Parallel result: $result")
    case Failure(error: Throwable) => println(s"Error $error")
  }
  Await.ready(res, Duration.Inf)
}

object FutureOnComplete extends App {
  calculate("A").onComplete {
    case Success(result) => println(s"Success with: $result")
    case Failure(error) => println(s"Failure with: $error")
  }
  Thread.sleep(20000)
}
