package scala.futures

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object FutureReferentialTransparency extends App {

  //future with side effect is not referentially transparent
  val expr = Future(println("Hey!"))
  Thread.sleep(100)
  println((expr, expr))

  println(Future(println("Hey!")), Future(println("Hey!")))
}
