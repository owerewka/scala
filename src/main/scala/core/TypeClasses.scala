package core

object TypeClasses extends App {

  //trait with an abstract method
  //in combination with the implicit instances of it
  //this pattern is called a type class

  //this allows us to provide specific implementation
  //for one types, but not for others
  trait Summable[T] {
    def sumElements(list: List[T]): T
  }
  object Summable {
    def apply[T](implicit summable: Summable[T]): Summable[T] = summable
  }

  implicit object IntSummable extends Summable[Int] {
    override def sumElements(list: List[Int]): Int = list.sum
  }
  implicit object StringSummable extends Summable[String] {
    override def sumElements(list: List[String]): String = list.mkString("")
  }
  implicit object BooleanSummable extends Summable[Boolean] {
    override def sumElements(list: List[Boolean]): Boolean = list.contains(true)
  }

  //implicit argument of a generic type T is called ad-hoc polymorphism
  //ad-hoc - because the implicit instance is required right at the call of
  //         of the processMyList method
  //polymorphism - depending on the actual instance of the Summable trait
  //               the behaviour of sumElements is polymorphic and depends
  //               on the actual type T
  def processMyList[T](list: List[T])(implicit summable: Summable[T]): T =
    summable.sumElements(list)


  println(processMyList(List(1, 2, 3)))
  println(processMyList(List("a", "b", "c")))
  println(processMyList(List(false, false, true)))


  //def apply[T](implicit summable: Summable[T]): Summable[T] = summable
  def processMyList1[T: Summable](list: List[T]): T = {
    Summable[T].sumElements(list)
  }

  def processMyList2[T: Summable](list: List[T]): T = {
    implicitly[Summable[T]].sumElements(list)
  }

  def processMyList3[T](list: List[T])(implicit summable: Summable[T]): T = {
    summable.sumElements(list)
  }
}

