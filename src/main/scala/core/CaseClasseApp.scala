package scala

/**
 * Created by oskar on 17.10.16.
 */
case class Person(firstName: String, lastName: String)

object CaseClassesApp extends App {

  sayHi(Person("Ala", "Kaciuba"))
  sayHi(Person("Benek", "Zenek"))
  sayHi(Person("Kleofas", "Wyszynski"))
  sayHi("Pizza")

  def sayHi(any: Any): Unit = {
    any match {
      case Person("Ala", _) => println(s"Cześć dla każdej Ali!")
      case p@Person(_, "Zenek") => println(s"Nazwisko Zenek! $p")
      case p@Person(_, lastName) => println(s"Pan z nazwiskiem $lastName jest nieznay, Person instance is: $p")
      case any => println(s"I have no match for $any!")
    }
  }

  ("Elon", 42) match {
    case guy@(name, 42) => println(s"Found $name: $guy")
  }
}