package core

import util.Util.IntOps

object Lazy extends App {

  val f: (Int, => Int) => Int = (a, b) =>
    if (a.isOdd) a
    else a * b

  println(f(
    {println("eval A"); 1},
    {println("eval B"); 2}
  ))

  println(f(
    {println("eval A"); 2},
    {println("eval B"); 3}
  ))

}