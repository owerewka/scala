package core

import java.util

object NullType extends App {

  val n: Null = null
  val g: String = null

  //wont compile, null cannot be used with AnyVal
  //val r: Int = null

  val d = new util.HashMap[java.lang.Integer, java.lang.Integer]()
  d.put(1, null)
  println(d.get(1))

  val s = Seq(1, 2, 3)
}
