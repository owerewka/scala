package core

object EtaExpansion extends App {

  def method(a: Int): Int = a

  val methodValue1 = method _ //eta expansion
  val methodValue2: Int => Int = method //auto eta-expansion
  val methodValue3: Int => Int = (a: Int) => method(a) //manual  method value creation

  List(1, 2, 3).map(method)
  List(1, 2, 3).map(method _)
}

object EtaExpansionMultipleArgumentLists extends App {

  def method(a: Int)(b: Int): Int = a + b
  val methodPartiallyApplied: Int => Int = method(2) _
  methodPartiallyApplied(3)

  List(1, 2, 3).map(method(3)) //auto eta-expansion on the second argument list
  List(1, 2, 3).map(method(3) _) //eta-expansion on the second argument list
}

