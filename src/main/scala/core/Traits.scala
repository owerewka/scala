package core

/**
 * Created by oskar on 17.10.16.
 */
object Traits {

  class A {
    def foo(): String = "A"
  }

  trait B extends A {
    override def foo(): String = super.foo() + "B"
  }

  trait C extends B {
    override def foo(): String = super.foo() + "C"
  }

  trait D extends A {
    override def foo(): String = super.foo() + "D"
  }

  trait E extends D {
    override def foo(): String = super.foo() + "E"
  }

  trait F extends E {
    override def foo(): String = super.foo() + "F"
  }

  object Linearization extends App {

    val a: A with B with C with D with E with F =
      new A with B with C with D with E with F

    println(a.foo())

  }
}