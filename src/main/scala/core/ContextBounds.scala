package core

object ContextBounds extends App {

  implicit val v: Option[String] = Option("Hi!")

  def f[T: Option](): Unit = println(implicitly[Option[T]])
  f()

}

object MultipleContextBounds extends App {

  implicit val option: Option[String] = Some("a")
  implicit val list: List[String] = List("b")
  implicit val tuple: Tuple1[String] = Tuple1("c")

  def f[A: Option : List : Tuple1]: Unit = {
    println(implicitly[Option[A]])
    println(implicitly[List[A]])
    println(implicitly[Tuple1[A]])
  }

  def g[A](implicit option: Option[A], list: List[A], tuple: Tuple1[A]): Unit = {
    println(option)
    println(implicitly[List[A]])
    println(implicitly[Tuple1[A]])
  }

  f
  g
}