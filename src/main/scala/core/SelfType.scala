package core

object SelfType extends App {

  trait Person {
    def name: String
  }
  trait Account { this: Person =>
    def id: Long
  }
  trait Authentication { this: Account =>
    def credentials: String
  }
  trait User { this: Authentication =>
    def authentication: String
  }

  val user: User with Person with Account with Authentication =
    new User with Person with Account with Authentication {
      override def name: String = "Oskar"
      override def id: Long = 256
      override def credentials: String = "PUBKEY:123456"
      override def authentication: String = s"$id $name $credentials"
      override def toString: String = authentication
    }

  println(user)

}

object SelfTypeOuter extends App {
  class Foo { self: Foo =>
    class Bar {
      def getFoo: Foo = self
    }
  }
  val f = new Foo()
  val b = new f.Bar()
  println(b.getFoo)
}

object SelfTypeReference extends App {
  class Foo { self: Foo =>
    println(this)
    println(self)
  }
  new Foo
}

object SeparateClassAndMethods extends App {

  case class Data(v: Long) extends DataOperations

  trait DataOperations { this: Data =>
    def +(o: Data): Data = Data(v + o.v)
    def -(o: Data): Data = Data(v - o.v)
    def /(o: Data): Data = Data(v / o.v)
    def *(o: Data): Data = Data(v * o.v)
  }

  println(Data(10) + Data(1))
  println(Data(10) - Data(3))
  println(Data(10) / Data(2))
  println(Data(10) * Data(4))

}