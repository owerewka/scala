package scala

object UnifiedTypes extends App {

  val anyList: List[Any] = List(
    "string",
    123,
    'c',
    true,
    () => "an anonymous function"
  )
  anyList.map("Any: " + _).foreach(println)

  val anyValList: List[AnyVal] = List(
    123,
    'c',
    true
  )
  anyValList.map("AnyVal: " + _).foreach(println)

  val anyRefList: List[AnyRef] = List(
    "string",
    () => "an anonymous function"
  )
  anyRefList.map("AnyRef: " + _).foreach(println)

}