package scala

import scala.language.postfixOps

object Operators extends App {

  case class UFO(i: Int) {
    //infix operator
    def ~+:><:+~(i: Int): Int = i * 2
    //prefix operators
    def unary_! : Int = -1 * i
    def unary_~ : String = s"UFO:$i"
    def unary_- : String = s"-UFO:$i"
    def unary_+ : String = s"+UFO:$i"
    //postfix operators
    def postFix: Int = 10 + i
    def >>>> : Int = 100 + i
  }
  object UFOImplicits {
    implicit def intWrapper(i: Int): UFO = UFO(i)
  }

  import UFOImplicits._

  println(2 ~+:><:+~ 4)
  println(!2)
  println(~UFO(4))
  println(-UFO(5))
  println(+UFO(6))
  println(3 postFix)
  println(3 >>>>)
}