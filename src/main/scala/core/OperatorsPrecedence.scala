package core

object OperatorsPrecedence extends App {

  import OperatorsAssociativity._

  println("A" :@ "B" @: "C")
  println("A" :@ ("B" @: "C"))
  println(("A" :@ "B") @: "C")

}
