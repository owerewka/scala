package scala.io

object SourceTest extends App {

  val veryLargeTextFilePath = "d:\\Dropbox\\tqsm\\2019-08\\tesco\\raw2019-07-16_2019-07-17"

  measureTime {
    val source = Source.fromFile(veryLargeTextFilePath, "UTF-8")
    println(s"mkString: ${source.mkString.size}")
    source.close
  }

  measureTime {
    val source = Source.fromFile(veryLargeTextFilePath, "UTF-8")
    println(s"getLines: ${source.getLines().mkString.size}")
    source.close
  }


  def measureTime(block: => Unit): Unit = {
    val t1 = System.currentTimeMillis()
    block
    val t2 = System.currentTimeMillis()
    println(s"Execution time was ${t2 - t1}")
  }
}
