package core

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import scala.math.abs

class OptionLift extends AnyFlatSpec with Matchers {

  def lift[A, B](f: A => B): Option[A] => Option[B] =
    o => o.map(f)

  val f: Int => Int = _ * 2

  lift(f)(None) shouldBe None
  lift(f)(None) shouldBe None.map(f)
  lift(f)(Some(3)) shouldBe Some(6)
  lift(abs)(Some(-100)) shouldBe Some(100)

}

class ListLift extends AnyFlatSpec with Matchers {

  val f: Int => Int = _ * 3

  def lift[A, B](f: A => B): List[A] => List[B] = _.map(f)

  lift(f)(Nil) shouldBe Nil
  lift(f)(List(1)) shouldBe List(3)
  lift(f)(List(1, 2)) shouldBe List(3, 6)

  private val llift = (f: Int => Int) => lift(lift(f))
  llift(f)(List(List(1, 2, 3))) shouldBe List(List(3, 6, 9))
}