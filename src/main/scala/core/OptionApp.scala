package core

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import scala.annotation.tailrec

class OptionApp extends AnyFlatSpec with Matchers {

  "Option" should "map2" in {

    type MAP2 = (Option[Int], Option[Int]) => ((Int, Int) => Int) => Option[Int]

    def map2[A, B, C](ao: Option[A], bo: Option[B])(f: (A, B) => C): Option[C] =
      (ao, bo) match {
        case (Some(a), Some(b)) => Some(f(a, b))
        case _ => None
      }

    def map2flatMap[A, B, C](ao: Option[A], bo: Option[B])(f: (A, B) => C): Option[C] =
      ao.flatMap(a => bo.map(b => f(a, b)))

    def map2forCom[A, B, C](ao: Option[A], bo: Option[B])(f: (A, B) => C): Option[C] =
      for {
        a <- ao
        b <- bo
      } yield f(a, b)

    checkMe(map2)
    checkMe(map2flatMap)
    checkMe(map2forCom)

    def checkMe(f: MAP2): Unit = {
      f(Some(1), Some(2))(_ + _) shouldBe Some(3)
      f(None: Option[Int], Some(1))(_ + _) shouldBe None
      f(Some(1), None: Option[Int])(_ + _) shouldBe None
      f(None, None)(_ + _) shouldBe None
    }
  }

  "Try" should "try" in {

    def Try[A](a: => A): Option[A] =
      try Some(a)
      catch {case _: Exception => None}

    Try {throw new RuntimeException("Fail!")} shouldBe None
    Try {1} shouldBe Some(1)
    Try(2) shouldBe Some(2)
    Try {"a word".toInt} shouldBe None
    Try {"123456".toInt} shouldBe Some(123456)
  }

  "List" should "sequence" in {

    def sequence[T](lo: List[Option[T]]): Option[List[T]] = {
      @tailrec
      def go(as: List[Option[T]], acc: List[T]): Option[List[T]] = {
        as match {
          case Nil => Some(acc)
          case Some(h) :: t => go(t, acc :+ h)
          case _ => None
        }
      }
      go(lo, Nil)
    }

    sequence(List(None)) shouldBe None
    sequence(List(Some(1))) shouldBe Some(List(1))
    sequence(List(Some(1), None)) shouldBe None
    sequence(List(Some(1), Some(2))) shouldBe Some(List(1, 2))
  }

  "List" should "traverse" in {

    def traverse[A, B](a: List[A])(f: A => Option[B]): Option[List[B]] = {
      @tailrec
      def go(as: List[A], acc: List[B]): Option[List[B]] = {
        as match {
          case Nil => Some(acc)
          case h :: t =>
            f(h) match {
              case Some(b) => go(t, acc :+ b)
              case None => None
            }
          case _ => None
        }
      }
      go(a, Nil)
    }

    traverse[Int, Int](Nil)(_ => None) shouldBe Some(Nil)
    traverse[Int, Int](Nil)(_ => Some(1)) shouldBe Some(Nil)
    traverse(List(1))(_ => None) shouldBe None
    traverse(List(1))(a => Some(a * 2)) shouldBe Some(List(2))
    traverse(List(1, 2))(a => if (a == 2) Some(a) else None) shouldBe None
    traverse(List(1, 2))(a => Some(a * 2)) shouldBe Some(List(2, 4))
  }
}
