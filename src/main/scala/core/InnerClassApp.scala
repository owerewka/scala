package core

object InnerClassApp extends App {

  class Outer {
    class Inner {
      def test(other: Outer#Inner): Unit = println(other)
    }
  }

  val outer1 = new Outer
  val inner1 = new outer1.Inner
  val outer2 = new Outer
  val inner2 = new outer2.Inner

  inner1.test(inner1)
  inner1.test(inner2)

}