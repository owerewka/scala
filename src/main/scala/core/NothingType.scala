package scala

object NothingType extends App {

  //usage in higher kinded types to indicate they are empty
  //case object Nil extends List[Nothing]
  //Seq[Nothing] <: Seq[String]
  val a: Seq[Nothing] = Nil
  val b: Seq[String] = a
  println(a)
  println(b)

  //case object None extends Option[Nothing]
  val o: Option[Int] = None

  //indication of a function that never returns
  val fe: () => Nothing = () => throw new Exception("Java is alive!")
  try {
    fe
  } catch {
    case e: Exception => println(e)
  }

  //def ??? : Nothing = throw new NotImplementedError
  val s: Nothing = ???
}
