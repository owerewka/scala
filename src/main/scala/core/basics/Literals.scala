package scala.basics

object Literals extends App {

  val json = """{"menu": {
               |  "id": "file",
               |  "value": "File",
               |  "popup": {
               |    "menuitem": [
               |      {"value": "New", "onclick": "CreateNewDoc()"},
               |      {"value": "Open", "onclick": "OpenDoc()"},
               |      {"value": "Close", "onclick": "CloseDoc()"}
               |    ]
               |  }
               |}}""".stripMargin

  println(json)

}
