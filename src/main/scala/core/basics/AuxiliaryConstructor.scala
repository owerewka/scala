package scala.basics

class Numbers(l: Int, k: Int) {

  def this(l: Int) {
    this(l, 100)
  }

  def this(l: String, k: String) {
    this(l.toInt, k.toInt)
  }

}


object AuxiliaryConstructor extends App {

  println(new Numbers(1))
  println(new Numbers("100", "200"))

}
