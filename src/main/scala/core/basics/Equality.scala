package core.basics


object CompareWithoutEquals extends App {

  class NoEquals(val value: String)
  object NoEquals {
    def apply(value: String): NoEquals = new NoEquals(value)
  }
  println(NoEquals("value") == NoEquals("value"))
  println(NoEquals("value") equals NoEquals("value"))

}

class Data(val value: String) {

  override def equals(obj: Any): Boolean =
    obj match {
      case d: Data if d.value == value => true
      case _ => false
    }
}

object CompareClass extends App {

  println(new Data("value1") equals new Data("value1"))
  println(new Data("value1") equals new Data("value2"))

  println(new Data("value1") == new Data("value1"))
  println(new Data("value1") == new Data("value2"))

}

case class Value(data: String)

object CompareCaseClass extends App {

  println(Value("a").canEqual(256))
  println(Value("a").canEqual(Value("b")))

}

object CompareWithNull extends App {

  println("a" == "a")
  println("a" equals "a")
  println(null == "a")
  println(null == null)
  try {
    null equals "a"
  } catch {
    case ex: java.lang.NullPointerException => println(s"What would you expect: $ex ?")
  }

}

object CompareWithImplicitConventions extends App {

  println(3 == BigInt(3)) //true
  println(BigInt(3) == 3) //true

  println(3 equals BigInt(3)) //false
  println(BigInt(3) equals 3) //true

}

object ReferenceComparison extends App {

  val x = Value("value1")
  val y = Value("value2")

  println(x eq x)
  println(x eq y)

  println(x ne x)
  println(x ne y)
}