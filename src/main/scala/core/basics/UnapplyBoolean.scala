package core.basics

object UnapplyBoolean extends App {

  object ContainsA {
    def unapply(s: String): Boolean =
      s.contains("A")
  }

  object ContainsB {
    def unapply(s: String): Option[Boolean] =
      if (s.contains("B")) Some(true) else None
  }

  val f: String => String = {
    case ContainsA() => "ContainsA"
    case ContainsB(b) => s"ContainsB: $b"
  }

  println(f("A"))
  println(f("B"))

}
