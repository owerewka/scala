package core.basics.typetags

import core.basics.typetags.Printer.printTypeParameters

import scala.reflect.runtime.universe._

object TypeTags extends App {

//  import scala.reflect.runtime.universe._
//  val typeTag: TypeTag[List[String]] = typeTag[List[String]]
//  println(typeTag)

//  import scala.reflect._
//  val classTag: ClassTag[List[String]] = classTag[List[String]]
//  println(classTag)

}

object TypeTagsImplicitArguments extends App {

  def paramInfo[T](value: T)(implicit tag: TypeTag[T]): Unit = {
    printTypeParameters(value, tag)
  }

  paramInfo(42)
  paramInfo(List(1, 2))
  paramInfo(Seq(List(256)))
  paramInfo[Seq[Seq[Int]]](Seq(List(256)))
  paramInfo((1, 2, 3, "a", "b", "c"))
}

object ContextBoundOfTypeParameterApp extends App {

  def paramInfo[T: TypeTag](value: T): Unit = {
    printTypeParameters[T](value)
  }

  paramInfo(List(1, 2))
  paramInfo(Seq(List(256)))
  paramInfo[Seq[Seq[Int]]](Seq(List(256)))
  paramInfo((1, 2, 3, "a", "b", "c"))
}

object Printer {

  def printTypeParameters[T](value: T)(implicit tag: TypeTag[T]): Unit = {
    val typeArgs = tag.tpe match {
      case TypeRef(_, _, args) => args.mkString(", ")
    }
    println(s"type of $value has type arguments: ${typeArgs.mkString}")
  }

}