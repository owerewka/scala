package core.basics.typetags


object Test1 extends App {

  class Foo
  class Bar extends Foo

  //this doesn't work because of type erasure
  def meth[A](xs: List[A]): String = xs match {
    case _: List[String] => "list of strings"
    case _: List[Foo] => "list of foos"
  }

  println(meth(List("string")))
  println(meth(List(new Bar)))

}

//noinspection ScalaUnusedSymbol,DuplicatedCode
object Test2 extends App {

  class Foo
  class Bar extends Foo

  import scala.reflect.runtime.universe._

  val t = scala.reflect.ClassTag

  def method[A: TypeTag](xs: List[A]): String =
    typeOf[A] match {
      case t if t =:= typeOf[String] => "list of strings"
      case t if t <:< typeOf[Foo] => "list of foos"
    }

  def method1[A](xs: List[A])(implicit evidence: TypeTag[A]): String =
    typeOf[A](evidence) match {
      case t if t =:= typeOf[String] => "list of strings"
      case t if t <:< typeOf[Foo] => "list of foos"
    }

  println(method1(List("string")))
  println(method1(List(new Bar)))

}