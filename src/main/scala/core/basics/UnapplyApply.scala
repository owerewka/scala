package core.basics

object UnapplyApply extends App {

  class Sensor(val key: String, val value: Int)
  object Sensor {
    def apply(key: String, value: Int) = new Sensor(key, value)
    def unapply(arg: Sensor): Option[(String, Int)] = Some(arg.key, arg.value)
  }

  val sensor: Sensor = Sensor("1A", 25)

  val Sensor(key, value) = sensor
  println(s"key: $key, value: $value")

  sensor match {
    case Sensor(key, value) => println(s"key: $key, value: $value")
  }
}