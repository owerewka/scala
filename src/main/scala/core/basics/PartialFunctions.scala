package core.basics

import org.scalatest.matchers.should.Matchers._

object PartialFunctions extends App {

  val divide1: PartialFunction[Int, Int] = new PartialFunction[Int, Int] {
    def apply(x: Int): Int = 42 / x
    def isDefinedAt(x: Int): Boolean = x != 0
  }

  divide1.isDefinedAt(0) shouldBe false
  the[ArithmeticException] thrownBy divide1(0) should have message "/ by zero"

  val divide2: PartialFunction[Int, Int] = {
    case d: Int if d != 0 => 42 / d
  }

  divide2(10) shouldBe 4
  the[MatchError] thrownBy divide2(0) should have message "0 (of class java.lang.Integer)"
}

object ChainingWithInterface extends App {

  val divide = new PartialFunction[Int, Int] {
    override def isDefinedAt(x: Int): Boolean = x != 0
    override def apply(v1: Int): Int = 100 / v1
  }

  val escape = new PartialFunction[Int, Int] {
    override def isDefinedAt(x: Int): Boolean = x == 0
    override def apply(v1: Int): Int = -100
  }

  val f: PartialFunction[Int, Int] = divide orElse escape

  f(10) shouldBe 10
  f(0) shouldBe -100

}

object SomeDefaultBehaviour extends App {

  val f0: PartialFunction[Int, Int] = {
    case x: Int if x == 0 => 5
  }

  f0.isDefinedAt(-1) shouldBe false
  f0.isDefinedAt(0) shouldBe true
  f0.isDefinedAt(1) shouldBe false
  f0(0) shouldBe 5
}

object ChainingWithCaseNotation extends App {

  val f1: PartialFunction[Int, Int] = {
    case x: Int if x == 0 => -1
  }

  val f2: PartialFunction[Int, Int] = {
    case x: Int if x == 10 => -100
  }

  val f3: PartialFunction[Int, Int] = {
    case x => x
  }

  val chain: PartialFunction[Int, Int] = f1.orElse(f2).orElse(f3)

  chain(0) shouldBe -1
  chain(10) shouldBe -100
  chain(99) shouldBe 99
}