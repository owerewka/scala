package core.basics.partialfunctions

//noinspection DuplicatedCode
object CollectWithPartialFunction extends App {

  val sample = 1 to 10

  val isEven: PartialFunction[Int, String] = {
    case x if x % 2 == 0 => x + " is even"
  }

  // the method collect can use PartialFunction
  // for filtering and mapping
  val evenNumbers = sample.collect(isEven)

  println(evenNumbers.mkString("\n"))

}
