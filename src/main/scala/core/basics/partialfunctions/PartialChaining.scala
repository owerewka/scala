package core.basics.partialfunctions

import core.basics.partialfunctions.Functions._
import util.Util._

object PartialChaining extends App {

  showResult {
    10 to 19 map
      (dontWant4 orElse isEven orElse isEvil orElse isOdd)
  }
  showResult {
    40 to 49 map process
  }

  def showResult(f: => Seq[String]): Unit = {
    println(f.mkString("\n"))
  }
}

//noinspection DuplicatedCode
object Functions {

  val isEvil: PartialFunction[Int, String] = {
    case i if i.isIn(5, 7) => s"$i is Evil!"
  }

  val isEven: PartialFunction[Int, String] = {
    case i if i.isEven => i + " is even"
  }

  val isOdd: PartialFunction[Int, String] = {
    case i if i.isOdd => i + " is odd"
  }

  val dontWant4: PartialFunction[Int, String] = {
    case i if i == 4 => "Nothing"
  }

  val process: PartialFunction[Int, String] = {
    case i if i.isIn(45, 47) => s"$i is Evil!"
    case i if i.isEven => i + " is even"
    case i if i.isOdd => i + " is odd"
  }
}

