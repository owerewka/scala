package scala.basics.traits.context

case class NotificationService() {
  def sendNotification(notification: String) =
    println(s"Senging notification: $notification")
}
case class OtherSpark() {
  def processQuery(query: String) =
    println(s"Processing query: $query")
}

sealed trait Context
trait SparkContext extends Context {
  val spark: OtherSpark
}
trait NotificationContext extends Context {
  val notification: NotificationService
}

class SparkAppSendingNotificationsTask(context: SparkContext with NotificationContext) {
  context.notification.sendNotification("starting application")
  context.spark.processQuery("world count size")
}

object TestApp extends App {
  val context = new SparkContext with NotificationContext {
    val spark: OtherSpark = OtherSpark()
    val notification: NotificationService = NotificationService()
  }
  new SparkAppSendingNotificationsTask(context)
}
