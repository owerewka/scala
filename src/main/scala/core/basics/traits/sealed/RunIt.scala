package scala.basics.traits.`sealed`


object RunIt extends App {

  val name: Name = Marysia

  name match {
    case Asia => println("Czesc Asiu!")
    case Basia => println("Czesc Basiu!")
    case Kasia => println("Czesc Kasiu!")
    case Marysia => println("Czesc Marysiu!")
    case Zosia => println("Czesc Zosiu!")
  }

}
