package scala.basics.traits.`sealed`

sealed trait Name {
}

case object Asia extends Name

case object Basia extends Name

case object Kasia extends Name

case object Marysia extends Name

case object Zosia extends Name
