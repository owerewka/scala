package core.basics

class UniformAccess {

  def uniformMethod: String = "uniformMethod"
  val uniformField: String = "uniformField"

}

object Run extends App {
  val instance = new UniformAccess();
  println(instance.uniformMethod)
  println(instance.uniformField)
}