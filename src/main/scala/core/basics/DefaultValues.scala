package scala.basics


object DefaultValues extends App {

  var list: String = _
  println(list)

  var int: Int = _
  println(int)

  var double: Double = _
  println(double)

  var boolean: Boolean = _
  println(boolean)

}
