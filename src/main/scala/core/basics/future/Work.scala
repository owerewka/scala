package scala.basics.future

import scala.util.Random


/**
 * Created by Oskar on 02-08-2015.
 */
class Work(amount: Int, name: String = "") {

  for (_ <- 1 to amount) {

    val delay: Long = Random.nextInt(300)
    println("Working " + name + ": " + delay)
    Thread.sleep(delay)

  }

}

object Work {
  //ugly factory method
  def apply(amount: Int, name: String = ""): Work = new Work(amount, name)
}