package scala.basics.future

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{Duration, _}
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

object Simple1 extends App {

  val textFuture = Future {
    throw new RuntimeException("I have died!")
    "Unreachable!"
  }

  textFuture onComplete {
    case Success(posts) => for (post <- posts) println(post)
    case Failure(t) => println("An error has occured: " + t.getMessage)
  }


  Await.result(textFuture, Duration(100, MILLISECONDS));

}
