package scala.basics.future


import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

object Simple2 extends App {

  try {
    cos
    println("Ciekawe czy to sie wykona?")
  } catch {
    case e: Exception => println("Cos sie zjebalo: " + e.toString)
  }


  def cos = {
    val future1 = Future {
      for (i <- 1 to 10) {
        println(i)
      }
      //throw new NullPointerException("to jest koniec")
    }

    future1 onComplete {
      case Failure(throwable) => println("Oh umarlem: " + throwable.toString)
      case Success(result) =>
        println("Counting is succesful!")
        val future2 = Future {
          for (i <- 100 to 120) {
            Thread.sleep(50)
            print(i + " ")
          }
        }

        future2 onComplete { case result =>
          println("Finish!")
        }
    }


    Await.result(future1, Duration(100, MILLISECONDS));
    Thread.sleep(1000)
  }
}
