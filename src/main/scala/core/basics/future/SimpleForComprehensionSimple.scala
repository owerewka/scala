package scala.basics.future


import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{Duration, _}
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

object SimpleForComprehensionSimple extends App {

  val future1 = Future {
    Work(5, "Basia")
    5
  }

  val future2 = Future {
    Work(10, "Kasia")
    10
  }

  val future3 = Future {
    Work(7, "Bad!")
    15
  }

  private val eventualString: Future[Int] = for {
    x <- future1
    y <- future2
    _ <- Future {
      Thread.sleep(x + y)
      println("Ja tutaj tylko drukuje i nic wiecej nie robie!")
    }
    z <- future3
  } yield x + y + z

  eventualString onComplete {
    case Success(v) => println("onSuccess value: " + v)
    case Failure(t) => println("onFailure value: " + t)
  }

  Await.result(eventualString, Duration(100000, MILLISECONDS));
  println("Finish")

}
