package scala.basics.future

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.{Duration, _}
import scala.concurrent.{Await, Future}
import scala.util.{Failure, Success}

/**
 * Created by Oskar on 02-08-2015.
 */
object SimpleForComprehensionWithError extends App {

  val future1 = Future {
    Work(5, "Basia")
    5
  }

  val future2 = Future {
    Work(10, "Kasia")
    10
  }

  val future3 = Future {
    Work(7, "Bad!")
    throw new NullPointerException("Be bad!")
    100
  }

  private val eventualString: Future[Int] = for {
    x <- future1
    y <- future2
    z <- future3
  } yield x + y + z

  eventualString.onComplete {
    case Success(v) => println("onSuccess value: " + v)
    case Failure(t) => println("onFailure value: " + t)
  }

  Await.result(eventualString, Duration(100000, MILLISECONDS));

  eventualString.value match {
    case Some(x) => {
      x match {
        case Success(value) => println("Tre result is: " + value)
        case Failure(t) => println("Failure with error: " + t.getMessage)
      }
    }
    case None => println("There is no result yet!")
  }

  println("Finish")

}
