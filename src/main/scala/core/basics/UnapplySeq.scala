package core.basics

class Database(val data: Int*)

object Database {
  def apply(data: Int*) = new Database(data: _*)

  def unapplySeq(values: Database): Option[Seq[Int]] = Some(values.data)
}

object UnapplySeq extends App {

  //Match specific number of arguments
  Database(10, 11, 12) match {
    case Database(a, b, c) => println(s"case2 $a $b $c")
  }

  //Match all of the unknown arguments count
  Database(1, 2, 3) match {
    case Database(all@_*) => println(s"case all: $all")
  }

  //Split arguments into first element and the reminder
  Database(100, 110, 120, 130) match {
    case Database(a, remainder@_*) => println(s"case1 $a remainder: $remainder")
  }

}

object UnapplySeqWithSeq extends App {

  val seq: Seq[Int] = Seq(1, 2, 3, 4, 5)

  seq match {
    case Seq(first, theRest@_*) => println(s"first: $first, $theRest")
  }

}
