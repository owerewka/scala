package core.basics

object StableIdentifier extends App {

  val open = "open"
  val close = "close"

  //wrong open always matches
  close match {
    case open => println("open")
    case close => println("close")
  }

  //correct
  close match {
    case `open` => println("open")
    case `close` => println("close")
  }

  close match {
    case "open" => println("open")
    case "close" => println("close")
  }

  val Open = "open"
  val Close = "close"
  Close match {
    case Open => println("open")
    case Close => println("close")
  }
}