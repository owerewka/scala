package core.basics.recursion

import core.basics.recursion.Printer.show

import scala.annotation.tailrec

object Fibonacci extends App {

  def fib(n: Int): Int =
    n match {
      case 0 => 0
      case 1 => 1
      case _ => fib(n - 1) + fib(n - 2)
    }

  show(0 to 10)(fib)

  def fibTail1(n: Int): Int = {
    @tailrec
    def go(i: Int, acc1: Int, acc2: Int): Int = {
      if (i >= n) acc1 + acc2
      else go(i + 1, acc2, acc1 + acc2)
    }
    n match {
      case 0 => 0
      case 1 => 1
      case _ => go(2, 0, 1)
    }
  }

  show(0 to 10)(fibTail1)

  def fibTail2(n: Int): Int = {
    @tailrec
    def go(steps: Int, curr: Int, next: Int): Int =
      if (steps <= 0) curr else
        go(
          steps = steps - 1,
          curr = next,
          next = next + curr
        )
    go(steps = n, curr = 0, next = 1)
  }

  show(0 to 10)(fibTail2)
}