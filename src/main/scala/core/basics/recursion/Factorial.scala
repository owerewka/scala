package core.basics.recursion

import core.basics.recursion.Printer.show

import scala.annotation.tailrec

object Factorial extends App {

  def factorialInfo(n: Int): (Int, String) = {
    @tailrec
    def go(i: Int, acc: Int, strAcc: String): (Int, String) = {
      if (i <= n)
        go(i + 1, i * acc, s"$i*($strAcc)")
      else
        (acc, strAcc)
    }
    go(1, 1, "1")
  }

  Option(factorialInfo(4)).map {
    case (f, s) => s"$f\n$s"
  }.foreach(println)

  def factorial(n: Int): Int = {
    @tailrec
    def go(n: Int, acc: Int): Int = {
      if (n > 0) go(n - 1, n * acc)
      else acc
    }
    go(n, 1)
  }

  show(0 to 5)(factorial)

  def factorialRange(n: Int): Int = (1 to n).product

  show(0 to 5)(factorialRange)
}