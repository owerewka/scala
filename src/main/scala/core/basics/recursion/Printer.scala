package core.basics.recursion


object Printer {

  val show: IterableOnce[Int] => (Int => Int) => Unit =
    range => f =>
      range.iterator
        .map(n => s"f($n)=${f(n)} ")
        .foreach(println)

}