package core.basics.recursion

import scala.annotation.tailrec

object SumOfList extends App {

  def sum(list: List[Int]): Int = {
    @tailrec
    def go(list: List[Int], acc: Int): Int =
      list match {
        case Nil => acc
        case head :: tail => go(tail, acc + head)
      }
    go(list, acc = 0)
  }

  val l = List(1, 2, 3, 4, 5)
  println(sum(l)) // prints 5

}
