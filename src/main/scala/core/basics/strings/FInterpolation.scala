package scala.basics.strings

object FInterpolation extends App {

  val int: Int = 123456
  val str: String = "string"
  val dou: Double = 344.123454d

  println(f"'$str%10s'")
  println(f"'$str%10S'")
  println(f"$dou%2.2f")
  println(f"'$int'")
  println(f"'$int%10d'")

}
