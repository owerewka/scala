package core.basics.strings

import scala.annotation.tailrec

object CustomStringInterpolation1 extends App {

  implicit class UpHelper(val sc: StringContext) extends AnyVal {
    def up(values: Any*): String = {
      @tailrec
      def go(parts: Seq[String], values: Seq[Any], acc: String): String =
        (parts, values) match {
          case (pHead :: pTail, vHead :: vTail) => go(pTail, vTail, acc + pHead + vHead)
          case (List(lastPart), Nil) => acc + lastPart
        }
      go(sc.parts.toList, values.toList, acc = "")
    }
  }

  val value1 = 11
  val value2 = 12
  println(up"Value1: $value1, Value2: $value2")

  //UpHelper implicit class adds a method with our custom
  //string interpolation to the class StringContext
  //so it can be called like this:
  println(
    StringContext("Value1: ", ", Value2: ", "")
      .up(value1, value2))
}

object CustomStringInterpolation2 extends App {

  implicit class UpHelper(val sc: StringContext) extends AnyVal {
    def up(args: Any*): String = {
      @tailrec
      def go(parts: Iterator[String], values: Iterator[Any], acc: String): String =
        if (values.hasNext)
          go(parts, values, acc + parts.next() + values.next())
        else
          acc + parts.next()
      go(sc.parts.iterator, args.iterator, "")
    }
  }

  val value1 = 11
  val value2 = 12
  println(up"Value1: $value1, Value2: $value2 ")
}

object CustomStringInterpolation3 extends App {

  implicit class UpHelper(val sc: StringContext) extends AnyVal {
    def up(args: Any*): String = {
      val parts: Iterator[String] = sc.parts.iterator
      val values: Iterator[Any] = args.iterator
      val buf = new StringBuffer(parts.next)
      while (parts.hasNext) {
        buf append values.next.toString.toUpperCase.trim
        buf append parts.next
      }
      buf.toString
    }
  }

  val value1 = 11
  val value2 = 12
  println(up"Value1: $value1, Value2: $value2 ")
}