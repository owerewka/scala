package scala.basics.strings

case class Deposit(accountId: String, amount: Long) {

  override def toString: String =
    s"""{
       |  "Deposit": {
       |    "accountId": "$accountId",
       |    "amount": $amount
       |  }
       |}
     """.stripMargin

}

object Test extends App {

  println(Deposit("id", 123))

}