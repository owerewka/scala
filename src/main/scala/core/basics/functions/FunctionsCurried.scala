package core.basics.functions

/**
 * Created by Oskar on 12-07-2015.
 */
object FunctionsCurried extends App {

  def h(x: Int)(y: Int) = x + y

  val plusOne: Int => Int = h(1)
  println(plusOne(2))

  val f: Int => Int => Int = x => y => x + y
  val g: Int => Int = f(1)

  println(g(2))

}
