package scala.basics.functions

class Functions {

  val f: (Int, Int) => Int = (x: Int, y: Int) => x + y
  val curried: Int => Int = f.curried(10)
  val tupled: ((Int, Int)) => Int = f.tupled

  println(f.apply(10, 1))
  println(curried.apply(5))
  println(tupled.apply((10, 7)))

  val list: Seq[Int] = 1 to 10
  println(list)
  list.foreach((x: Int) => print(x))
  println("")
  list.foreach(x => print(x))
  println("")
  list.foreach(print(_))
  println("")
  list.foreach(print _)
  println("")
  list.foreach(print)

  val f1 = println()
  val f2 = println

  def sum(x: Int, y: Int, z: Int): Int = x + y + z

  val fs1 = sum _
  println(fs1(1, 2, 3))

  val partial1 = sum(10, _: Int, 10)
  println(partial1(3))

  val partial2 = sum(_: Int, 100, 100)
  println(partial2(13))

  val partial3 = sum(1000, 1000, _: Int)
  println(partial3(113))

  val partial4 = sum(10000, _: Int, _: Int)
  println(partial4(5, 5))


  list.foreach((x: Int) => println(x > 3))
  list.foreach(x => x > 3)
  list.foreach(_ > 3)
}

object RunIt extends App {

  new Functions

}