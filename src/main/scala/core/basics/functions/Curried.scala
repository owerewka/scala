package core.basics.functions

object Curried extends App {

  def func(a: Int, b: Int, c: Int): Int = a + b + c

  val curried: Int => Int => Int => Int =
    (func _).curried

}
