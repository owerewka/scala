package scala.basics.functions

/**
 * Created by Oskar on 04-07-2015.
 */
object FunctionLiteral {

  def main(args: Array[String]) {

    val s: String = "ABDaF"

    println(s.charAt(0).isLower)

    val x: Boolean = s exists (p => {
      println(p)
      p.isLower
    })
    println(x)

  }

}
