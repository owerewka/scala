package core.basics.functions

trait T {

  type A
  type B
  type C
  val a: A
  val b: B
  val f: (A, B) => C

  val h: B => C = f(a, _)
  val k: A => C = f(_, b)

  lazy val g: A => B => C = f.curried
}

class M extends T {
  override type A = Int
  override type B = Int
  override type C = Int
  override val a: A = 1
  override val b: B = 10
  override val f: (A, B) => C = _ + _
}

object Test1 extends App {

  val m: M = new M()

  println(m.g(1)(2))
  println(m.h(10))
  println(m.k(10))
}