package core.basics.functions

object Samples {
  case class X(i: Int)
  case class Y(i: Int)
  case class Z(i: Int)
  val f: (X, Y) => Z = (a, b) => Z(a.i + b.i)
  val x: X = X(5)
  val y: Y = Y(50)
  val z: Z = Z(500)
}

object Partial extends App {

  import Samples._
  def partial1[A, B, C](a: A, f: (A, B) => C): B => C = f(a, _)
  def partial2[A, B, C](a: A, f: (A, B) => C): B => C = (b: B) => f(a, b)
  println(partial1(x, f)(y))
  println(partial2(x, f)(y))

}

object Curry extends App {

  import Samples._
  def curry1[A, B, C](f: (A, B) => C): A => B => C =
    (a: A) =>
      (b: B) =>
        f(a, b)

  def curry2[A, B, C](f: (A, B) => C): A => B => C = f.curried

  def curry3[A, B, C](f: (A, B) => C): A => B => C = a => b => f(a, b)

  println(f(x, y))
  println(curry1(f)(x)(y))
  println(curry2(f)(x)(y))
  println(curry3(f)(x)(y))
}

object Uncurry extends App {

  import Samples._
  def uncurry[A, B, C](f: A => B => C): (A, B) => C = (a, b) => f(a)(b)

  val g: X => Y => Z =
    x =>
      y =>
        f(x, y)

  println(g(x)(y))
  println(uncurry(g)(x, y))

}

object Composition extends App {

  import Samples._
  val f: Y => Z = y => Z(1000 + y.i)
  val g: X => Y = x => Y(100 + x.i)

  def compose1[A, B, C](f: B => C, g: A => B): A => C = a => f(g(a))
  def compose2[A, B, C](f: B => C, g: A => B): A => C = f compose g
  def compose3[A, B, C](f: B => C, g: A => B): A => C = g andThen f

  println(f(y))
  println(g(x))
  println(compose1(f, g)(x))
  println(compose2(f, g)(x))
  println(compose3(f, g)(x))

}