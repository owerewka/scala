package scala.basics.functions

/**
 * Created by Oskar on 12-07-2015.
 */
object FunctionColsure {

  def createIncrement(more: Int) = (x: Int) => {
    x + more
  }

  def main(args: Array[String]) {

    val a10 = createIncrement(10)
    val a100 = createIncrement(100)
    val a1000 = createIncrement(1000)

    println(a10(7))
    println(a100(7))
    println(a1000(7))
  }
}
