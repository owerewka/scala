package core.basics.functions

import java.io.File

/**
 * Created by Oskar on 12-07-2015.
 */
object FunctionClosures {

  private def filesHere: Seq[File] = (new java.io.File("c:\\Windows\\System32")).listFiles().toSeq

  private def filesMatching(matcher: String => Boolean): Seq[File] =
    for (file <- filesHere; if matcher(file.getName))
      yield file

  def filesEnding(query: String) = filesMatching(_.endsWith(query))

  def filesNameContains(query: String) = {
    val predicate: String => Boolean = {
      //query is the free variable that is closed over by this closure
      new Function[String, Boolean] {
        override def apply(v1: String): Boolean = v1.contains(query)
      }
    }
    filesMatching(predicate)
  }

  def filesNameRegex(query: String) = filesMatching(_.matches(query))

  def main(args: Array[String]) {
    FunctionClosures.filesEnding(".dll").foreach(println)
    FunctionClosures.filesNameContains("xy").foreach(println)
    FunctionClosures.filesNameRegex(".*a.*b.*").foreach(println)
  }
}
