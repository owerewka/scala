package scala.basics.functions

/**
  * Created by Oskar on 30-06-2015.
  */
object FunctionParameter {

  def oncePerSecond(callback: () => Unit) {
    while (true) {
      callback()
      Thread sleep 1000
    }
  }

  def oncePerSecond(callback: (String) => Unit) {
    while (true) {
      callback("now")
      Thread sleep 1000
    }
  }


  def main(args: Array[String]) {
    oncePerSecond(() => {
      println("time flies like an arrow ...")
    })
    oncePerSecond((msg: String) => {
      println(msg + " time flies like an arrow ...")
    })
  }
}
