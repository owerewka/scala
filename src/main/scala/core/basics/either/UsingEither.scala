package core.basics.either

import util.Util.IntOps

object UsingEither extends App {

  sealed trait Error {
    def message: String
  }
  case class Fatal(message: String) extends Error
  case class EvilValue(message: String, evilValue: Int) extends Error

  val monadicComposition: PartialFunction[Int, Either[Error, String]] = {
    case i if i.isIn(5, 7) => Left(EvilValue(s"Found Evil!", i))
    case i if i.isEven => Right(i + " is even")
    case i if i.isOdd => Right(i + " is odd")
    case _ => Left(Fatal("Congratulations, you found a natural number that is not even or odd!"))
  }

  val result: Seq[Either[Error, String]] = 1 to 10 map monadicComposition

  println(result.mkString("\n"))
}

object MappingEither extends App {

  case class ErrorMessage(message: String)
  case class Data(value: Int)

  val e: Either[ErrorMessage, Data] = Left(ErrorMessage("error"))

  val right: Either[ErrorMessage, String] = e.map(_ => "right bias")
  val left: Either[String, Data] = e.left.map(e => s"There was an error: ${e.message}")

  val left1: Either.LeftProjection[Nothing, Int] = Right(42).left
  println(left1)

  println {
    Right[String, Int](42).map(_ + 100)
  }
  println {
    Left[String, Int]("Error").map(_ + 100)
  }

}