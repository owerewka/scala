package scala.basics

object Variables extends App {

  var value = 1

  //assignment returns a Unit: ()
  println((value += 1).toString)

  //2
  println(value)

}
