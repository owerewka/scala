package core

import util.Test
import java.lang.System.currentTimeMillis
import java.lang.Thread.sleep

object ObjectAsAFunction extends App {

  object EMail extends ((String, String) => String) {
    def apply(user: String, domain: String): String =
      user + "@" + domain
  }

  println {
    List("oskar", "spaces.pl").reduce(EMail)
  }
}

class TheTypeOfAClassCompanionObject extends Test {

  val listObj: List.type = List

  listObj.empty[Int] shouldBe (Nil)
  listObj.range(1, 4) shouldBe List(1, 2, 3)

}

object TopLevelObjectIsASingleton {
  val time: Long = currentTimeMillis()
}

class ObjectApp extends Test {
  val value1: Long = TopLevelObjectIsASingleton.time
  sleep(100)
  val value2: Long = TopLevelObjectIsASingleton.time
  value1 shouldBe value2
}

class LocalObjectIsALazyVal extends Test {
  def test(): Long = {
    object LocalObject {
      val time: Long = currentTimeMillis()
      sleep(100)
    }
    LocalObject.time
  }
  test() should not be test()
}

class TraitCompanionObject extends Test {

  trait Value {
    val a: Int
  }
  object Value {
    def apply(i: Int): Value = new Value {
      override val a: Int = i
    }
  }
  val v: Value = Value(2)

  v.a shouldBe 2

  Seq(1)
}

