package scala.lazyval

trait LazyInstance {

  implicit lazy val list = {
    println("creating lazy val")
    List(1, 2, 3, 4, 5)
  }


}
