package scala.lazyval

object LazyValTest extends App {

  implicit lazy val instance: String = {
    println("I'm creating a new instance!")
    "Instance"
  }

  def usingImplicit(useInstance: Boolean)(implicit instance: String): Unit = {
    if (useInstance) {
      println(s"Instance: $instance")
    } else {
      println("I don't care about the instance.")
    }
  }

  usingImplicit(false)
}
