package core

import scala.language.implicitConversions

object OperatorsAssociativity extends App {

  case class V(s: String) {
    def :@(o: String): String = s"$s $o"
    //methods ending with : are right associative
    def @:(o: String): String = s"$s $o"
  }
  implicit def stringWrapper(s: String): V = V(s)

  println("Left" :@ "Right")
  println("Left" @: "Right")

  println("Left" :@ "Right")
  println("Left" @: "Right")

  println("Left" :@ "Middle" :@ "Right")
  println("Left" @: "Middle" @: "Right")

}

object DefaultOperators extends App {

  println(Nil :+ 1 :+ 2 :+ 3)
  println(1 +: 2 +: 3 +: Nil)
  println(1 :: 2 :: 3 :: Nil)

  println(Nil :+ 1 :+ 2 :+ 3 :+ 4)
  println((((Nil :+ 1) :+ 2) :+ 3) :+ 4)
  println(1 +: 2 +: 3 +: 4 +: Nil)
  println(1 +: (2 +: (3 +: (4 +: Nil))))

}

object ImplicitClass extends App {

  implicit class V(s: String) {
    def :@(o: String): String = s"$s $o"
    def @:(o: String): String = s"$s $o"
  }

  println("Left" :@ "Right")
  println("Left" @: "Right")

}