package sttp

import org.json4s.DefaultFormats
import org.json4s.native.Serialization
import sttp.client3._
import sttp.client3.akkahttp._
import sttp.client3.json4s._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

case class HttpBinResponse(origin: String, headers: Map[String, String])

object Example3App extends App {

  implicit val serialization: Serialization.type = org.json4s.native.Serialization
  implicit val formats: DefaultFormats.type = org.json4s.DefaultFormats

  val request = basicRequest
    .get(uri"https://httpbin.org/get")
    .response(asJson[HttpBinResponse])

  val backend: SttpBackend[Future, Any] = AkkaHttpBackend()

  val response: Future[Response[Either[ResponseException[String, Exception], HttpBinResponse]]] =
    request.send(backend)

  for {
    r <- response
    binResponse <- r.body
  } {
    println(s"Got response code: ${r.code}")
    println(s"Response body: ${r.body}")
    println(s"Body right: $binResponse")
    backend.close()
  }
}

/*

GOOD URL

Got response code: 200
Right(HttpBinResponse(89.70.182.81,Map(Accept-Encoding -> gzip, deflate, Host -> httpbin.org, User-Agent -> akka-http/10.2.2, X-Amzn-Trace-Id -> Root=1-600612e2-1cfcbaea3ed9afbe67885e4b)))

BAD URL

Got response code: 404
Left(sttp.client3.HttpError: statusCode: 404, response: <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<title>404 Not Found</title>
<h1>Not Found</h1>
<p>The requested URL was not found on the server.  If you entered the URL manually please check your spelling and try again.</p>
)

Process finished with exit code 0

 */