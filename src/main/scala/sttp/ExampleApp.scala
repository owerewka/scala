package sttp

import sttp.model.Uri

object ExampleApp extends App {

  import sttp.client3._

  val request: Request[Either[String, String], Any] = basicRequest.get(Uri("https://www.onet.pl/"))

  println(request)

}

