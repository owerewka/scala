package sttp

import scala.util.{Failure, Success, Try}

object RussiaApp extends App {

  import sttp.client3._
  var total = 0L
  while (true) {
    Try {
      val request = basicRequest
        .get(uri"https://tass.com/")
      val backend = HttpURLConnectionBackend()
      val response = request.send(backend)
      response.body.map(_.length).foreach { s =>
        total += s
        println(s"$total $s")
      }
      //      println(response.headers)
    } match {
      case Success(value) => println(value)
      case Failure(exception) => println(exception)
    }
  }

}
