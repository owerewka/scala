import scala.collection.mutable.ListBuffer
object ___LazyValPerformance extends App {

  val const = 1

  val seq = Seq(1, 2, 3)
  def method: Int = seq.size
  lazy val lazyVal: Int = seq.size

  val STEPS = 1e9

  var AVG_LAZY: Long = 0
  var AVG_METHOD: Long = 0

  {
    println("Method")
    val measurements = new ListBuffer[Long]
    (1 to 10).foreach { _ =>
      var counter = 0;
      var sum = 0
      val t1 = System.currentTimeMillis()
      do {
        sum += method
        counter += 1
      } while (counter < STEPS)
      val t2 = System.currentTimeMillis()
      measurements.addOne(t2 - t1)
      println(t2 - t1)
    }
    AVG_METHOD = measurements.sum / measurements.length
    println(s"Average: $AVG_METHOD")
  }
  {
    println("Lazy Val")
    val measurements = new ListBuffer[Long]
    (1 to 10).foreach { _ =>
      var counter = 0;
      var sum = 0
      val t1 = System.currentTimeMillis()
      do {
        sum += lazyVal
        counter += 1
      } while (counter < STEPS)
      val t2 = System.currentTimeMillis()
      measurements.addOne(t2 - t1)
      println(t2 - t1)
    }
    AVG_LAZY = measurements.sum / measurements.length
    println(s"Average: $AVG_LAZY")
  }

  if (AVG_LAZY > AVG_METHOD) {
    val p = ((AVG_LAZY - AVG_METHOD).toDouble / AVG_METHOD) * 100
    val per = f"$p%2.2f"
    println(s"Lazy val was slower by $per%")
  } else {
    val p = ((AVG_METHOD - AVG_LAZY).toDouble / AVG_LAZY) * 100
    val per = f"$p%2.2f"
    println(s"The method was slower by the factor of $per%")
  }
}
