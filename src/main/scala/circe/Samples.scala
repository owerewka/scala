package circe

import io.circe
import io.circe.generic.auto._
import io.circe.parser.decode
import io.circe.syntax._
import io.circe.{Json, JsonObject}

object JsonObjectSerDe extends App {

  case class Something(text: String, parameter: Int)
  val something: Something = Something("Text", 23)
  val jsonObject: JsonObject = something.asJsonObject
  val modifiedJsonObject: JsonObject = jsonObject.add("key", Json.fromString("value"))
  println(decode[Something](modifiedJsonObject.asJson.spaces2))
  println(modifiedJsonObject)
  println(modifiedJsonObject.asJson.as[Something])

}

object CaseClassSerDe extends App {

  case class Request(id: Int, correlationId: String, payload: String)
  val request: Request = Request(3, "cor1", "payload")
  val encoded: String = request.asJson.spaces4
  val decoded: Either[circe.Error, Request] = decode[Request](encoded)
  println(encoded)
  println(decoded)

}

object ListSerDe extends App {

  val encoded: String = List(1, 2, 3).asJson.spaces4
  val decoded: Either[circe.Error, List[Int]] = decode[List[Int]](encoded)
  println(encoded)
  println(decoded)

}