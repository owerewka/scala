package circe

import io.circe
import io.circe.generic.auto._
import io.circe.parser.parse

object Parsing extends App {

  case class Request(id: Int, correlationId: String, payload: String)

  val encoded =
    """{
      |    "id" : 1,
      |    "correlationId" : "c1",
      |    "payload" : "p1"
      |}""".stripMargin

  val result: Either[circe.Error, Request] = for {
    json <- parse(encoded)
    request <- json.as[Request]
  } yield request

  println(result == Right(Request(1, "c1", "p1")))

}
