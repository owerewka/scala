import java.util.UUID
import scala.collection.immutable.HashMap

object Test extends App {

  case class Person(name: String, age: Int, value: Long)

  val john = Person("John", 33, 10)
  val olderJohn = john.copy(age = 34)

  println(s"John       : $john")
  println(s"Older John : $olderJohn")

  List(1, 2, 3).map(_ + 1).foreach(println)


  type AccountId = Int
  type Currency = Int

  sealed abstract class AccountCommand(id: AccountId)
  case class AccountOpening(id: AccountId) extends AccountCommand(id)
  case class Deposit(amount: Currency, id: AccountId) extends AccountCommand(id)
  case class Withdrawal(amount: Currency, id: AccountId) extends AccountCommand(id)
  case class AccountClosure(id: AccountId) extends AccountCommand(id)

  case class Account(balance: Currency = 0) {
    def deposit(amount: Currency): Account = Account(balance + amount)
    def withdrawal(amount: Currency): Account = Account(balance - amount)
  }

  case class Bank(accounts: Map[AccountId, Account] = HashMap()) {
    def transaction(accountCommand: AccountCommand): Bank =
      accountCommand match {
        case AccountOpening(id) => Bank(accounts + (id -> Account()))
        case AccountClosure(id) => Bank(accounts.removed(id))
        case Deposit(amount, id) => Bank(accounts.updatedWith(id)(_.map(_.deposit(amount))))
        case Withdrawal(amount, id) => Bank(accounts.updatedWith(id)(_.map(_.withdrawal(amount))))
      }
  }

  val bank = Bank()
    .transaction(AccountOpening(1))
    .transaction(AccountOpening(2))
    .transaction(Deposit(100, 1))
    .transaction(Deposit(10, 1))
    .transaction(Deposit(200, 2))
    .transaction(Deposit(20, 2))
    .transaction(Withdrawal(5, 1))
    .transaction(Withdrawal(5, 2))

  println(bank)

  println(bank
    .transaction(AccountClosure(1))
    .transaction(AccountClosure(2)))

}
