object ___ForComp extends App {

  for {
    i <- 1 to 2
    j <- 'a' to 'b'
  } {
    println(s"$i $j")
  }

  for (i <- 1 to 2; j <- 'a' to 'b') {
    println(s"$i $j")
  }

}
