package util

import scala.reflect.runtime.universe._

object TypePrinter {

  def printType[T](x: T)(implicit tag: TypeTag[T]): Unit = println(tag)

}