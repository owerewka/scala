package util

object Util {

  implicit class IntOps(i: Int) {
    def times(block: => Unit): Unit = 1 to i foreach { _ => block }
    def isIn(expected: Int*): Boolean = expected.contains(i)
    def isEven: Boolean = i % 2 == 0
    def isOdd: Boolean = i % 2 == 1
  }

  implicit class StringOps(s: String) {
    def isIn(expected: String*): Boolean = expected.contains(s)
  }

  implicit class BooleanOps(b: Boolean) {
    def map[A](`true`: A, `false`: A): A = if (b) `true` else `false`
  }

  def show(a: Any*): Unit = {
    a.foreach(println)
  }
}