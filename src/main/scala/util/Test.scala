package util

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.{EitherValues, OptionValues}

class Test extends AnyFlatSpec with Matchers with OptionValues with EitherValues {

  implicit class IntOps(i: Int) {
    def times(block: => Unit): Unit = 1 to i foreach { _ => block }
    def isIn(in: Int*): Boolean = in.contains(i)
    def isEven: Boolean = i % 2 == 0
    def isOdd: Boolean = i % 2 == 1
  }

  def show(a: Any*): Unit = {
    a.foreach(println)
  }

}
