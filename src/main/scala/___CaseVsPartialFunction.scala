object ___CaseVsPartialFunction extends App {

  println(Map(1 -> "a")
    .filter((p: (Int, String)) => true))

  println(Map(1 -> "a")
    .filter { case (p: (Int, String)) => true })

  val divide: PartialFunction[(Int, String), Boolean] = {
    case _ => true
  }

  println(divide.isInstanceOf[Function1[(Int, String), Boolean]])

  val f = divide

  println(Map(1 -> "a").filter(divide))
  println(Map(1 -> "a").filter(f))

}
