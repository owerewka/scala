import scala.util.matching.Regex

object ___ExtractorsRegex extends App {

  val Decimal: Regex = """(-?)(\d+)(\.\d*)?""".r

  Decimal.findAllIn("-2332.54").foreach(println)

  //each variable pattern bounds to a group in the regexp
  //the groups are designated with parenthesis
  //(-?) - this is the first group
  val Decimal(sign, integerPart, decimalPart) = "-2332.54"
  println(sign)
  println(integerPart)
  println(decimalPart)
}

object SimpleExample extends App {

  val pattern = """(AB)(12)(CD)(34)""".r
  val pattern(a, b, c, d) = "AB12CD34"
  println(a)
  println(b)
  println(c)
  println(d)

}