import shapeless.test.showType
import util.Test

class ___GetType extends Test {

  def foo = 5
  def bar() = 5

  println {
    showType(foo _)
  }
  println {
    showType(bar _)
  }

  case class T(a: Int)
  implicit val o: Ordering[T] = (x: T, y: T) => x.a compareTo y.a
  Seq(T(2), T(1)).sorted
}
