object ___WeirdSelfTypes extends App {

  trait A {
    val a: String = "a"
  }
  trait B { self: A =>
    val b: String = s"b $a"
  }

  //wrong order with val
  val ba: B with A = new B with A
  println(ba.a)
  println(ba.b) //blew up A is not initialized

  val ab: A with B = new A with B
  println(ab.a)
  println(ab.b)
  val c: B with A = ba
  val d: A with B = ab

}

object InitWithLazyVals extends App {

  trait A {
    lazy val a: String = "a"
  }
  trait B { self: A =>
    lazy val b: String = s"b $a"
  }

  val ba: B with A = new B with A
  println(ba.a)
  println(ba.b) //with lazy val A will be initialized

  val ab: A with B = new A with B
  println(ab.a)
  println(ab.b)
  val c: B with A = ba
  val d: A with B = ab

}