package types.higherkinded

trait Container[F[_]] {
  def add[A](a: A): F[A]
}

object Dddd extends App {

  val container: Container[List] = new Container[List] {
    override def add[A](a: A): List[A] = List(a)
  }

  println {
    val value: Seq[Int] = container.add(1)
    value
  }
  println {
    val value: Seq[String] = container.add("a")
    value
  }
  println {
    val value: Seq[Boolean] = container.add(false)
    value
  }
}
