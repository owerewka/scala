package types.higherkinded

trait BatchRun[M[_]] {
  def write[A](a: A, db: M[A]): M[A] = transform(a, db)
  def transform[A](a: A, db: M[A]): M[A]
}

object TestWithList extends App {
  val batchRun = new BatchRun[List] {
    override def transform[A](a: A, db: List[A]): List[A] = db :+ a
  }
  val db: List[String] = List("data 1", "data 2")
  val result: List[String] = batchRun.write("data 3", db)
  println(result)
}

object TestWithSet extends App {
  val batchRun = new BatchRun[Set] {
    override def transform[A](a: A, db: Set[A]): Set[A] = db + a
  }
  var db = Set("data 1", "data 2")
  db = batchRun.write("data 3", db)
  db = batchRun.write("data 4", db)
  db = batchRun.write("data 1", db)
  println(db)
}

object TestWithCustomContainer extends App {
  trait SemiGroup[A] {
    def compose(a1: A, a2: A): A
  }
  case class Container[T](sum: T, count: Int)(implicit val semiGroup: SemiGroup[T]) {
    def append(a: T): Container[T] = Container(semiGroup.compose(sum, a), count + 1)
  }

  val batchRun = new BatchRun[Container] {
    override def transform[A](a: A, db: Container[A]): Container[A] = db.append(a)
  }

  implicit val intAddition: SemiGroup[Int] = (a1: Int, a2: Int) => a1 + a2
  var db = Container[Int](sum = 100, count = 0)
  db = batchRun.write(1, db)
  db = batchRun.write(1, db)
  db = batchRun.write(1, db)
  println(db)
}