package types.higherkinded

/*
 * Higher kinded type as a type class.
 */
object HigherKindedTypeClass extends App {

  trait Storage[F[_]] {
    def isSequential: Boolean
    def get[A](s: F[A], i: Int): A
    def add[A](s: F[A], a: A): F[A]
  }

  implicit val listChecker: Storage[List] = new Storage[List] {
    override def isSequential: Boolean = true
    override def get[A](s: List[A], i: Int): A = s(i)
    override def add[A](s: List[A], a: A): List[A] = s prepended a
  }
  println(listChecker.isSequential)
  println(listChecker.get(List(1), 0))
  println(listChecker.add(List(1), 2))

  implicit val vectorChecker: Storage[Vector] = new Storage[Vector] {
    override def isSequential: Boolean = false
    override def get[A](s: Vector[A], i: Int): A = s(i)
    override def add[A](s: Vector[A], a: A): Vector[A] = s prepended a
  }
  println(vectorChecker.isSequential)
  println(vectorChecker.get(Vector(1), 0))
  println(vectorChecker.add(Vector(1), 2))

  class Printer[F[_] : Storage](c: F[_]) {
    def print(): Unit = println(s"Here is your collection: ${implicitly[Storage[F]].get(c, 0).toString}")
  }

  new Printer(List(96)).print()
  new Printer(Vector(84)).print()

}