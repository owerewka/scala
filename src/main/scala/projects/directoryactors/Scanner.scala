package projects.directoryactors

import akka.actor.{Actor, ActorRef, Props}
import projects.directoryactors.Scanner.{Finished, escape}

import java.io.File

class Scanner(fileCounter: ActorRef) extends Actor {

  var count = new Counter

  override def receive: Receive = {
    case d: Scanner.Directory =>
      //> > self.path.name + " scanning " + d
      val dir = new File(d.dir)
      if (dir.exists) {
        dir.listFiles.foreach(file => {
          val path = file.getAbsolutePath
          val name = escape(file.getAbsolutePath)
          if (file.isDirectory) {
            context.actorOf(Scanner.props(fileCounter), name) ! Scanner.Directory(path)
            count.inc()
          } else {
            context.actorOf(FileProcessor.props(fileCounter), name) ! FileProcessor.File(path)
            count.inc()
          }
        })
      }
    case Finished =>
      if (count.dec()) {
        println(self.path.name + " sendinf Finished to " + context.parent.path.name)
        context.parent ! Finished
      }
  }
}

class Counter {
  var counter = 0

  def inc() {
    counter = counter + 1
    println(s"inc couter $counter")
  }

  def dec(): Boolean = {
    counter = counter - 1;
    println(s"dec counter $counter")
    counter <= 0
  }
}

object Scanner {

  def props(fileCounter: ActorRef): Props = Props(new Scanner(fileCounter))

  case class Directory(dir: String)

  case class Finished()

  def escape(dir: String): String =
    dir.replace('\\', '_')
      .replace(' ', '_')
      .replace('ą', 'a')
      .replace('ę', 'e')
      .replace('ś', 's')
      .replace('ć', 'c')
      .replace('ł', 'l')
      .replace('ż', 'z')
      .replace('ó', 'o')
      .replace('(', '_')
      .replace(')', '_')
      .replace(']', '_')
      .replace('[', '_')
      .replace('ü', 'u')
      .replace('Š', 'S')
      .replace('Ś', 'S')
      .replace('ń', 'n')
      .replace('î', 'i')
      .replace('#', '_')

}