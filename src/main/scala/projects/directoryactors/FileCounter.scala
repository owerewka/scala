package projects.directoryactors

import akka.actor.Actor
import projects.directoryactors.FileCounter.AddFile

class FileCounter extends Actor {

  var count = 0

  override def receive: Receive = {
    case AddFile =>
      count = count + 1
      //> > count
  }
}

object FileCounter {
  val name = "FileCounter"

  case class AddFile()

}