package projects.directoryactors

import akka.actor.{ActorSystem, Props}

object Run extends App {

  val actorSystem = ActorSystem("FileScanningActorSystem")

  actorSystem.actorOf(Props[DirSearch], "DirSearch")

  actorSystem.terminate().wait()

}