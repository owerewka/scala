package projects.directoryactors

import akka.actor.{Actor, ActorRef, Props}

class FileProcessor(fileCounter: ActorRef) extends Actor {

  override def receive: Receive = {
    case f: FileProcessor.File =>
      //> > s"File: $f"
      fileCounter ! FileCounter.AddFile
      //> > self.path.name + " sending finished to " + context.parent.path.name
      context.parent ! Scanner.Finished
  }
}

object FileProcessor {

  case class File(dir: String)

  def props(fileCounter: ActorRef): Props = Props(new FileProcessor(fileCounter))

}