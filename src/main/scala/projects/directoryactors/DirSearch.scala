package projects.directoryactors

import actors.old.Main.system
import akka.actor.{Actor, Props}
import projects.directoryactors.Scanner.{Directory, Finished}

class DirSearch extends Actor {

  val fileCounter = context.system.actorOf(Props[FileCounter], FileCounter.name)
  val scanner = context.actorOf(Scanner.props(fileCounter), "RootScanner")

  //scanner ! Directory("C:\\test")
  scanner ! Directory("D:\\zdjecia")

  override def receive: Receive = {
    case Finished => {
      println("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx")
      system.terminate()
    }
  }
}
