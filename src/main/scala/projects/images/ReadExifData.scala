package projects.images

import com.drew.imaging.ImageMetadataReader
import com.drew.metadata.exif.{ExifDirectoryBase, ExifSubIFDDirectory}
import org.joda.time.DateTime

import java.io.File

object ReadExifData extends App {

  val metadata = ImageMetadataReader.readMetadata(new File("/home/oskar/Dropbox/Camera Uploads/2016-12-20 10.37.36.jpg"))

  val directory = metadata.getFirstDirectoryOfType(classOf[ExifSubIFDDirectory])
  val creationTime = directory.getDate(ExifDirectoryBase.TAG_DATETIME_ORIGINAL)
  println(s"""oryginal: "$creationTime"""")

  val dateTime = new DateTime(creationTime)

  val month: Int = dateTime.getMonthOfYear
  val year: Int = dateTime.getYear
  val day: Int = dateTime.getDayOfMonth
  val hour = dateTime.getHourOfDay

  println(s"year $year")
  println(s"month $month")
  println(s"day $day")
  println(s"hour $hour")

}

//for (tag: Tag <- directory.getTags.asScala) {
//println(s"name: ${tag.getTagName}, type: ${tag.getTagType}, as string: ${tag.toString}")
