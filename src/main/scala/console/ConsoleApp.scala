package console

object ConsoleApp extends App {

  case class Config(
    configServiceUrl: String = "",
    localPath: String = ""
  )

  import scopt.OParser
  val builder = OParser.builder[Config]
  val parser = {
    import builder._
    OParser.sequence(
      programName("Config Comparator"),
      opt[String]("local")
        .action((value, c) => c.copy(localPath = value))
        .text("Path to Local Configuration Files")
        .optional(),
      opt[String]("url")
        .action((value, c) => c.copy(configServiceUrl = value))
        .text("Config Service Url")
        .optional(),
      checkConfig(config => Left("a chance to do something else"))
    )
  }

  OParser.parse(parser, args, Config()) match {
    case Some(config) => println(s"Success: $config")
    case _ => println("Arguments are bad, error message will have been displayed")
  }

}
