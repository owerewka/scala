package console

object ConsoleExampleApp extends App {

  import java.io.File
  case class Config(
    foo: Int = -1,
    out: File = new File("."),
    xyz: Boolean = false,
    libName: String = "",
    maxCount: Int = -1,
    verbose: Boolean = false,
    debug: Boolean = false,
    mode: String = "",
    files: Seq[File] = Seq(),
    keepalive: Boolean = false,
    jars: Seq[File] = Seq(),
    kwargs: Map[String, String] = Map())

  import scopt.OParser
  val builder = OParser.builder[Config]
  val parser1 = {
    import builder._
    OParser.sequence(
      programName("scopt"),
      head("scopt", "4.x"),
      // option -f, --foo
      opt[Int]('f', "foo")
        .action((x, c) => c.copy(foo = x))
        .text("foo is an integer property"),
      // more options here...
    )
  }

  // OParser.parse returns Option[Config]
  OParser.parse(parser1, args, Config()) match {
    case Some(config) => println(s"Success: $config")
    case _ => println("Arguments are bad, error message will have been displayed")
  }

}
