package stalatest

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers
import org.scalatest.prop.TableDrivenPropertyChecks

class ParametrizedTest extends AnyFlatSpec with Matchers with TableDrivenPropertyChecks {

  it should "run as parametrized test" in {
    //given
    val buckets = Table(
      ("header", "prepared", "standardServing", "standardWeightOrVolume"),
      ("(baked as directed) Per 100g (baked)", true, false, true),
      ("(boiled) per 100g", true, false, true),
      ("Baked", true, false, false),
      ("Per 100   mL = 2x scoop**", false, false, true)
    )
    //then
    forAll(buckets) { (rawHeader: String, prepared: Boolean, standardServing: Boolean, standardWeightOrVolume: Boolean) =>
      //when
      val header = Header(rawHeader).asInstanceOf[BucketHeader]
      header.isPrepared shouldBe prepared
      header.hasStandardServing shouldBe standardServing
      header.hasStandardWeightOrVolume shouldBe standardWeightOrVolume
    }
  }

}
