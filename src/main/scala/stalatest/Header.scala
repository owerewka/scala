package stalatest

sealed trait Header {
  def text: String
}

case class BucketHeader(isPrepared: Boolean, hasStandardServing: Boolean, hasStandardWeightOrVolume: Boolean, hasServing: Boolean) extends Header {
  override def text: String = {
    val when = if (isPrepared) "As Prepared" else "As Sold"

    if (hasStandardServing)
      "Per 100g/100ml Serving " + when
    else if (hasStandardWeightOrVolume)
      "Per 100g/100ml " + when
    else if (hasServing)
      "Per Serving " + when
    else
      when + " Not Covered"
  }
}

case class SkippedHeader() extends Header {
  override val text: String = "Skipped"
}

case class RawHeader(text: String) extends Header

object Header {
  def apply(header: String): Header = {
    if (shouldSkip(header)) SkippedHeader()
    else BucketHeader(isPrepared(header), hasStandardServing(header), hasStandardWeightOrVolume(header), hasServing(header))
  }

  private def shouldSkip(header: String): Boolean = matchAny(
    header.toLowerCase,
    List("ri", "gda", "dri", "rda"),
    words = true
  ) || matchAny(
    header.toLowerCase,
    List("%", raw"\bref(erence)?\b.*\bintake\b")
  )

  private def isPrepared(header: String): Boolean = matchAny(
    header.toLowerCase,
    List(
      "directed", "boiled", "baked", "consumed", "fried", "grilled", "instruction", "microwave",
      "oven", "roasted", "drained", "prepared", "cooked", "diluted", "prapared"
    ),
    words = true
  )

  private def hasStandardServing(header: String): Boolean = {
    matchAny(header.toLowerCase, List(raw"\(100\s*g\)", raw"\(100\s*ml\)")) ||
      (matchAny(header.toLowerCase, List("serving", "portion", "pack")) && hasStandardWeightOrVolume(header))
  }

  private def hasStandardWeightOrVolume(header: String): Boolean = matchAny(
    header.toLowerCase.replaceAll("\\s", ""),
    List("100g", "100ml")
  )

  private def hasServing(header: String): Boolean = matchAny(
    header.toLowerCase,
    List("each", "per", "one", "daily", "serving"),
    words = true
  ) || matchAny(
    header,
    List(raw"\d+")
  )

  private def matchAny(txt: String, regexes: Iterable[String], words: Boolean = false): Boolean = {
    val patterns = if (words) regexes.map(pattern => ("\\b" + pattern + "\\b").r) else regexes.map(_.r)
    patterns.exists(_.findFirstIn(txt).nonEmpty)
  }
}