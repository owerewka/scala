package fp.parallel

import java.util.concurrent.{ExecutorService, Executors}

object WithExecutor {
  def withExecutor(block: => ExecutorService => Any): Unit = {
    val executor: ExecutorService = Executors.newFixedThreadPool(20)
    block(executor)
    executor.shutdownNow()
  }
}

