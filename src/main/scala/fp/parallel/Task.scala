package fp.parallel

import fp.parallel.WithExecutor.withExecutor
import util.Test
import util.Util._
import java.lang.Thread.sleep
import java.util.concurrent.Future
import scala.collection.mutable
import scala.collection.mutable.ListBuffer

sealed trait `Type`
case object Start extends `Type`
case object Finish extends `Type`
case object Count extends `Type`

sealed class Execution {
  def andThen(e: => Execution): Execution = this match {
    case m@MultipleEvaluation(_) => m
    case _ => e
  }
}
case object SingleEvaluation extends Execution
case class MultipleEvaluation(stack: String) extends Execution
case class Parallel(stack: String) extends Execution
case class Sequential(stack: String) extends Execution

case class Log[A](a: A, `type`: `Type`, time: Long = System.currentTimeMillis() % 60000)
class Logs[A](count: Int) {
  def execution: Execution = evaluationCount.andThen(isParallel.map(Parallel(toString), Sequential(toString)))
  def evaluationCount: Execution = forType(Count).map(_.a).groupBy(identity).map(_._2.length).forall(_ == count)
    .map(SingleEvaluation, MultipleEvaluation(s"Tasks were executed more than once!\n$toString"))
  override def toString: String = logs.mkString("\n")
  protected def log(log: Log[A]): Unit = logs.synchronized(logs.addOne(log))
  private def isParallel: Boolean = forType(Start).map(_.time).max < forType(Finish).map(_.time).min
  private val logs: mutable.ListBuffer[Log[A]] = new ListBuffer[Log[A]]()
  private def forType(`type`: Type): List[Log[A]] = logs.filter(_.`type` == `type`).toList

}

class Tasks[A](delay: Int = 10, count: Int = 10) extends Logs[A](count) {
  def task(a: A): A = {
    println(s"Started task for $a")
    log(Log(a, Start))
    count times {
      sleep(delay)
      println(s"Processing $a")
      log(Log(a, Count))
    }
    println(s"Finished task for $a")
    log(Log(a, Finish))
    a
  }
  def apply(a: A): A = task(a)
}
object Tasks {
  def apply[A](): Tasks[A] = new Tasks()
}

class TasksTest extends Test {

  "Tasks" should "verify parallel execution" in {
    withExecutor { implicit es =>
      val tasks = Tasks[String]()

      val f1: Future[String] = es.submit(() => tasks.task("A"))
      val f2: Future[String] = es.submit(() => tasks.task("B"))
      f1.get()
      f2.get()

      tasks.execution shouldBe a[Parallel]
    }
  }

  it should "verify sequential execution" in {
    val tasks = Tasks[Int]()

    tasks.task(1)
    tasks.task(2)

    tasks.execution shouldBe a[Sequential]
  }

  it should "verify multiple evaluations" in {
    val tasks = Tasks[String]()

    tasks.task("same task id")
    tasks.task("same task id")

    tasks.execution shouldBe a[MultipleEvaluation]
  }
}