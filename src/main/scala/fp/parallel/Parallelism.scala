package fp.parallel

import fp.parallel.WithExecutor.withExecutor
import util.Test
import util.Util._
import java.util.concurrent._

object Parallelism extends App {

  type Par[A] = ExecutorService => Future[A]

  // all that you do here is construction of a function
  // that takes an executor service and returns a future
  // this function might

  object Par {
    private case class UnitFuture[A](get: A) extends Future[A] {
      override def isDone: Boolean = true
      override def get(timeout: Long, unit: TimeUnit): A = get
      override def isCancelled: Boolean = false
      override def cancel(mayInterruptIfRunning: Boolean): Boolean = false
    }
    def run[A](a: Par[A])(implicit es: ExecutorService): Future[A] = a(es)
    def fork[A](a: => Par[A]): Par[A] = es => {
      println(s"Submitting new task $a to the executor!")
      es.submit(() => a(es).get)
    }
    def unit[A](a: => A): Par[A] = _ => UnitFuture(a)

    def mapPar[A, B](pa: Par[A])(f: A => B): Par[B] = es => UnitFuture(f(pa(es).get()))
    def map2[A, B, C](a: Par[A], b: Par[B])(f: (A, B) => C): Par[C] = es => {
      val fa: Future[A] = a(es)
      val fb: Future[B] = b(es)
      UnitFuture(f(fa.get, fb.get))
    }

    def lazyUnit[A](a: => A): Par[A] = fork(unit(a))
    def asyncF[A, B](f: A => B): A => Par[B] = a => lazyUnit(f(a))

    def function[A]: (List[A], A) => List[A] = (t, h) => t.prepended(h)

    def sequenceBalanced[A](as: IndexedSeq[Par[A]]): Par[IndexedSeq[A]] = fork {
      if (as.isEmpty) unit(Vector())
      else if (as.length == 1) mapPar(as.head)(a => Vector(a))
      else {
        val (l, r) = as.splitAt(as.length / 2)
        map2(sequenceBalanced(l), sequenceBalanced(r))(_ ++ _)
      }
    }

    def sequence[A](as: List[Par[A]]): Par[List[A]] =
      mapPar(sequenceBalanced(as.toIndexedSeq))(_.toList)

    def sequenceSimple[A](ps: List[Par[A]]): Par[List[A]] = {
      //for whatever the reason it won't run in parallel with foldLeft
      ps.foldRight[Par[List[A]]](unit(Nil))((h, t) => map2(h, t)(_ :: _))
    }

    def parMap[A, B](as: List[A])(f: A => B): Par[List[B]] = fork {
      val fbs: List[Par[B]] = as.map(asyncF(f))
      sequence(fbs)
    }

    // each call to the predicate should happen on a separate thread
    // this means that every predicate call must be wrapped in its own
    // Par
    def parFilter[A](as: List[A])(p: A => Boolean): Par[List[A]] = fork {
      val filtered: List[Par[Option[A]]] = as.map(asyncF(Option(_).filter(p)))
      val par: Par[List[Option[A]]] = sequence(filtered)
      mapPar(par)(_.flatten)
    }

    def sortPar(l: Par[List[Int]]): Par[List[Int]] = fork(mapPar(l)(_.sorted))
  }
}

class Tests extends Test {

  import fp.parallel.Parallelism.Par
  import fp.parallel.Parallelism.Par._

  // this: List(task("A")) will instantly run the task blocking the calling thread
  // as long as task is executing, because List parameters are call by value

  "Par" should "parFilter parallelising evaluation of the filter function" in {
    withParallel[String] { tasks =>
      withExecutor { implicit es =>
        val l = List("A", "B", "C", "D")
        val predicate: String => Boolean = s => tasks(s).isIn("B", "C")
        val function: Par[List[String]] = parFilter(l)(predicate)
        Par.run(function).get() shouldBe List("B", "C")
      }
    }
  }

  it should "map parallelising evaluation of the elements of the list" in {
    withParallel[String] { tasks =>
      withExecutor { implicit es =>
        val l = List(() => tasks.task("A"), () => tasks("B"), () => tasks("C"), () => tasks("D"))
        val function: Par[List[String]] = parMap(l)(_ () * 3)
        Par.run(function).get() shouldBe List("AAA", "BBB", "CCC", "DDD")
      }
    }
  }

  it should "map paralleling the execution of the map function itself" in {
    withParallel[String] { tasks =>
      withExecutor { implicit es =>
        val l = List("A", "B", "C", "D")
        val function: Par[List[String]] = parMap(l)(tasks(_) * 3)
        Par.run(function).get() shouldBe List("AAA", "BBB", "CCC", "DDD")
      }
    }
  }

  it should "submit lazy tasks" in {
    withParallel[String] { tasks =>
      withExecutor { implicit es =>
        val function: Par[List[String]] =
          sequence(
            List(
              unit(tasks("A")),
              unit(tasks("B")),
              unit(tasks("C"))
            ))
        Par.run(function).get() shouldBe List("A", "B", "C")
      }
    }
  }

  it should "evaluate the task only once" in {
    withParallel[String] { tasks =>
      withExecutor { implicit es =>
        lazyUnit(tasks("C"))(es).get()
      }
      tasks.evaluationCount shouldBe a[SingleEvaluation.type]
    }
  }

  it should "run a sample transformation" in {
    withParallel[String] { tasks =>
      withExecutor { implicit es =>
        val function: Par[String] =
          map2(
            lazyUnit(tasks("A")),
            map2(
              lazyUnit(tasks("B")),
              asyncF[String, String](tasks(_) * 10)("X")
            )(_ + _)
          )(_ + _)
        Par.run(function).get() shouldBe "ABXXXXXXXXXX"
      }
    }
  }

  it should "have sorted for Lists" in {
    withExecutor { implicit es =>
      val l: Par[List[Int]] = unit(List(3, 4, 2, 1))
      Par.run(sortPar(l)).get() shouldBe List(1, 2, 3, 4)
    }
  }

  def withParallel[A](block: Tasks[A] => Unit): Unit = {
    val tasks = Tasks[A]()
    block(tasks)
    tasks.execution shouldBe a[Parallel]
  }

}
