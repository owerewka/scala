package fp.continuation.par

import fp.continuation.par.Impl.Par
import fp.continuation.par.Impl.Par._
import fp.continuation.par.Runner.withExecutor
import fp.continuation.par.Task.heavy
import fp.continuation.par.Util.___
import java.util.concurrent.CompletableFuture.completedFuture
import java.util.concurrent._

object Impl {

  type Par[A] = ExecutorService => Future[A]

  object Par {

    def unit[A](a: A): Par[A] =
      (_: ExecutorService) => UnitFuture(a)

    def map2[A, B, C](a: Par[A], b: Par[B])(f: (A, B) => C): Par[C] =
      (es: ExecutorService) => {
        val fa: Future[A] = a(es)
        val fb: Future[B] = b(es)
        UnitFuture(f(fa.get(), fb.get()))
      }

    def fork[A](a: => Par[A]): Par[A] =
      (es: ExecutorService) =>
        es.submit(
          () => a(es).get()
        )

    def lazyUnit[A](a: => A): Par[A] =
      fork(unit(a))

    def run[A](es: ExecutorService)(a: Par[A]): Future[A] =
      a(es)

    def asyncF[A, B](f: A => B): A => Par[B] =
      (a: A) => lazyUnit(f(a))

    def map[A, B](para: Par[A])(f: A => B): Par[B] =
      (es: ExecutorService) =>
        UnitFuture(f(para(es).get()))

    def sort[A](parList: Par[List[Int]]): Par[List[Int]] =
      map(parList)(_.sorted)

    def sequenceByMap2[A](l: List[Par[A]]): Par[List[A]] =
      l.foldRight(unit(List[A]()))((h, t) => map2(h, t)(_ :: _))

    def sequencePrimitive[A](l: List[Par[A]]): Par[List[A]] =
      (es: ExecutorService) => UnitFuture(l.map(_ (es)).map(_.get()))

    def parMap[A, B](as: List[A])(f: A => B): Par[List[B]] =
      fork {
        val l: List[Par[B]] = as.map(asyncF(a => f(a)))
        sequencePrimitive(l)
      }

    def parFilter[A](as: List[A])(f: A => Boolean): Par[List[A]] =
      fork {
        val l: List[Par[Option[A]]] = as.map(asyncF(Some(_).filter(f)))
        map(sequencePrimitive(l))(_.flatten)
      }

    def parFold[A](ints: IndexedSeq[A])(z: A)(f: (A, A) => A): Par[A] =
      if (ints.size <= 1) {
        Par.unit(ints.headOption getOrElse z)
      } else {
        val (l, r) = ints.splitAt(ints.length / 2)
        map2(
          fork(parFold(l)(z)(f)),
          fork(parFold(r)(z)(f))
        )(f)
      }

    def paragraph[A](l: List[A])(f: A => Int): Par[Int] = {
      val value = parMap(l)(f)
      map(value)(_.sum)
    }

    def map3[A, B, C, D](a: Par[A], b: Par[B], c: Par[C])(f: (A, B, C) => D): Par[D] = {
      val x = (a: A, b: B) => f(a, b, _: C)
      val value: Par[C => D] = fork(map2(a, b)(x))
      map2(value, c)((cd: C => D, c) => cd(c))
    }

    def map3p[A, B, C, D](fa: Par[A], fb: Par[B], fc: Par[C])(f: (A, B, C) => D): Par[D] =
      (es: ExecutorService) => {
        val a: Future[A] = fa(es)
        val b: Future[B] = fb(es)
        val c: Future[C] = fc(es)
        completedFuture(f(a.get(), b.get(), c.get()))
      }

    def chooser[A, B](pa: Par[A])(choices: A => Par[B]): Par[B] =
      (es: ExecutorService) => {
        val a: A = pa(es).get()
        choices(a)(es)
      }

    def choiceN[A](n: Par[Int])(choices: List[Par[A]]): Par[A] =
      chooser(n)(choices(_))

    def choice[A](cond: Par[Boolean])(t: Par[A], f: Par[A]): Par[A] =
      chooser(cond)(b => if (b) t else f)

    def join[A](a: Par[Par[A]]): Par[A] =
      (es: ExecutorService) =>
        a(es).get()(es)
  }
}

object DeadlockTheThreadPoolApp extends App {
  withExecutor { es =>
    // TODO: implement a case that will deadlock the threadpool
  }
}

object JoinApp extends App {
  withExecutor { es =>
    val a: Par[String] = lazyUnit(heavy("A"))
    val b: Par[Par[String]] = lazyUnit(heavy(a))
    val par: Par[String] = join(b)
    ___
    par(es).get()
  }
}

object ChoiceNApp extends App {
  withExecutor { es =>
    val choice: Par[Int] = lazyUnit(heavy(1))
    val choices: List[Par[String]] = List(
      lazyUnit(heavy("A")),
      lazyUnit(heavy("B")),
      lazyUnit(heavy("C")))
    val par = choiceN(choice)(choices)
    println("___")
    par(es).get()
  }
}

object ChoiceApp extends App {
  withExecutor { es =>
    val selector: Par[Boolean] = lazyUnit(heavy(true))
    val success: Par[String] = lazyUnit(heavy("SUCCESS"))
    val failure: Par[String] = lazyUnit(heavy("FAILURE"))
    val par: Par[String] = choice(selector)(success, failure)
    ___
    par(es).get()
  }
}

object TestingApp extends App {
  def test[A](par1: Par[A], par2: Par[A]): Unit = {
    withExecutor { es =>
      val v1 = par1.apply(es).get()
      val v2 = par2.apply(es).get()
      assert(v1 == v2)
    }
  }
  test(
    map(unit(1))(_ + 1),
    unit(2)
  )
}

object AsyncFTestApp extends App {
  withExecutor { es =>
    val f: String => String = a => heavy(a.toUpperCase)
    val g: String => Par[String] = asyncF(f)
    val h: Par[String] = g("ala ma kota")
    ___
    h(es).get()
  }
}

object ParMapSS extends App {
  withExecutor { es =>
    val par = map3p(
      fork(unit(heavy("a"))),
      fork(unit(heavy(1))),
      fork(unit(heavy(true)))
    )(_ + _ + _)
    ___
    par(es).get()
  }
}

object ParMapsApp extends App {
  withExecutor { es =>
    val par = map2(
      lazyUnit(heavy(1)),
      lazyUnit(heavy(2)))(_ + _)
    par(es).get()
  }
}

object ParagraphApp extends App {
  withExecutor { es =>
    val l = List("ala ma kota", "kot ma", "Ale")
    val f: String => Int = a => heavy(a.split(" ").length)
    val par = paragraph(l)(f)
    ___
    par(es).get()
  }
}

object ParSumHeavyApp extends App {
  withExecutor { es =>
    val v = Vector("a", "b", "c", "d", "e", "a", "b", "c", "d", "e", "a", "b", "c", "d", "e")
    val f: (String, String) => String = (a, b) => heavy(a + b)
    val par = parFold(v)("")(f)
    ___
    par(es).get()
  }
}

object ParSumApp extends App {
  withExecutor { es =>
    val v = Vector(1, 2, 3, 4, 5)
    val f: (Int, Int) => Int = _ + _
    val par = parFold(v)(0)(f)
    ___
    par(es).get()
  }
  withExecutor { es =>
    val v = Vector()
    val f: (String, String) => String = _ + _
    val par = parFold(v)("empty")(f)
    ___
    par(es).get()
  }
}

object ParFilterApp extends App {
  withExecutor { es =>
    val data: List[Int] = (1 to 50000).toList
    val f: Int => Boolean = _ > 2
    val par: Par[List[Int]] = parFilter(data)(a => heavy(f(a)))
    ___
    par(es).get()
  }
}

object ParMapApp extends App {
  withExecutor { es =>
    val data: List[Int] = (1 to 3).toList
    val par: Par[List[Int]] = parMap(data)(heavy(_))
    ___
    par(es).get()
  }
}

object SequenceApp extends App {
  withExecutor { es =>
    val par1: List[Par[String]] = List(lazyUnit(heavy("A")), lazyUnit(heavy("B")), lazyUnit(heavy("C")))
    val par2 = sequenceByMap2(par1)
    ___
    par2(es).get()
  }
}

object AsyncFApp extends App {
  withExecutor { es =>
    val f: String => String = heavy(_).toUpperCase()
    val par: Par[String] = asyncF(f).apply("Lazy Lift")
    ___
    par(es).get()
  }
}

object SortApp extends App {
  //this is sequential because sort and embedded in it map
  //causes waiting on get
  withExecutor { es =>
    val par1: Par[List[Int]] = fork(unit(heavy(List(4, 2, 3, 1))))
    val par2: Par[String] = map2(sort(par1), sort(par1))(_ + " " + _)
    ___
    par2(es).get()
  }
  //parallel, just move the sort inside the fork,
  //fork will return immediately, allowing the other task to be submitted
  withExecutor { es =>
    val par1: Par[List[Int]] = fork(sort(unit(heavy(List(4, 2, 3, 1)))))
    val par2 = map2(par1, par1)(_ + " " + _)
    ___
    par2(es).get()
  }
}

object UnitsApp extends App {
  withExecutor { es =>
    val cons: Par[String] = unit("Cheap Unit")
    val left: Par[String] = lazyUnit(heavy("Lazy Left"))
    val right: Par[String] = lazyUnit(heavy("Lazy Right"))
    ___
    val par =
      map2(left,
        map2(right,
          cons
        )(_ + " " + _)
      )(_ + " " + _)
    par(es).get()
  }
}

object Util {
  def ___ : Unit = println("___")
}