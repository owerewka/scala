package fp.continuation.par

import java.lang.Thread.sleep

object Task {
  def heavy[A](a: A, delay: Int = 250, count: Int = 10): A = {
    (1 to count).foreach { i =>
      sleep(delay)
      println(s"$a: $i/$count")
    }
    a
  }
}