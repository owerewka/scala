package fp.continuation.par

import java.util.concurrent._

case class UnitFuture[A](a: A) extends Future[A] {
  override def cancel(mayInterruptIfRunning: Boolean): Boolean = false
  override def isCancelled: Boolean = false
  override def isDone: Boolean = true
  override def get(): A = a
  override def get(timeout: Long, unit: TimeUnit): A = a
}
