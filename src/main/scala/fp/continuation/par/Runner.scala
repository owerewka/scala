package fp.continuation.par

import java.util.concurrent.TimeUnit.MILLISECONDS
import java.util.concurrent._

object Runner {

  def withExecutor(block: ExecutorService => Any): Unit = {

    val es: ThreadPoolExecutor =
      new ThreadPoolExecutor(
        20,
        20,
        10, MILLISECONDS,
        new LinkedBlockingQueue[Runnable] {})

    val ess = Executors.newScheduledThreadPool(1)
    ess.scheduleAtFixedRate(
      () => println(s"active threads: ${es.getActiveCount}"),
      100,
      500, MILLISECONDS)

    try {
      println {
        block(es)
      }
    } finally {
      es.shutdown()
      //  es.awaitTermination(1,SECONDS)
      ess.shutdownNow()
    }
  }

}