package fp.continuation.par

import fp.continuation.par.Impl.Par
import fp.continuation.par.Impl.Par._
import fp.continuation.par.Runner.withExecutor
import fp.continuation.par.Task.heavy
import java.util.concurrent._

object ParMapHellApp {

  type Par[A] = ExecutorService => Future[A]

  object Par {

    def unit[A](a: A): Par[A] = (_: ExecutorService) => UnitFuture(a)

    def map2[A, B, C](a: Par[A], b: Par[B])(f: (A, B) => C): Par[C] =
      (es: ExecutorService) => {
        val fa: Future[A] = a(es)
        val fb: Future[B] = b(es)
        UnitFuture(f(fa.get(), fb.get()))
      }

    def fork[A](a: => Par[A]): Par[A] =
      (es: ExecutorService) => es.submit(() => a(es).get())

    def lazyUnit[A](a: => A): Par[A] =
      fork(unit(a))

    def asyncF[A, B](f: A => B): A => Par[B] =
      (a: A) => lazyUnit(f(a))

    def sequenceByMap2[A](l: List[Par[A]]): Par[List[A]] =
      l.foldRight(unit(List[A]()))((h, t) => map2(h, t)(_ :: _))

    def sequencePrimitive[A](l: List[Par[A]]): Par[List[A]] =
      (es: ExecutorService) => UnitFuture(l.map(_ (es)).map(_.get()))

    def parMap[A, B](as: List[A])(f: A => B): Par[List[B]] =
      fork {
        val l: List[Par[B]] = as.map(asyncF(a => f(a)))
        sequencePrimitive(l)
      }
  }
}

object ParMapDemoApp extends App {
  withExecutor { es =>
    val data: List[Int] = (1 to 3).toList
    val par: Par[List[Int]] = parMap(data)(heavy(_))
    println("___")
    par(es).get()
  }
}
