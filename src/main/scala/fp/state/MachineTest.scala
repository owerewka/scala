package fp.state

import Machine._
import util.Test

//noinspection DuplicatedCode,NameBooleanParameters
class MachineTest extends Test {

  "The Machine" should "do nothing out of candy" in {
    simulate(Coin).a(Machine(Closed, 0, 0)) shouldBe(0, 0)
    simulate(Turn).a(Machine(Closed, 0, 0)) shouldBe(0, 0)
    simulate(Coin, Turn).a(Machine(Closed, 0, 0)) shouldBe(0, 0)
    simulate(Turn, Coin).a(Machine(Closed, 0, 0)) shouldBe(0, 0)
    simulate(Coin, Coin, Coin).a(Machine(Closed, 0, 0)) shouldBe(0, 0)
    simulate(Turn, Turn, Turn).a(Machine(Closed, 0, 0)) shouldBe(0, 0)
  }

  it should "give one candy when already open" in {
    simulate(Turn).a(Machine(Open, 2, 0)) shouldBe(1, 0)
  }

  it should "sell one candie" in {
    simulate(Coin, Turn)
      .a(Machine(Closed, 5, 0)) shouldBe(4, 1)
  }

  it should "sell two candies" in {
    simulate(Coin, Turn, Turn, Coin, Coin, Turn)
      .a(Machine(Closed, 20, 10)) shouldBe(18, 12)
  }

  it should "sell all candies" in {
    simulate(Coin, Turn, Coin, Turn, Coin, Turn)
      .a(Machine(Closed, 3, 0)) shouldBe(0, 3)
  }

}