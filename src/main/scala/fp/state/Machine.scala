package fp.state

object Machine extends App {

  trait Lock
  case object Closed extends Lock
  case object Open extends Lock

  type Coins = Int
  type Candies = Int

  sealed trait Action
  case object Coin extends Action
  case object Turn extends Action

  case class Machine(lock: Lock, candies: Candies, coins: Coins)

  def simulate(actions: Action*): State[Machine, (Candies, Coins)] = {
    State((machine: Machine) => {
      val m: Machine = actions.foldLeft(machine) { (machine, action) =>
        (action, machine) match {
          case (_, m@Machine(_, 0, _)) => m
          case (Coin, m@Machine(Open, _, _)) => m
          case (Turn, m@Machine(Closed, _, _)) => m
          case (Coin, Machine(Closed, candies, coins)) => Machine(Open, candies, coins + 1)
          case (Turn, Machine(Open, candies, coins)) => Machine(Closed, candies - 1, coins)
        }
      }
      (m, (m.candies, m.coins))
    })
  }

}
