package fp.state

import scala.annotation.tailrec
import scala.language.postfixOps

trait RNG {
  def nextInt: (Int, RNG)
}

object RNG {

  //this type describes a state transition function
  //it takes old state, does something (produces A)
  //and returns the new state
  type Rand[+A] = RNG => (A, RNG)

  case class Simple(seed: Long) extends RNG {
    override def nextInt: (Int, RNG) = {
      val newSeed = (seed * 0x5DEECE66DL + 0xBL) & 0xFFFFFFFFFFFFL
      val nextRNG = Simple(newSeed)
      val n = (newSeed >>> 16).toInt
      (n, nextRNG)
    }
  }

  def nonNegativeInt: Rand[Int] =
    rng => {
      val (i, r) = rng.nextInt
      (if (i < 0) -(i + 1) else i, r)
    }

  def double: Rand[Double] = map(nonNegativeInt)(
    i => i / (Integer.MAX_VALUE.toDouble + 1))

  def intDouble: Rand[(Int, Double)] = both(int, double)
  def doubleInt: Rand[(Double, Int)] = both(double, int)

  def double3(rng: RNG): ((Double, Double, Double), RNG) = {
    val (d1, r1) = double(rng)
    val (d2, r2) = double(r1)
    val (d3, r3) = double(r2)
    ((d1, d2, d3), r3)
  }

  def ints(count: Int)(rng: RNG): (List[Int], RNG) = {
    @tailrec
    def go(n: Int, rng: RNG, acc: List[Int]): (List[Int], RNG) =
      if (n > 0) {
        val (i, r) = nonNegativeInt(rng)
        go(n - 1, r, i :: acc)
      } else (acc, rng)
    go(count, rng, Nil)
  }

  val int: Rand[Int] = _.nextInt

  def unit[A](a: A): Rand[A] = rnd => (a, rnd)

  def map[A, B](s: Rand[A])(f: A => B): Rand[B] =
    flatMap(s)(a => unit(f(a)))

  def mapStandard[A, B](s: Rand[A])(f: A => B): Rand[B] = rng => {
    val (a, r) = s(rng)
    (f(a), r)
  }

  def nonNegativeEven: Rand[Int] =
    map(nonNegativeInt)(i => i - i % 2)

  def map2Standard[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] =
    rng => {
      val (a, r1) = ra(rng)
      val (b, r2) = rb(r1)
      (f(a, b), r2)
    }

  def map2[A, B, C](ra: Rand[A], rb: Rand[B])(f: (A, B) => C): Rand[C] =
    flatMap(ra) { a =>
      map(rb) { b =>
        f(a, b)
      }
    }

  def both[A, B](ra: Rand[A], rb: Rand[B]): Rand[(A, B)] =
    map2(ra, rb)((_, _))

  def sequence[A](fs: List[Rand[A]]): Rand[List[A]] = {
    @tailrec
    def go(fs: List[Rand[A]], rng: RNG, acc: List[A]): (List[A], RNG) =
      fs match {
        case h :: t =>
          val (a, r) = h(rng)
          go(t, r, a :: acc)
        case _ => (acc.reverse, rng)
      }
    go(fs, _, Nil)
  }

  def flatMap[A, B](f: Rand[A])(g: A => Rand[B]): Rand[B] =
    (r1: RNG) => {
      val (a: A, r2: RNG) = f(r1)
      g(a).apply(r2)
    }

}
