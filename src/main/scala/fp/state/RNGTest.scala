package fp.state

import fp.state.RNG._
import util.Test

class RNGTest extends Test {

  "An RNG" should "always return the same value" in {
    val rng: Simple = Simple(1)
    rng.nextInt._1 shouldBe 384748
    rng.nextInt._1 shouldBe 384748
    rng.nextInt._1 shouldBe 384748
  }

  it should "also return a next step RNG" in {
    val (r1, rng1) = Simple(5).nextInt
    val (r2, rng2) = rng1.nextInt
    val (r3, rng3) = rng2.nextInt
    r1 shouldBe 1923744
    r2 shouldBe -1478223346
    r3 shouldBe 832832900
    rng1 should not be theSameInstanceAs(rng2)
    rng2 should not be theSameInstanceAs(rng3)
    rng3 should not be theSameInstanceAs(rng1)
  }

  it should "have nonNegative" in {
    //this test in the current code would require mocking
    nonNegativeInt(Simple(10)) shouldBe
      (3847489, Simple(252149039181L))
  }

  it should "have double" in {
    val (d, rng) = double(Simple(15))
    d shouldBe 0.002687439788132906
    rng shouldBe Simple(378223558766L)
  }

  it should "have intDouble" in {
    val rng = Simple(20)
    val ((i, d), r) = intDouble(rng)
    i shouldBe 7694978
    d shouldBe 0.7593179126270115
    r shouldBe Simple(174610480805614L)
  }

  it should "have doubleInt" in {
    val rng = Simple(20)
    val ((d, i), r) = doubleInt(rng)
    d shouldBe 0.003583253361284733
    i shouldBe -1630622802
    r shouldBe Simple(174610480805614L)
  }

  it should "have double3" in {
    val rng = Simple(25)
    val ((d1, d2, d3), r) = double3(rng)
    d1 shouldBe 0.004479066468775272
    d2 shouldBe 0.5503599112853408
    d3 shouldBe 0.3939578104764223
    r shouldBe Simple(226030343917290L)
  }

  it should "have ints" in {
    ints(count = 0)(Simple(30)) shouldBe
      (List(), Simple(30))
    ints(count = 1)(Simple(35)) shouldBe
      (List(13466211), Simple(882521637106L))
    ints(count = 2)(Simple(40)) shouldBe
      (List(1029489455, 15389956), Simple(67468620957474L))
  }

  it should "have int" in {
    int(Simple(45)) shouldBe
      (17313700, Simple(1134670676276L))
  }

  it should "have unit" in {
    unit(2)(Simple(45)) shouldBe(2, Simple(45))
  }

  it should "have map" in {
    val s: Rand[Int] = rng => (3, rng)
    val f: Int => Int = _ * 2
    map(s)(f)(Simple(50)) shouldBe(6, Simple(50))
  }

  it should "have map short notation" in {
    map((3, _))(_ * 2)(Simple(50)) shouldBe
      (6, Simple(50))
  }

  it should "have map2" in {
    val ra: Rand[Int] = (1, _)
    val rb: Rand[String] = ("b", _)
    val f: (Int, String) => String = _ + _
    map2(ra, rb)(f)(Simple(1)) shouldBe("1b", Simple(1))
  }

  it should "have map2 short notation" in {
    map2((1, _), ("b", _))(_ + _)(Simple(1)) shouldBe("1b", Simple(1))
  }

  it should "have both" in {
    val ra: Rand[Int] = int(_)
    val rb: Rand[Double] = double(_)
    both(ra, rb)(Simple(5)) shouldBe
      ((1923744, 0.6883513857610524), Simple(184598131514055L))
  }

  it should "have sequence" in {
    sequence(Nil)(Simple(1)) shouldBe
      (Nil, Simple(1))
    sequence(List(double))(Simple(2)) shouldBe
      (List(3.5832496359944344E-4), Simple(50429807845L))
    sequence(List(unit(1), unit(2)))(Simple(3)) shouldBe
      (List(1, 2), Simple(3))
    sequence(List(int, int, int))(Simple(5)) shouldBe
      (List(1923744, -1478223346, 832832900), Simple(54580536946886L)
      )
  }

  it should "have flatMap" in {
    flatMap(unit(1))(a => unit(s"$a!"))(Simple(55)) shouldBe
      ("1!", Simple(55))
    flatMap(int)(a => unit(s"$a!"))(Simple(60)) shouldBe
      ("23084934!", Simple(1512894235031L))
    flatMap(int)(a => map(int)(d => s"$a-$d"))(Simple(70)) shouldBe
      ("26932423-724690544", Simple(47493319540592L))
  }
}
