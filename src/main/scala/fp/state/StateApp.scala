package fp.state

import fp.state.State.get
import util.Util.show

object SimpleApp extends App {

  val st: State[List[Int], Unit] =
    get[List[Int]]
      .modify(_ :+ 2)
      .modify(_ :+ 3)
      .modify(_ :+ 4)
      .produce(_.size)
      .map(_ + 100)
      .modify(_ :+ 10)

  println {
    st.run(Nil)
  }
}

object StateApp extends App {

  type S = List[Int]

  def count: State[S, Int] = get.map(_.size)
  def add(a: Int*): State[S, _] = get.modify(_ :++ a)
  def fold: State[S, _] = get.modify(l => List(l.sum))
  def sum: State[S, Int] = get.map(_.sum)
  def str: State[S, String] = get[S].map(_.mkString("-"))

  val st: State[S, (Int, Int, Int, Int, Int, String)] =
    for {
      _ <- add(1, 2, 3, 4, 5)
      c1 <- count
      _ <- add(10, 11, 12)
      c2 <- count
      _ <- add(100)
      c3 <- count
      str <- str
      _ <- fold
      c4 <- count
      sum <- sum
      _ <- add(200, 300)
    } yield (c1, c2, c3, c4, sum, str)

  show(
    st.run(Nil),
    st.run(List(1, 2, 3)),
    st.run(List(1000)),
    st.run((1 to 100).toList)
  )
}