package fp.state

import fp.state.State.{get, sequence, set, unit}
import util.Test

/* KB

   unit returns constant value and doesn't change the state

   map functions work on the value associated with the state, but don't change the state

 */

class StateTest extends Test {

  "State" should "have unit" in {
    unit[Int, String]("A")
      .run(1) shouldBe(1, "A")
  }

  it should "have map" in {
    unit[Int, String]("Hi!")
      .map(_ + "Ho!")
      .run(1) shouldBe(1, "Hi!Ho!")
  }

  it should "have map2" in {
    unit[Int, String]("A")
      .map2(unit[Int, Double](0.99))((a, b) => s"C: $a $b")
      .run(1) shouldBe(1, "C: A 0.99")
  }

  it should "have flatMap" in {
    unit[Int, String]("A")
      .flatMap(a => unit(s"$a+B"))
      .run(1) shouldBe(1, "A+B")
  }

  it should "have sequence" in {
    sequence(List(unit[Int, Int](1), unit[Int, Int](2), unit[Int, Int](3)))
      .run(100) shouldBe unit(List(1, 2, 3)).run(100)
  }

  it should "have get" in {
    get[String].run("S1") shouldBe("S1", "S1")
    get[Int].run(55) shouldBe(55, 55)
  }

  it should "have set" in {
    set[String]("S").run("discarded state") shouldBe("S", ())
  }

  it should "have modify" in {
    get[Int].modifyFor(_ * 2).modifyFor(_ * 2).run(3) shouldBe(12, ())
  }

  it should "get and set" in {
    State.get[Int].run(1) shouldBe(1, 1)
    State.set[Int](1).run(0) shouldBe(1, ())
  }
}
