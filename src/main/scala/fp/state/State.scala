package fp.state

import fp.state.State._
import scala.annotation.tailrec

case class State[S, +A](run: S => (S, A)) {

  def map[B](f: A => B): State[S, B] =
    flatMap { a =>
      unit(f(a))
    }

  def map2[B, C](sb: State[S, B])(f: (A, B) => C): State[S, C] =
    flatMap { a =>
      sb.map { b =>
        f(a, b)
      }
    }

  def flatMap[B](f: A => State[S, B]): State[S, B] =
    State(
      (s1: S) => {
        val (s2: S, a: A) = run(s1)
        f(a).run(s2)
      })

  def modify(f: S => S): State[S, Unit] =
    State(s1 => (f(run(s1)._1), ()))

  def modifyFor(f: S => S): State[S, Unit] =
    for {
      s <- get
      _ <- set(f(s))
    } yield ()

  def produce[B](f: S => B): State[S, B] =
    State(s1 => {
      val (s2, _) = run(s1)
      (s2, f(s2))
    })

  private[state] def a(s: S): A =
    run(s)._2
}

object State {

  def unit[S, A](a: A): State[S, A] =
    State(s => (s, a))

  def sequence[S, A](l: List[State[S, A]]): State[S, List[A]] = {
    @tailrec
    def go(l: List[State[S, A]], s: S, acc: List[A]): (S, List[A]) =
      l match {
        case h :: t =>
          val (s2, a) = h.run(s)
          go(t, s2, a :: acc)
        case _ =>
          (s, acc.reverse)
      }
    State(go(l, _: S, Nil))
  }

  def get[S]: State[S, S] =
    State(s => (s, s))

  def modify[S](f: S => S): State[S, S] =
    State(s => (f(s), s))

  def set[S](s: S): State[S, Unit] =
    State(_ => (s, ()))

}