package fp.propertytesting

import org.scalacheck.Gen
import org.scalacheck.Prop.forAll
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ListTest extends AnyFlatSpec with Matchers {

  "List" should "reverse" in {
    val intList: Gen[List[Int]] = Gen.listOf(Gen.choose(0, 100))
    val prop =
      forAll(intList)(ns => ns.reverse.reverse == ns) &&
        forAll(intList)(ns => ns.headOption == ns.reverse.lastOption) &&
        forAll(intList)(ns => ns.sum == ns.reverse.sum)
    prop.check()
  }

  it should "fail" in {
    val intList: Gen[List[Int]] = Gen.listOf(Gen.choose(0, 100))
    val failingProp = forAll(intList)(ns => ns.reverse == ns)
    failingProp.check()
  }
}