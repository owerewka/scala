package fp.adts

object TreeAdt extends App {

  sealed trait Tree[A] extends TreeOps[A]
  case class Leaf[A](value: A) extends Tree[A]
  case class Branch[A](left: Tree[A], right: Tree[A]) extends Tree[A]

  trait TreeOps[A] { self: Tree[A] =>

    def sizeR: Int = this match {
      case Leaf(_) => 1
      case Branch(l, r) => l.sizeR + r.sizeR + 1
    }
    def size: Int = fold(_ => 1)(_ + _ + 1)

    def maxR(implicit ordering: Ordering[A]): A = this match {
      case Leaf(v) => v
      case Branch(l, r) => ordering.max(l.maxR, r.maxR)
    }
    def max(implicit ordering: Ordering[A]): A =
      fold(identity)(ordering.max(_, _))

    def depthR: Int = this match {
      case Leaf(_) => 1
      case Branch(l, r) => math.max(l.depthR, r.depthR) + 1
    }
    def depth: Int = fold(_ => 1)(math.max(_, _) + 1)

    def mapRec[B](f: A => B): Tree[B] = this match {
      case Leaf(v) => Leaf(f(v))
      case Branch(l, r) => Branch(l.mapRec(f), r.mapRec(f))
    }
    def map[B](f: A => B): Tree[B] =
      fold(v => Leaf(f(v)): Tree[B])(Branch(_, _))

    def fold[B](f: A => B)(g: (B, B) => B): B = this match {
      case Leaf(v) => f(v)
      case Branch(l, r) => g(l.fold(f)(g), r.fold(f)(g))
    }

  }
}
