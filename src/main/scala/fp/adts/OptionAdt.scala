package fp.adts

object OptionAdt {

  sealed trait Option[+A] {
    def map[B](f: A => B): Option[B]
    def flatMap[B](f: A => Option[B]): Option[B]
    def getOrElse[B >: A](default: => B): Option[B]
    def orElse[B >: A](op: => Option[B]): Option[B]
    def filter(f: A => Boolean): Option[A]
  }
  case class Some[+A](get: A) extends Option[A] {
    override def map[B](f: A => B): Option[B] = Some(f(get))
    override def flatMap[B](f: A => Option[B]): Option[B] = f(get)
    override def getOrElse[B >: A](default: => B): Option[B] = this
    override def orElse[B >: A](op: => Option[B]): Option[B] = this
    override def filter(f: A => Boolean): Option[A] = if (f(get)) this else None
  }
  case object None extends Option[Nothing] {
    override def map[B](f: Nothing => B): Option[B] = None
    override def flatMap[B](f: Nothing => Option[B]): Option[B] = None
    override def getOrElse[B >: Nothing](default: => B): Option[B] = Some(default)
    override def orElse[B >: Nothing](op: => Option[B]): Option[B] = op
    override def filter(f: Nothing => Boolean): Option[Nothing] = None
  }
}

object OptionAdtWithTrait {

  sealed trait Option[+A] extends OptionOps[A]
  case class Some[+A](get: A) extends Option[A]
  case object None extends Option[Nothing]

  trait OptionOps[+A] { this: Option[A] =>
    def map[B](f: A => B): Option[B] = this match {
      case Some(a) => Some(f(a))
      case _ => None
    }
    def flatMap[B](f: A => Option[B]): Option[B] = this match {
      case Some(a) => f(a)
      case _ => None
    }
    def getOrElse[B >: A](default: => B): Option[B] = this match {
      case Some(_) => this
      case _ => Some(default)
    }
    def orElse[B >: A](op: => Option[B]): Option[B] = this match {
      case Some(_) => this
      case _ => op
    }
    def filter(f: A => Boolean): Option[A] = this match {
      case Some(a) if f(a) => this
      case _ => None
    }
  }
}
