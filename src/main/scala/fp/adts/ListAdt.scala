package fp.adts

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

object ListAdt extends App {

  sealed trait List[+A]
  case object Nil extends List[Nothing]
  case class Cons[+A](head: A, tail: List[A]) extends List[A]

  object List {
    def sum(xs: List[Int]): Int = {
      @tailrec
      def go(l: List[Int], acc: Int): Int =
        l match {
          case Nil => acc
          case Cons(h, t) => go(t, acc + h)
        }
      go(xs, acc = 0)
    }

    def sum2(list: List[Int]): Int = foldLeft(list, 0)(_ + _)

    def product(xs: List[Double]): Double = {
      @tailrec
      def go(acc: Double, l: List[Double]): Double = l match {
        case Nil => acc
        case Cons(x, xs) => go(x * acc, xs)
      }
      go(acc = 1.0, xs)
    }

    def product2(list: List[Double]): Double = foldLeft(list, 1.0)(_ * _)

    @tailrec
    def foldLeft[A, B](as: List[A], z: B)(f: (A, B) => B): B =
      as match {
        case Nil => z
        case Cons(x, xs) => foldLeft(xs, f(x, z))(f)
      }

    def foldRight[A, B](as: List[A], z: B)(f: (A, B) => B): B =
      foldLeft(reverse(as), z)(f)

    def length[A](xs: List[A]): Int = foldLeft(xs, 0)((_, acc) => acc + 1)

    def apply[A](as: A*): List[A] = {
      @tailrec
      def go(acc: List[A], as: A*): List[A] =
        if (as.isEmpty) acc
        else go(Cons(as.head, acc), as.tail: _*)
      go(Nil, as.reverse: _*)
    }

    def tail[A](l: List[A]): List[A] = l match {
      case Nil => Nil
      case Cons(_, tail) => tail
    }

    def setHead[A](l: List[A], h: A): List[A] = l match {
      case Nil => Cons(h, Nil)
      case Cons(_, t) => Cons(h, t)
    }

    def drop[A](l: List[A], n: Int): List[A] = {
      @tailrec
      def go(l: List[A], remaining: Int): List[A] = {
        if (remaining > 0)
          l match {
            case Nil => Nil
            case Cons(_, Nil) => Nil
            case Cons(_, t) => go(t, remaining - 1)
          }
        else l
      }
      go(l, n)
    }

    @tailrec
    def dropWhile[A](l: List[A], f: A => Boolean): List[A] = l match {
      case Cons(h, t) if f(h) => dropWhile(t, f)
      case _ => l
    }

    //change to tail recursive
    def append[A](a1: List[A], a2: List[A]): List[A] = a1 match {
      case Nil => a2
      case Cons(h, t) => Cons(h, append(t, a2))
    }

    def init[A](l: List[A]): List[A] = {
      @tailrec
      def go(l: List[A], acc: ListBuffer[A]): List[A] = l match {
        case Nil => Nil
        case Cons(_, Nil) => List(acc.toList: _*)
        case Cons(h, t) => go(t, acc += h)
      }
      go(l, new ListBuffer[A]())
    }

    def reverse[A](l: List[A]): List[A] =
      foldLeft(l, Nil: List[A])((a: A, acc: List[A]) => Cons(a, acc))

    def append[A](l: List[A], a: A): List[A] =
      foldRight(l, List(a))((a: A, acc: List[A]) => Cons(a, acc))

    def concat[A](l: List[List[A]]): List[A] = flatten(l)
    def flatten[A](l: List[List[A]]): List[A] = reverse(
      foldLeft(l, Nil: List[A])((as: List[A], acc: List[A]) =>
        foldLeft(as, acc)((a: A, acc: List[A]) => Cons(a, acc))
      ))

    def plus1(l: List[Int]): List[Int] = map(l)(_ + 1)
    def doubleToString(l: List[Double]): List[String] = map(l)(_.toString)
    def map[A, B](l: List[A])(f: A => B): List[B] =
      foldRight(l, Nil: List[B])((a, acc) => Cons(f(a), acc))

    def filter[A](l: List[A])(f: A => Boolean): List[A] =
      foldRight(l, Nil: List[A])((a, acc) => if (f(a)) Cons(a, acc) else acc)

    def flatMap[A, B](l: List[A])(f: A => List[B]): List[B] =
      flatten(map(l)(f))
    def filterViaFlatMap[A](l: List[A])(f: A => Boolean): List[A] =
      flatMap(l)(a => if (f(a)) List(a) else Nil)

    def zipWithInts(as: List[Int], bs: List[Int]): List[Int] =
      zipWith(as, bs)(_ + _)

    def zipWith[A, B, C](as: List[A], bs: List[B])(f: (A, B) => C): List[C] = {
      @tailrec
      def go(l: (List[A], List[B]), acc: List[C]): List[C] = l match {
        case (Cons(ha, ta), Cons(hb, tb)) => go((ta, tb), Cons(f(ha, hb), acc))
        case _ => acc
      }
      reverse(go((as, bs), Nil))
    }

    @tailrec
    def hasSubsequence[A](sup: List[A], sub: List[A]): Boolean = sup match {
      case Nil => sub == Nil
      case _ if startsWith(sup, sub) => true
      case Cons(_, t) => hasSubsequence(t, sub)
    }

    @tailrec
    def startsWith[A](sup: List[A], sub: List[A]): Boolean = (sup, sub) match {
      case (_, Nil) => true
      case (Cons(h1, t1), Cons(h2, t2)) if h1 == h2 => startsWith(t1, t2)
      case _ => false
    }
  }

}