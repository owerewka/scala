package fp.adts

import fp.adts.OptionAdt._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class OptionAdtTest extends AnyFlatSpec with Matchers {

  "Option" should "have map" in {
    None.map(_.toString) shouldBe None
    Some(1).map(_.toString) shouldBe Some("1")
    Some("AB").map(_.length) shouldBe Some(2)
  }

  it should "have flatMap" in {
    None.flatMap(_ => None) shouldBe None
    None.flatMap(_ => Some(1)) shouldBe None
    Some(1).flatMap(v => Some(v.toString)) shouldBe Some("1")
    Some(2).flatMap(Some(_)) shouldBe Some(2)
    Some("A").flatMap(_ => None) shouldBe None
  }

  it should "have getOrElse" in {
    None.getOrElse(default = 1) shouldBe Some(1)
    Some(2).getOrElse(3) shouldBe Some(2)
  }

  it should "have orElse" in {
    None.orElse(Some(1)) shouldBe Some(1)
    None.orElse(None) shouldBe None
    Some(2).orElse(None) shouldBe Some(2)
    Some(1).orElse(Some(1)) shouldBe Some(1)
  }

  it should "have filter" in {
    None.filter(_ => true) shouldBe None
    None.filter(_ => false) shouldBe None
    Some(1).filter(_ == 1) shouldBe Some(1)
    Some(1).filter(_ == 2) shouldBe None
  }
}
