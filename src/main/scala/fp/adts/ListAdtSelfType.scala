package fp.adts

import scala.annotation.tailrec

//noinspection DuplicatedCode
object ListAdtSelfType extends App {

  sealed trait List[+A] extends ListExtensions[A] with Values[A]
  case object Nil extends List[Nothing]
  case class Cons[+A](head: A, tail: List[A]) extends List[A]

  trait ListExtensions[+A] { this: List[A] =>

    @tailrec
    final def foldLeft[B](z: B)(f: (A, B) => B): B =
      this match {
        case Nil => z
        case Cons(x, xs) => xs.foldLeft(f(x, z))(f)
      }

    def size: Int = foldLeft(0)((_, acc) => acc + 1)

  }

  trait Values[+A] { this: ListExtensions[A] =>
    lazy val length: Int = size
  }

  object List {
    def apply[A](as: A*): List[A] = {
      @tailrec
      def go(acc: List[A], as: A*): List[A] =
        if (as.isEmpty) acc
        else go(Cons(as.head, acc), as.tail: _*)
      go(Nil, as.reverse: _*)
    }
  }

  val l = List(1, 2, 3)
  println(l.foldLeft(0)(_ + _))
  println(l.size)
  println(l.length)
  println(l.length)
}
