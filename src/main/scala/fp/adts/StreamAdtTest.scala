package fp.adts

import fp.adts.StreamAdt.Stream._
import fp.adts.StreamAdt._
import util.Test
import scala.collection.mutable.ListBuffer

//noinspection DuplicatedCode
class StreamAdtTest extends Test {

  val stackLimit = 100000

  it should "have an evaluating toList " in {
    Stream().toList shouldBe Nil
    Stream(1).toList shouldBe List(1)
    Stream(1, 2).toList shouldBe List(1, 2)
    Stream(1, 2, 3).toList shouldBe List(1, 2, 3)
  }

  it should "have take" in {
    Stream().take(10).toList shouldBe Nil
    Stream(1).take(1).toList shouldBe List(1)
    Stream(2, 3).take(1).toList shouldBe List(2)
    Stream(4, 5, 6).take(2).toList shouldBe List(4, 5)
  }

  it should "have take unfold" in {
    Stream().takeUnfold(10).toList shouldBe Nil
    Stream(1).takeUnfold(1).toList shouldBe List(1)
    Stream(2, 3).takeUnfold(1).toList shouldBe List(2)
    Stream(4, 5, 6).takeUnfold(2).toList shouldBe List(4, 5)
  }

  it should "have drop" in {
    Stream().drop(10).toList shouldBe Nil
    Stream(1).drop(0).toList shouldBe List(1)
    Stream(1, 2).drop(1).toList shouldBe List(2)
    Stream(1, 2, 3).drop(2).toList shouldBe List(3)
    Stream(1, 2, 3, 4).drop(2).toList shouldBe List(3, 4)
  }

  it should "have takeWhile" in {
    Stream().takeWhile(_: Any => true).toList shouldBe Nil
    Stream(2).takeWhile(_.isOdd).toList shouldBe Nil
    Stream(1, 3).takeWhile(_.isOdd).toList shouldBe List(1, 3)
    Stream(1, 3, 2).takeWhile(_.isOdd).toList shouldBe List(1, 3)
    Stream(1, 3, 4, 5, 7).takeWhile(_.isOdd).toList shouldBe List(1, 3)
  }

  it should "have takeWhile unfold" in {
    Stream().takeWhileUnfold(_: Any => true).toList shouldBe Nil
    Stream(2).takeWhileUnfold(_.isOdd).toList shouldBe Nil
    Stream(1, 3).takeWhileUnfold(_.isOdd).toList shouldBe List(1, 3)
    Stream(1, 3, 2).takeWhileUnfold(_.isOdd).toList shouldBe List(1, 3)
    Stream(1, 3, 4, 5, 7).takeWhileUnfold(_.isOdd).toList shouldBe List(1, 3)
  }

  it should "have foldRight" in {
    Stream[Int]().foldRight(0)(_ + _) shouldBe 0
    Stream(1).foldRight(0)(_ + _) shouldBe 1
    Stream(10, 20).foldRight(1)(_ + _) shouldBe 31
    Stream("A", "B", "C").foldRight("->")((a, b) => b + a) shouldBe "->CBA"
  }

  it should "have foldLeft" in {
    Stream[Int]().foldLeft(0)(_ + _) shouldBe 0
    Stream(1).foldLeft(0)(_ + _) shouldBe 1
    Stream(10, 20).foldLeft(1)(_ + _) shouldBe 31
    Stream("A", "B", "C").foldLeft("->")((a, b) => b + a) shouldBe "->ABC"
  }

  it should "have forAll" in {
    Stream[Int]().forAll(_ => true) shouldBe true
    Stream[Int]().forAll(_ => false) shouldBe true
    Stream(1).forAll(_ == 1) shouldBe true
    Stream(1).forAll(_ == 2) shouldBe false
    Stream(1, 2).forAll(_ == 2) shouldBe false
    Stream(1, 2).forAll(_ == 1) shouldBe false
    Stream(1, 2).forAll(_ > 0) shouldBe true
  }

  it should "for all should not evaluate all elements of the stream" in {

    var eval1, eval2, eval3, eval4, eval5: Boolean = false

    cons({eval1 = true; 1},
      cons({eval2 = true; 2},
        cons({eval3 = true; 3},
          cons({eval4 = true; 4},
            cons({eval5 = true; 5},
              emptyStream)))))
      .forAll(_ < 3) shouldBe false

    eval1 shouldBe true
    eval2 shouldBe true
    eval3 shouldBe true
    eval4 shouldBe false
    eval5 shouldBe false
  }

  it should "have headOption" in {
    Stream().headOption shouldBe None
    Stream(1).headOption.value shouldBe 1
    Stream(2, 3).headOption.value shouldBe 2
  }

  it should "head option should not evaluate other elements" in {

    var eval1, eval2, eval3: Boolean = false

    cons({eval1 = true; 1},
      cons({eval2 = true; 2},
        cons({eval3 = true; 3}, emptyStream)))
      .headOption.value shouldBe 1

    eval1 shouldBe true
    eval2 shouldBe false
    eval3 shouldBe false
  }

  it should "have map" in {
    Stream[Int]().map(identity).toList shouldBe Nil
    Stream(1).map(_ * 2).toList shouldBe List(2)
    Stream(1, 2, 3).map(_ * 2).toList shouldBe List(2, 4, 6)
  }

  it should "have map unfold" in {
    Stream[Int]().mapUnfold(identity).toList shouldBe Nil
    Stream(1).mapUnfold(_ * 2).toList shouldBe List(2)
    Stream(1, 2, 3).mapUnfold(_ * 2).toList shouldBe List(2, 4, 6)
  }

  it should "have filter" in {
    Stream[Int]().filter(_ => true).toList shouldBe Nil
    Stream[Int]().filter(_ => false).toList shouldBe Nil
    Stream(1).filter(_ == 1).toList shouldBe List(1)
    Stream(1, 2, 3, 4).filter(_ <= 2).toList shouldBe List(1, 2)
  }

  it should "have append" in {
    Stream().append(Stream()).toList shouldBe Nil
    Stream().append(Stream(1)).toList shouldBe List(1)
    Stream(1).append(Stream(2)).toList shouldBe List(1, 2)
    Stream(1, 2).append(Stream(3, 4)).toList shouldBe List(1, 2, 3, 4)
  }

  it should "have append that is non-strict" in {
    var eval: Boolean = false
    val stream = Stream(1)
      .append({eval = true; Stream(2)})
    eval shouldBe false
    stream.toList shouldBe List(1, 2)
    eval shouldBe true
  }

  it should "have non-strict prepend" in {

    var eval1, eval2, eval3: Boolean = false

    val stream = Stream()
      .prepend({eval1 = true; 1})
      .prepend({eval2 = true; 2})
      .prepend({eval3 = true; 3})

    eval1 shouldBe false
    eval2 shouldBe false
    eval3 shouldBe false

    stream.toList shouldBe List(3, 2, 1)
  }

  it should "have flat map" in {
    val f = (a: Int) => Stream(a, a * 2)
    Stream().flatMap(f).toList shouldBe Nil
    Stream(1).flatMap(f).toList shouldBe List(1, 2)
    Stream(1, 10).flatMap(f).toList shouldBe List(1, 2, 10, 20)
  }

  it should "have find" in {
    Stream[Int]().find(_ => true) shouldBe None
    Stream(1).find(_ == 1).value shouldBe 1
    Stream(1).find(_ != 1) shouldBe None
    Stream(1, 2).find(_ == 2).value shouldBe 2
    Stream(1, 2, 3).find(_ == 2).value shouldBe 2
  }

  it should "have exists" in {
    Stream[Int]().exists(1) shouldBe false
    Stream(1).exists(1) shouldBe true
    Stream(1, 2, 3).exists(2) shouldBe true
    Stream(1, 2, 3).exists(5) shouldBe false
  }

  it should "not have exist evaluate the whole stream" in {

    var eval1, eval2, eval3: Boolean = false

    cons({eval1 = true; 1},
      cons({eval2 = true; 2},
        cons({eval3 = true; 3}, emptyStream)))
      .exists(2) shouldBe true

    eval1 shouldBe true
    eval2 shouldBe true
    eval3 shouldBe false
  }

  it should "have constant" in {
    val fifes: Stream[Int] = constant(5)
    fifes.headOption.value shouldBe 5
    fifes.take(100).toList shouldBe List.fill(100)(5)
  }

  it should "have constant with unfold" in {
    constantUnfold(3).take(3).toList shouldBe List(3, 3, 3)
  }

  it should "have ones unfold" in {
    onesUnfold.take(3).toList shouldBe List(1, 1, 1)
  }

  it should "have from constant Int increasing Stream" in {
    from(0).take(0).toList shouldBe Nil
    from(0).take(3).toList shouldBe List(0, 1, 2)
    from(10).take(4).toList shouldBe List(10, 11, 12, 13)
  }

  it should "have from constant Int increasing Stream with unfold" in {
    fromUnfold(0).take(0).toList shouldBe Nil
    fromUnfold(0).take(3).toList shouldBe List(0, 1, 2)
    fromUnfold(10).take(4).toList shouldBe List(10, 11, 12, 13)
  }

  it should "have fibonacci numbers Stream" in {
    fibs.take(8).toList shouldBe List(0, 1, 1, 2, 3, 5, 8, 13)
  }

  it should "have fibonacci numbers Stream with unfold" in {
    fibsUnfold.take(8).toList shouldBe List(0, 1, 1, 2, 3, 5, 8, 13)
  }

  it should "have forEach" in {
    val buffer = new ListBuffer[Int]()
    Stream(1, 2, 3).forEach(buffer.addOne)
    buffer shouldBe List(1, 2, 3)
  }

  it should "have forEachRecursive fill up stack" in {
    try {
      constant(1).take(stackLimit).forEachRecursive(_ => ())
      fail("Should throw StackOverflowError!")
    }
    catch {
      case _: java.lang.StackOverflowError =>
    }
  }

  it should "have a stack-safe forEach" in {
    var count = 0
    constant(1).take(stackLimit).forEach(_ => count += 1)
    count shouldBe stackLimit
  }

  it should "have an unfold" in {
    unfold(0)(_ => None).toList shouldBe Nil
    unfold(1)(s => Some(s, s + 1)).take(5).toList shouldBe List(1, 2, 3, 4, 5)
    unfold(1)(s => if (s >= 20) None else Some((s, s * 2))).toList shouldBe List(1, 2, 4, 8, 16)
  }

  it should "have zipWith" in {
    Stream().zipAll(Stream()).toList shouldBe Nil
    Stream(1).zipAll(Stream()).toList shouldBe List((Some(1), None))
    Stream().zipAll(Stream(10)).toList shouldBe List((None, Some(10)))
    Stream(1).zipAll(Stream(11)).toList shouldBe List((Some(1), Some(11)))
    Stream(1, 2).zipAll(Stream(10)).toList shouldBe List((Some(1), Some(10)), (Some(2), None))
    Stream(1, 2).zipAll(Stream(10, 11)).toList shouldBe List((Some(1), Some(10)), (Some(2), Some(11)))
  }

  it should "have startsWith" in {
    Stream().startsWith(Stream()) shouldBe true
    Stream(1).startsWith(Stream()) shouldBe true
    Stream().startsWith(Stream(1)) shouldBe false
    Stream(1).startsWith(Stream(1)) shouldBe true
    Stream(2).startsWith(Stream(1)) shouldBe false
    Stream(1, 2).startsWith(Stream(1)) shouldBe true
    Stream(1, 2).startsWith(Stream(1, 2)) shouldBe true
    Stream(1, 2, 3, 4).startsWith(Stream(1, 2, 3)) shouldBe true
    Stream(1, 2).startsWith(Stream(3)) shouldBe false
    Stream(1, 2).startsWith(Stream(2, 1)) shouldBe false
  }

  it should "have tails" in {
    Stream().tails.toList shouldBe Nil
    Stream(1).tails.map(_.toList).toList shouldBe List(List(1))
    Stream(1, 2).tails.map(_.toList).toList shouldBe List(List(1, 2), List(2))
    Stream(1, 2, 3).tails.map(_.toList).toList shouldBe List(List(1, 2, 3), List(2, 3), List(3))
  }

  it should "have hasSubsequence" in {
    Stream().hasSubsequence(Stream()) shouldBe false
    Stream(1).hasSubsequence(Stream()) shouldBe true
    Stream().hasSubsequence(Stream(1)) shouldBe false
    Stream(1).hasSubsequence(Stream(1)) shouldBe true
    Stream(1, 2).hasSubsequence(Stream(2)) shouldBe true
    Stream(1, 2).hasSubsequence(Stream(3)) shouldBe false
    Stream(1, 2).hasSubsequence(Stream(1, 2)) shouldBe true
    Stream(1, 2).hasSubsequence(Stream(2, 1)) shouldBe false
    Stream(1, 2, 3, 4).hasSubsequence(Stream(2, 3)) shouldBe true
    Stream(1, 2, 3, 4).hasSubsequence(Stream(3, 2)) shouldBe false
  }

  it should "have scanLeft" in {
    //reverse output
    Stream[Int]().scanLeft(0)(_ + _).toList shouldBe Nil
    Stream(1).scanLeft(0)(_ + _).toList shouldBe List(1)
    Stream(1, 2).scanLeft(0)(_ + _).toList shouldBe List(3, 1)
    Stream(1, 2, 3).scanLeft(0)(_ + _).toList shouldBe List(6, 3, 1)
    Stream(1, 10, 100).scanLeft(0)(_ + _).toList shouldBe List(111, 11, 1)
  }

  it should "scanLeft and the helper functions should be stack-safe" in {
    onesUnfold.take(stackLimit).scanLeft(0)(_ + _).toList.size shouldBe stackLimit
  }

}