package fp.adts

import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

//noinspection DuplicatedCode
object StreamAdt {

  import Stream._
  sealed trait Stream[+A] {

    def toList: List[A] = {
      val buffer = new ListBuffer[A]()
      @tailrec
      def go(s: Stream[A]): List[A] = s match {
        case Cons(h, t) =>
          buffer += h()
          go(t())
        case _ => buffer.toList
      }
      go(this)
    }

    def take(n: Int): Stream[A] = this match {
      case Cons(h, t) if n > 1 => cons(h(), t().take(n - 1))
      case Cons(h, _) if n == 1 => cons(h(), empty)
      case _ => empty
    }
    def takeUnfold(n: Int): Stream[A] = unfold((n, this)) {
      case (i, Cons(h, t)) if i > 0 => Some(h(), (i - 1, t()))
      case _ => None
    }

    def drop(n: Int): Stream[A] = this match {
      case Cons(_, t) if n > 0 => t().drop(n - 1)
      case _ => this
    }

    def takeWhileRecursive(p: A => Boolean): Stream[A] = this match {
      case Cons(h, t) if p(h()) => cons(h(), t().takeWhileRecursive(p))
      case _ => empty
    }

    def takeWhile(p: A => Boolean): Stream[A] = foldRight(empty[A]) { (h, t) =>
      if (p(h)) cons(h, t)
      else empty
    }

    def takeWhileUnfold(p: A => Boolean): Stream[A] = unfold(this) {
      case Cons(h, t) if p(h()) => Some(h(), t())
      case _ => None
    }

    def foldRight[B](acc: => B)(f: (A, => B) => B): B = this match {
      case Cons(h, t) => f(h(), t().foldRight(acc)(f))
      case _ => acc
    }

    def foldLeft[B](z: => B)(f: (A, => B) => B): B = {
      @tailrec
      def go(z: B, s: => Stream[A]): B = s match {
        case Cons(h, t) => go(f(h(), z), t())
        case _ => z
      }
      go(z, this)
    }

    def forAll(p: A => Boolean): Boolean =
      foldRight[Boolean](true)((h, t) => p(h) && t)

    def headOption: Option[A] =
      foldRight(None: Option[A])(
        (h, _) => Some(h)
      )

    def map[B](f: A => B): Stream[B] =
      foldRight(empty[B])((h, t) => cons(f(h), t))
    def mapUnfold[B](f: A => B): Stream[B] =
      unfold(this) {
        case Cons(h, t) => Some((f(h()), t()))
        case Empty => None
      }

    def filter(p: A => Boolean): Stream[A] =
      foldRight(empty[A])((h, t) => if (p(h)) cons(h, t) else t)

    def append[B >: A](a: => Stream[B]): Stream[B] =
      foldRight(a)((h, t) => cons(h, t))

    def prepend[B >: A](b: => B): Stream[B] = cons(b, this)

    def flatMap[B](f: A => Stream[B]): Stream[B] =
      foldRight(empty[B])((h, t) => f(h).append(t))

    def find(p: A => Boolean): Option[A] =
      filter(p).headOption

    def exists[B >: A](a: B): Boolean =
      foldRight(false)((h, t) => a == h || t)

    def forEachRecursive(f: A => Unit): Unit =
      this match {
        case Cons(h, t) =>
          f(h())
          t().forEachRecursive(f)
        case _ => ()
      }

    def forEach(f: A => Unit): Unit = {
      @tailrec
      def go(s: => Stream[A]): Unit = s match {
        case Cons(h, t) => f(h()); go(t())
        case _ => ()
      }
      go(this)
    }

    def zipAll[B](s2: Stream[B]): Stream[(Option[A], Option[B])] = unfold(this, s2) {
      case (Cons(h1, t1), Cons(h2, t2)) => Some((Some(h1()), Some(h2())), (t1(), t2()))
      case (Cons(h1, t1), Empty) => Some((Some(h1()), None), (t1(), Empty))
      case (Empty, Cons(h2, t2)) => Some((None, Some(h2())), (Empty, t2()))
      case _ => None
    }

    def startsWith[B >: A](s2: Stream[B]): Boolean = zipAll(s2)
      .takeWhile(_._2.isDefined)
      .forAll { case (a, b) => a == b }

    def tails: Stream[Stream[A]] = unfold(this) {
      case Cons(h, t) => Option(cons(h(), t()), t())
      case Empty => None
    }

    def hasSubsequence[B >: A](s2: Stream[B]): Boolean =
      tails.find(_.startsWith(s2)).isDefined

    def scanLeft[B](z: B)(f: (A, => B) => B): Stream[B] =
      this.foldLeft[(Stream[B], B)]((empty[B], z)) {
        case (a: A, (acc: Stream[B], z: B)) =>
          lazy val b = f(a, z)
          (cons(b, acc), b)
      }._1

    def scanRight[B](z: B)(f: (A, => B) => B): Stream[B] =
      this.foldRight[(Stream[B], B)]((empty[B], z)) {
        case (a, (acc: Stream[B], z: B)) =>
          lazy val b = f(a, z)
          (cons(b, acc), b)
      }._1

    def debug(): Stream[A] = map { a => println(a); a }
    def pull(): Unit = forAll(_ => true)
  }

  case object Empty extends Stream[Nothing]
  case class Cons[+A](h: () => A, t: () => Stream[A]) extends Stream[A]

  object Stream {
    def cons[A](hd: => A, tl: => Stream[A]): Stream[A] = {
      lazy val head = hd
      lazy val tail = tl
      Cons(() => head, () => tail)
    }
    def empty[A]: Stream[A] = Empty
    def emptyStream[A]: Stream[A] = Empty
    def apply[A](as: A*): Stream[A] =
      if (as.isEmpty) empty
      else cons(as.head, apply(as.tail: _*))

    def constant[A](a: A): Stream[A] = cons(a, constant(a))
    def constantUnfold[A](a: A): Stream[A] = unfold(a)(a => Some(a, a))
    def onesUnfold: Stream[Int] = unfold(1)(_ => Some(1, 1))

    def from(n: Int): Stream[Int] = cons(n, from(n + 1))
    def fromUnfold(n: Int): Stream[Int] = unfold(n)(s => Some(s, s + 1))

    def fibs: Stream[Int] = {
      def go(f0: Int, f1: Int): Stream[Int] = cons(f0, go(f1, f0 + f1))
      go(0, 1)
    }
    def fibsUnfold: Stream[Int] =
      unfold((0, 1)) { case (f0, f1) => Some(f0, (f1, f0 + f1)) }

    def unfold[A, S](z: S)(f: S => Option[(A, S)]): Stream[A] = f(z) match {
      case Some((h, s)) => cons(h, unfold(s)(f))
      case None => empty
    }
  }

}