package fp.adts

import fp.adts.TreeAdt._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TreeAdtTest extends AnyFlatSpec with Matchers {

  "A Tree" should "have size" in {
    Leaf(1).size shouldBe 1
    Branch(Leaf(1), Leaf(2)).size shouldBe 3
    Branch(Branch(Leaf(1), Leaf(1)), Leaf(1)).size shouldBe 5
  }

  it should "have a max" in {
    Leaf(1).max shouldBe 1
    Branch(Leaf(1), Leaf(2)).max shouldBe 2
    Branch(Branch(Leaf(2), Leaf(3)), Leaf(1)).max shouldBe 3
  }

  it should "have a depth" in {
    Leaf(1).depth shouldBe 1
    Branch(Leaf(1), Leaf(2)).depth shouldBe 2
    Branch(Branch(Leaf(2), Leaf(3)), Leaf(1)).depth shouldBe 3
    Branch(
      Branch(
        Leaf(2),
        Leaf(3)),
      Branch(
        Branch(
          Leaf(2),
          Leaf(3)),
        Leaf(3))).depth shouldBe 4
  }

  it should "have a map" in {
    val f: Int => String = _.toString
    Leaf(1).map(f) shouldBe Leaf("1")
    Branch(Leaf(1), Leaf(2)).map(f) shouldBe
      Branch(Leaf("1"), Leaf("2"))
    Branch(Branch(Leaf(2), Leaf(3)), Leaf(1)).map(f) shouldBe
      Branch(Branch(Leaf("2"), Leaf("3")), Leaf("1"))
  }

  it should "have a fold" in {

    val f = (i: Int) => i.toString
    val g: (String, String) => String = _ + _

    Leaf(1).fold(f)(g) shouldBe "1"
    Branch(Leaf(1), Leaf(2)).fold(f)(g) shouldBe "12"
    Branch(
      Branch(
        Leaf(2),
        Leaf(3)),
      Leaf(1)
    ).fold(f)(g) shouldBe "231"
  }
}
