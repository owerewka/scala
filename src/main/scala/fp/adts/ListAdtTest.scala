package fp.adts

import fp.adts.ListAdt.List._
import fp.adts.ListAdt.{List, _}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ListAdtTest extends AnyFlatSpec with Matchers {

  "A List" should "have sum" in {
    sum(Nil) shouldBe 0
    sum(List(1)) shouldBe 1
    sum(List(1, 2)) shouldBe 3
    sum2(Nil) shouldBe 0
    sum2(List(1)) shouldBe 1
    sum2(List(1, 2)) shouldBe 3
  }

  it should "fold into a list if f is list data constructor" in {
    foldLeft(List(1, 2, 3), Nil: List[Int])(Cons(_, _)) shouldBe
      List(3, 2, 1)
  }

  it should "have a foldLeft that folds from left to the right" in {
    foldLeft(List(1, 2, 3), "Start")((a, acc) => acc + "->" + a) shouldBe
      "Start->1->2->3"
  }

  it should "have a foldRight that folds from right to the left" in {
    foldRight(List(1, 2, 3), "Start")((a, acc) => acc + "->" + a) shouldBe
      "Start->3->2->1"
  }

  it should "have product" in {
    product(Nil) shouldBe 1.0
    product(List(2.0)) shouldBe 2.0
    product(List(2, 3)) shouldBe 6.0
    product2(Nil) shouldBe 1.0
    product2(List(2.0)) shouldBe 2.0
    product2(List(2, 3)) shouldBe 6.0
  }

  it should "have foldLeft" in {
    foldLeft(List("a", "bb"), 0)(_.length + _) shouldBe 3
    foldLeft(List("a", "bb"), "")(_ + _) shouldBe "bba"
    scala.List("a", "bb").foldRight("")(_ + _) shouldBe "abb"
    scala.List("a", "bb").foldLeft("")(_ + _) shouldBe "abb"
  }

  it should "have length" in {
    List.length(Nil) shouldBe 0
    List.length(List(1)) shouldBe 1
    List.length(List(1, 2)) shouldBe 2
  }

  it should "have tale" in {
    tail(Nil) shouldBe Nil
    tail(List(1, 2, 3)) shouldBe List(2, 3)
  }

  it should "have setHead" in {
    setHead(Nil, 1) shouldBe List(1)
    setHead(List(1, 2, 3), 10) shouldBe List(10, 2, 3)
  }

  it should "have drop" in {
    drop(Nil, 0) shouldBe Nil
    drop(Nil, 10) shouldBe Nil
    drop(List(1), 1) shouldBe Nil
    drop(List(1), 10) shouldBe Nil
    drop(List(1, 2), 1) shouldBe List(2)
    drop(List(1, 2, 3), 2) shouldBe List(3)
  }

  it should "drop while predicate is true" in {
    dropWhile(Nil, _: Any => true) shouldBe Nil
    dropWhile(Nil, _: Any => false) shouldBe Nil
    dropWhile(List(1), _: Int => false) shouldBe List(1)
    dropWhile(List(1), _: Int => true) shouldBe Nil
    dropWhile(List(1, 2), _: Int => false) shouldBe List(1, 2)
    dropWhile(List(1, 2), _: Int => true) shouldBe Nil
    dropWhile(List(1, 2, 3), (_: Int) <= 2) shouldBe List(3)
    dropWhile(List(1, 2, 3, 4), (_: Int) < 4) shouldBe List(4)
  }

  it should "append two lists" in {
    append(Nil, Nil) shouldBe Nil
    append(List(1), Nil) shouldBe List(1)
    append(Nil, List(1)) shouldBe List(1)
    append(Nil, List(1, 2)) shouldBe List(1, 2)
    append(List(1), List(2)) shouldBe List(1, 2)
    append(List(1, 2), Nil) shouldBe List(1, 2)
  }

  it should "have init" in {
    init(Nil) shouldBe Nil
    init(List(1)) shouldBe Nil
    init(List(1, 2)) shouldBe List(1)
    init(List(1, 2, 3)) shouldBe List(1, 2)
    init(List(1, 2, 3, 4)) shouldBe List(1, 2, 3)
  }

  it should "have reverse" in {
    reverse(Nil) shouldBe Nil
    reverse(List(1)) shouldBe List(1)
    reverse(List(1, 2)) shouldBe List(2, 1)
    reverse(List(1, 2, 3)) shouldBe List(3, 2, 1)
  }

  it should "have append" in {
    append(Nil, 1) shouldBe List(1)
    append(List(1), 2) shouldBe List(1, 2)
    append(List(1, 2), 3) shouldBe List(1, 2, 3)
  }

  it should "have flatten" in {
    flatten(Nil) shouldBe Nil
    flatten(List(Nil)) shouldBe Nil
    flatten(List(List(1))) shouldBe List(1)
    flatten(List(List(1), Nil)) shouldBe List(1)
    flatten(List(Nil, List(1))) shouldBe List(1)
    flatten(List(List(1), List(2))) shouldBe List(1, 2)
    flatten(List(List(1, 2), List(3))) shouldBe List(1, 2, 3)
    flatten(List(List(1), List(2, 3))) shouldBe List(1, 2, 3)
    concat(List(List(1), List(2, 3))) shouldBe List(1, 2, 3)
  }

  it should "have plus1" in {
    plus1(Nil) shouldBe Nil
    plus1(List(1)) shouldBe List(2)
    plus1(List(1, 2)) shouldBe List(2, 3)
  }

  it should "have doubleToString" in {
    doubleToString(Nil) shouldBe Nil
    doubleToString(List(1.1)) shouldBe List("1.1")
    doubleToString(List(1.1, 2.2)) shouldBe List("1.1", "2.2")
  }

  it should "have a map" in {
    map(Nil: List[Int])(identity) shouldBe Nil
    map(List(1))(_ + 1) shouldBe List(2)
    map(List(2, 3))(_ * 2) shouldBe List(4, 6)
  }

  it should "have a filter" in {
    filter(Nil: List[Int])(_ => true) shouldBe Nil
    filter(List(1))(_ => false) shouldBe Nil
    filter(List(1, 2))(_ != 1) shouldBe List(2)
    filter(List(1, 2, 3))(_ > 1) shouldBe List(2, 3)
    filterViaFlatMap(List(1, 2, 3))(_ > 1) shouldBe List(2, 3)
  }

  it should "have a flatMap" in {
    flatMap[Int, Int](Nil)(List(_)) shouldBe Nil
    flatMap(List(1))(a => List(a)) shouldBe List(1)
    flatMap(List(1, 2))(a => List(a)) shouldBe List(1, 2)
    flatMap(List(2))(a => List(a - 1, a, a + 1)) shouldBe List(1, 2, 3)
  }

  it should "have zipWithInts" in {
    zipWithInts(Nil, Nil) shouldBe Nil
    zipWithInts(List(1), List(10)) shouldBe List(11)
    zipWithInts(List(1, 2), List(10, 20)) shouldBe List(11, 22)
    zipWithInts(List(1), List(10, 20)) shouldBe List(11)
    zipWithInts(List(1, 2), Nil) shouldBe Nil
    zipWithInts(List(1, 2, 3), List(10, 20)) shouldBe List(11, 22)
  }

  it should "have subsequence" in {
    hasSubsequence(Nil, Nil) shouldBe true
    hasSubsequence(Nil, List(1)) shouldBe false
    hasSubsequence(List(1), Nil) shouldBe true
    hasSubsequence(List(1), List(1)) shouldBe true
    hasSubsequence(List(1, 2), List(2)) shouldBe true
    hasSubsequence(List(1, 2), List(1, 2)) shouldBe true
    hasSubsequence(List(1, 2), List(2, 1)) shouldBe false
    hasSubsequence(List(1, 2, 3), List(2, 3)) shouldBe true
  }

  it should "have startsWith" in {
    startsWith(Nil, Nil) shouldBe true
    startsWith(List(1), Nil) shouldBe true
    startsWith(Nil, List(1)) shouldBe false
    startsWith(List(1), List(1)) shouldBe true
    startsWith(List(1), List(1, 2)) shouldBe false
    startsWith(List(1, 2), List(1, 2)) shouldBe true
    startsWith(List(1, 2), List(2, 1)) shouldBe false
    startsWith(List(1, 2, 3), List(1, 2)) shouldBe true
  }
}
