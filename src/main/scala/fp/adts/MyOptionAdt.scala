package fp.adts

object MyOptionAdt extends App {

  sealed trait Option[+A] extends OptionOps[A]
  case object None extends Option[Nothing]
  case class Some[A](v: A) extends Option[A]

  trait OptionOps[+A] { self: Option[A] =>

    lazy val isEmpty: Boolean = self match {
      case None => true
      case _ => false
    }

    lazy val get: A = self match {
      case Some(a) => a
      case _ => throw new IllegalStateException("Cannot call get on None")
    }

    def map[B](f: A => B): Option[B] = self match {
      case Some(a) => Some(f(a))
      case _ => None
    }
  }

  object Option {
    def apply[A](a: A): Option[A] = Some(a)
    def apply[A](): Option[A] = None
  }

  println(Some("value"))
  println(Some("value").isEmpty)
  println(Some("value").get)
  println(Option("value").map(_.length))
  println(None)
  println(None.isEmpty)
  println(None.map((_: Int) * 2))

}