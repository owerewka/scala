package fp.adts

import fp.adts.Either.{sequence, traverse}
import util.Test
import scala.annotation.tailrec

sealed trait Either[+E, +A] {
  def map[B](f: A => B): Either[E, B]
  def flatMap[EE >: E, B](f: A => Either[EE, B]): Either[EE, B]
  def orElse[EB >: E, B >: A](b: => Either[EB, B]): Either[EB, B]
  def map2[EB >: E, B, C](b: Either[EB, B])(f: (A, B) => C): Either[EB, C]
}
case class Left[+E](value: E) extends Either[E, Nothing] {
  override def map[B](f: Nothing => B): Either[E, B] = this
  override def flatMap[EE >: E, B](f: Nothing => Either[EE, B]): Either[EE, B] = this
  override def orElse[EB >: E, B](b: => Either[EB, B]): Either[EB, B] = b
  override def map2[EB >: E, B, C](b: Either[EB, B])(f: (Nothing, B) => C): Either[EB, C] = this
}
case class Right[+A](value: A) extends Either[Nothing, A] {
  override def map[B](f: A => B): Either[Nothing, B] = Right(f(value))
  override def flatMap[EB, B](f: A => Either[EB, B]): Either[EB, B] = f(value)
  override def orElse[EB, B >: A](b: => Either[EB, B]): Either[EB, B] = this
  override def map2[EB >: Nothing, B, C](b: Either[EB, B])(f: (A, B) => C): Either[EB, C] = b match {
    case l@Left(_) => l
    case Right(bValue) => Right(f(value, bValue))
  }
}
object Either {
  def sequence[E, A](es: List[Either[E, A]]): Either[E, List[A]] = traverse(es)(identity)
  def traverse[E, A, B](as: List[A])(f: A => Either[E, B]): Either[E, List[B]] = {
    @tailrec
    def go(as: List[A], acc: List[B]): Either[E, List[B]] = as match {
      case Nil => Right(acc)
      case h :: t => f(h) match {
        case Right(r) => go(t, acc :+ r)
        case l: Left[E] => l
      }
    }
    go(as, Nil)
  }
}

class Map extends Test {

  "Either" should "have map" in {
    val f: String => Int = _.length
    Left("Error").map(f) shouldBe Left("Error")
    Right("Success").map(f) shouldBe Right(7)
  }

  it should "have flatMap" in {
    Left("Error").flatMap(identity) shouldBe Left("Error")
    Right(2).flatMap(r => Right(r * 3)) shouldBe Right(6)
    Right(3).flatMap(r => Left(s"$r is invalid!")) shouldBe Left("3 is invalid!")
  }

  it should "have orElse" in {
    Left("Nothing").orElse(Right("Something")) shouldBe Right("Something")
    Left("Nothing").orElse(Left("Error")) shouldBe Left("Error")
    Right(1).orElse(Right(2)) shouldBe Right(1)
  }

  it should "have map2" in {
    val f = (a: String, b: String) => s"$a + $b"
    Left("Empty1").map2(Left("Empty2"))(f) shouldBe Left("Empty1")
    Left("Empty1").map2(Right("Full2"))(f) shouldBe Left("Empty1")
    Right("Full1").map2(Left("Empty2"))(f) shouldBe Left("Empty2")
    Right("Full1").map2(Right("Full2"))(f) shouldBe Right("Full1 + Full2")
  }

  it should "have sequence" in {
    sequence(List()) shouldBe Right(Nil)
    sequence(List(Left("failure"))) shouldBe Left("failure")
    sequence(List(Right("success"))) shouldBe Right(List("success"))
    sequence(List(Right("success"), Left("failure"))) shouldBe Left("failure")
    sequence(List(Left("failure"), Right("success"))) shouldBe Left("failure")
    sequence(List(Right("success"), Right("success"))) shouldBe Right(List("success", "success"))
  }

  it should "have traverse" in {
    def f(a: Int): Either[String, Int] = if (a.isOdd) Left(s"$a") else Right(a)
    traverse[String, Int, Int](Nil)(f) shouldBe Right(Nil)
    traverse(List(1))(f) shouldBe Left("1")
    traverse(List(2))(f) shouldBe Right(List(2))
    traverse(List(1, 2))(f) shouldBe Left("1")
    traverse(List(2, 4))(f) shouldBe Right(List(2, 4))
    traverse(List(2, 4, 7))(f) shouldBe Left("7")
  }
}

class Mean extends Test {

  def mean(xs: IndexedSeq[Double]): Either[String, Double] =
    if (xs.isEmpty) Left("Empty Seq!")
    else Right(xs.sum / xs.size)

  mean(Vector()) shouldBe Left("Empty Seq!")
  mean(Vector(1)) shouldBe Right(1)
  mean(Vector(2, 4)) shouldBe Right(3)
}

class SafeDiv extends Test {

  def safeDiv(x: Int, y: Int): Either[Exception, Int] =
    try Right(x / y)
    catch {case e: Exception => Left(e)}

  safeDiv(4, 2) shouldBe Right(2)
  safeDiv(1, 0) match {
    case Left(e) => e.getMessage shouldBe "/ by zero"
    case _ => fail("should throw exception")
  }
}

class TryTest extends Test {

  def Try[A](a: => A): Either[Exception, A] = {
    try Right(a)
    catch {case e: Exception => Left(e)}
  }

  Try(1) shouldBe Right(1)
  val e = new RuntimeException("Error")
  Try(throw e) shouldBe Left(e)
}