package fp.design

import fp.design.Functions.{fold, foldMap}
import fp.design.Test1.intAddition
import fp.design.WordCount._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

object Functions {

  def fold[A](as: IndexedSeq[A], monoid: Monoid[A]): A =
    if (as.isEmpty) {
      monoid.zero
    } else if (as.length == 1) {
      as.head
    } else {
      val (l, r) = as.splitAt(as.length / 2)
      monoid.op(fold(l, monoid), fold(r, monoid))
    }

  def foldMap[A, B](as: IndexedSeq[A], monoid: Monoid[B])(f: A => B): B =
    fold(as.map(f), monoid)
}

object WordCount {

  sealed trait WC
  case class Stub(chars: String) extends WC
  case class Part(lStub: String, words: Int, rStub: String) extends WC

  class WCMonoid extends Monoid[WC] {
    override def op(a1: WC, a2: WC): WC =
      (a1, a2) match {
        case (Stub(a), Part(l, w, r)) => Part(a + l, w, r)
        case (Part(l, w, r), Stub(a)) => Part(l, w, r + a)
        case (Stub(a), Stub(b)) => Stub(a + b)
        case (Part(l1, w1, r1), Part(l2, w2, r2)) => Part(l1, w1 + (if ((r1 + l2).isEmpty) 0 else 1) + w2, r2)
      }
    override def zero: WC = Stub("")
  }

  def count(s: String): Int = {
    def wc(c: Char): WC = if (c.isWhitespace) Part("", 0, "") else Stub(c.toString)
    def length(s: String): Int = s.length min 1
    foldMap(s.toIndexedSeq, new WCMonoid)(wc) match {
      case Stub(s) => length(s)
      case Part(l, w, r) => length(l) + w + length(r)
    }
  }
}

class WordCountTest extends AnyFlatSpec with Matchers {

  val monoid = new WCMonoid
  val f: (WC, WC) => WC = monoid.op
  val zero: WC = monoid.zero

  "WCMonoid" should "respect the monoid laws" in {
    f(zero, zero) shouldBe zero
    f(zero, Stub("a")) shouldBe f(Stub("a"), zero)
    f(Stub("a"), Stub("b")) shouldBe Stub("ab")
    f(Part("Ja", 1, "m"), Part("am", 10, "domek!")) shouldBe Part("Ja", 12, "domek!")
    f(Part("", 0, "ba"), Part("", 10, "")) shouldBe Part("", 11, "")
  }

  "WC" should "count" in {
    count("ala ma kota") shouldBe 3
  }
}

class SplitIndexedSeq extends AnyFlatSpec with Matchers {

  "IndexedSeq" should "split" in {
    fold(Vector(), intAddition) shouldBe 0
    fold(Vector(1), intAddition) shouldBe 1
    fold(Vector(2), intAddition) shouldBe 2
    fold(Vector(1, 2), intAddition) shouldBe 3
    fold(Vector(1, 2, 3), intAddition) shouldBe 6
    fold(Vector(1, 2, 3, 4), intAddition) shouldBe 10
  }

  "A Seq" should "be brought to the the domain with a monoid" in {
    foldMap(Vector("A", "BB", "CCC"), intAddition)(_.length) shouldBe 6
  }

}