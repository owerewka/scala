package fp.design

object Delete extends App {

  println {
    "A".length + "B".length == ("A" + "B").length
  }

  val stringConcatMonoid = new Monoid[String] {
    override def op(a1: String, a2: String): String = a1 + a2
    override def zero: String = ""
  }

  val intSumMonoid = new Monoid[Int] {
    override def op(a1: Int, a2: Int): Int = a1 + a2
    override def zero: Int = 0
  }

  val len: String => Int = _.length

  println {
    intSumMonoid.op(len("A"), len("BB")) == len(stringConcatMonoid.op("A", "BB"))
  }
}
