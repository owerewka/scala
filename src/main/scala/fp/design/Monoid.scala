package fp.design

import org.scalacheck.Gen
import org.scalacheck.Prop.forAll

trait Monoid[A] {
  def op(a1: A, a2: A): A
  def zero: A
}

class ProductMonoid extends Monoid[Int] {
  override def op(a: Int, b: Int): Int = a * b
  override def zero: Int = 0
}

object Test1 {

  def list[A]: Monoid[List[A]] = new Monoid[List[A]] {
    override def op(a1: List[A], a2: List[A]): List[A] = a1 ++ a2
    override def zero: List[A] = List()
  }

  val intAddition: Monoid[Int] = new Monoid[Int] {
    override def op(a1: Int, a2: Int): Int = a1 + a2
    override def zero: Int = 0
  }

  val intMultiplication: Monoid[Int] = new Monoid[Int] {
    override def op(a1: Int, a2: Int): Int = a1 * a2
    override def zero: Int = 1
  }

  val booleanOr: Monoid[Boolean] = new Monoid[Boolean] {
    override def op(a1: Boolean, a2: Boolean): Boolean = a1 || a2
    override def zero: Boolean = false
  }

  val booleanAnd: Monoid[Boolean] = new Monoid[Boolean] {
    override def op(a1: Boolean, a2: Boolean): Boolean = a1 && a2
    override def zero: Boolean = true
  }

  val string: Monoid[String] = new Monoid[String] {
    override def op(x: String, y: String): String = x + y
    override def zero: String = ""
  }

  def option[A]: Monoid[Option[A]] = new Monoid[Option[A]] {
    override def op(a1: Option[A], a2: Option[A]): Option[A] = a1 orElse a2
    override def zero: Option[A] = None
  }
  assert(((Some(1) orElse Some(2)) orElse Some(3)).contains(1))
  assert((Some(1) orElse (Some(2) orElse Some(3))).contains(1))
  assert((Some(1) orElse None).contains(1))
  assert((None orElse Some(1)).contains(1))

  def endoMonoid[A]: Monoid[A => A] = new Monoid[A => A] {
    override def op(a1: A => A, a2: A => A): A => A = a1 compose a2
    override def zero: A => A = identity
  }
}

//this might be generalized to a test that works on all monoids
object StringMonoidTest extends App {

  val m: Monoid[String] = Test1.string

  val x = Gen.alphaStr
  val xyz = for {
    x <- Gen.alphaStr
    y <- Gen.alphaStr
    z <- Gen.alphaStr
  } yield (x, y, z)

  val laws =
    forAll(x)(x => m.op(x, x) == x + x) &&
      forAll(x)(x => m.op(x, m.zero) == m.op(m.zero, x)) &&
      forAll(xyz) { case (x, y, z) =>
        m.op(m.op(x, y), z) == m.op(x, m.op(y, z))
      }

  laws.check()

}

//noinspection DuplicatedCode
object Test3 extends App {
  import Test1._
  def concatenate[A](ls: List[A], monoid: Monoid[A]): A =
    ls.fold(monoid.zero)(monoid.op)

  concatenate(List("A", "B", "C"), string)
  concatenate(List(true, false), booleanOr)
  concatenate(List(false, false), booleanOr)
  concatenate(List(false, false), booleanAnd)
  concatenate(List(true, true), booleanAnd)

  def foldMap[A, B](as: List[A], m: Monoid[B])(f: A => B): B =
    as.map(f).fold(m.zero)(m.op)

  def foldMapI[A, B](is: IndexedSeq[A], m: Monoid[B])(f: A => B): B =
    is.iterator.map(f).fold(m.zero)(m.op)

  def foldRight[A, B](as: List[A])(z: B)(f: (A, B) => B): B = {
    val g: A => B => B = (a: A) => (b: B) => f(a, b)
    foldMap[A, B => B](as, endoMonoid[B])(g)(z)
  }
  def foldLeft[A, B](as: List[A])(z: B)(f: (B, A) => B): B = {
    val g: A => B => B = (a: A) => (b: B) => f(b, a)
    foldMap[A, B => B](as.reverse, endoMonoid)(g)(z)
  }

  println("Fold rights:")
  println(('a' to 'd').foldRight("")(
    (c: Char, str: String) => {
      println(c)
      c + str
    }))
  println(foldRight(('a' to 'd').toList)("")(
    (c: Char, str: String) => {
      println(c)
      c + str
    }))
  println("Fold lefts:")
  println(('a' to 'd').foldLeft("")(
    (str: String, c: Char) => {
      println(c)
      str + c
    }))
  println(foldLeft(('a' to 'd').toList)("")(
    (str: String, c: Char) => {
      println(c)
      str + c
    }))
}