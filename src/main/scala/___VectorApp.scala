import scala.annotation.tailrec

object Ops {

  implicit class VectorOps[A](vector: Vector[A]) {

    def mapFirst[B](f: A => B)(g: B => Boolean): Option[B] = {
      @tailrec
      def go(iterator: Iterator[A]): Option[B] =
        iterator.nextOption() match {
          case Some(a) => f(a) match {
            case Some(a: A) => Some(f(a)).filter(g)
            case _ => go(iterator)
          }
          case _ => None
        }
      go(vector.iterator)
    }
  }
}

object ___VectorApp extends App {

  import Ops.VectorOps
  println {
    Vector(1, 2, 3, 5, 8).mapFirst(_ * 2)(_ > 10)
  }
  println {
    Vector(1, 2, 3, 4, 0).mapFirst(_ * 2)(_ > 10)
  }

}

object ___DDfadf extends App {

  object Value {
    def apply(): Value.type = {
      println("Returning you the object!")
      this
    }
  }

  def parameterless: Value.type = Value

  parameterless()

}

object SSFF extends App {

  sealed trait X
  case class A(a: Int) extends X
  case class B(b: Int) extends X
  case object C extends X
  case object D extends X
  case object E extends X

  Seq(A(1), B(2), C, D, E).foreach {
    case x@(A(_) | B(_)) => println(s"$x is one of: A or B")
    case x@(C | D | E) => println(s"$x is one of: C, D, or E")
  }
}

object ASfd extends App {

  case class X(var b: Int)

  val x = X(5)
  println {
    x
  }
  x.b = 6
  println {
    x
  }

}



























