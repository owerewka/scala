package shell

import scala.sys.process._

object RunCommandApp extends App {
  println("cmd /C dir".!!)
}

object RunCommandCollectOutputApp extends App {
  val stdout = new StringBuilder
  val stderr = new StringBuilder
  val status = "java -version" ! ProcessLogger(stdout append _ append '\n', stderr append _ append '\n')
  println(status)
  println("stdout: " + stdout)
  println("stderr: " + stderr)
}

object RunGitCommandsApp extends App {

  import scala.sys.process._
  "git status" #&&
    "git add ." #&&
    "git commit -m \"Testing scala.sys.process with git.\"" #&&
    "git push" !
    ProcessLogger(
      s => println(s"OUT: $s"),
      s => println(s"ERR: $s"))

}

