package akka.actors

import actors.MonitoredActor
import akka.actor.{ActorSystem, Props}

import scala.concurrent.duration._
import scala.language.postfixOps

/**
 * Created by oskar on 28.08.15.
 */
object Run extends App {

  println("start!")

  val system = ActorSystem("ActorSystem")
  val myActor = system.actorOf(Props[MonitoredActor], "myactor2")

  import scala.concurrent.ExecutionContext.Implicits.global

  println("started...")

  var eventCounter = 0
  system.scheduler.schedule(2 seconds, 1 seconds) {
    myActor ! s"Event no$eventCounter"
    eventCounter = eventCounter + 1
  }

  Thread.sleep(100000)
  system.terminate().foreach { _ =>
    println("Actor system was shut down")
  }

}