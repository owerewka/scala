package actors

import akka.actor.{Actor, Props}


/**
  * Created by oskar on 28.08.15.
  */
object DemoActor {

  def props(magicNumber: Int): Props = Props(new DemoActor(magicNumber))

}

class DemoActor(magicNumber: Int) extends Actor {
  override def receive: Receive = {
    case x: Int => sender() ! (x + magicNumber)
  }
}

class SomeOtherActor extends Actor {

  context.actorOf(DemoActor.props(42), "demo")

  override def receive: Actor.Receive = {
    case _: Any => print(_: Any)
  }
}