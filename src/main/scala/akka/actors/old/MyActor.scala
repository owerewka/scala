package actors.old

import akka.actor.{Actor, ActorSystem, Props}

class MyActor extends Actor {
  def receive = {
    case "hello" => println("hello back at you")
    case _ => {
      println("' huh?")
    }
  }
}

class MyActorWithParameter(id: String) extends Actor {
  def receive = {
    case "hello" => println(id + " hello back at you")
    case _ => println(id + " ' huh?")
  }
}


object Main extends App {
  val system = ActorSystem("HelloSystem")
  // default Actor constructor
  val a1 = system.actorOf(Props[MyActor], name = "helloactor")
  val a2 = system.actorOf(Props(new MyActorWithParameter("25")), name = "paramactor")

  a1 ! "hello"
  a1 ! "buenos dias"
  a1 ! "cos tam"
  a1 ! "hello"

  a2 ! "hej ho"
  a2 ! "hej ho"
  a2 ! "hej ho"
  a2 ! "hej ho"
}