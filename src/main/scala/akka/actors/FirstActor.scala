package actors

import akka.actor.{Actor, Props}


class FirstActor extends Actor {

  val child = context.actorOf(Props[MonitoredActor], name = "myChild")

  override def receive: Receive = {
    case _ => println(_: Any)
  }
}