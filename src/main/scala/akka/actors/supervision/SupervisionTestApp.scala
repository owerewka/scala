package actors.supervision

import actors.supervision.SupervisionTestApp.Commands.{Die, Hello}
import akka.actor.SupervisorStrategy.{Escalate, Restart, Resume, Stop}
import akka.actor.{Actor, ActorLogging, ActorSystem, OneForOneStrategy, Props}

import scala.concurrent.duration._
import scala.language.postfixOps

object SupervisionTestApp extends App {

  class Child extends Actor with ActorLogging {
    log.info("Welcome sweet World!")

    override def receive: Receive = {
      case Hello => log.info("Received: Hej!")
      case Die => {
        log.info("I will kill myself now!")
        throw new RuntimeException("I cannot live anymore in Java world!")
      }
    }
  }

  class Supervisor extends Actor with ActorLogging {
    override val supervisorStrategy =
      OneForOneStrategy(maxNrOfRetries = 10, withinTimeRange = 1 minute) {
        case _: ArithmeticException => Resume
        case _: NullPointerException => Restart
        case _: IllegalArgumentException => Stop
        case _: RuntimeException => Restart
        case _: Exception => Escalate
      }

    val child = context.actorOf(Props[Child], name = "child")

    override def receive: Receive = {
      case Hello => {
        log.info("Forwarding to child...")
        child.forward(Hello)
      }
      case Die => child.forward(Die)
      case x: Any => log.info(x.toString)
    }
  }

  val system = ActorSystem("Test")
  val supervisor = system.actorOf(Props[Supervisor], "supervisor")
  supervisor ! Hello
  supervisor ! Die
  supervisor ! Hello
  supervisor ! Die

  object Commands {
    case object Hello
    case object Die
  }
}
