package actors

import java.util.concurrent.TimeUnit

import akka.actor.Actor
import akka.event.Logging
import com.codahale.metrics.{ConsoleReporter, MetricRegistry}

case class Greeting(from: String)

case object Goodbye

class MonitoredActor extends Actor {

  val log = Logging(context.system, this)
  val metricRegistry = new MetricRegistry()

    val reporter = ConsoleReporter.forRegistry(metricRegistry)
      .convertRatesTo(TimeUnit.SECONDS)
      .convertDurationsTo(TimeUnit.MILLISECONDS)
      .outputTo(System.out)
      .build()
    reporter.start(10, TimeUnit.SECONDS)

  override def receive: Receive = {
    case m: Any => {
      metricRegistry.counter("txcount").inc()
      val duration = metricRegistry.timer("elapsedTime").time()
      m match {
        case Greeting(greeter) => log.info(s"Received message from $greeter");
        case Goodbye => log.info("Receiver Goodbye!")
        case s: String => log.info(s"Received '$s', hey don't send me strings!")
        case _ => log.info("Received unknown message")
      }
      duration.stop()
    }
  }
}
