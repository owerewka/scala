package akka.actors

trait TimeSource {
  def getTime: Long
}

trait Scheduler {
  def scheduleAt(time: Long, task: () => Unit)

  def reset()
}

case class AlarmClock(timeSource: TimeSource,
                      scheduler: Scheduler,
                      alarmAction: () => Unit,
                      private val alarmTime: Option[Long] = None,
                      private val alarmEnabled: Boolean = false) {

  (alarmEnabled, alarmTime) match {
    case (true, Some(t)) => scheduler.scheduleAt(t, alarmAction)
    case _ => println("Something went wrong!")
  }

  def enableAlarm(): AlarmClock = {
    alarmTime.foreach(t => scheduler.scheduleAt(t, alarmAction))
    copy(alarmEnabled = true)
  }

  def disableAlarm(): AlarmClock = {
    scheduler.reset()
    copy(alarmEnabled = false)
  }

  def setAlarmTime(newAlarmTime: Long): AlarmClock = {
    if (alarmEnabled) {
      scheduler.reset()
      scheduler.scheduleAt(newAlarmTime, alarmAction)
    }
    copy(alarmTime = Some(newAlarmTime))
  }

  def getAlarmTime = alarmEnabled match {
    case false => None
    case true => alarmTime
  }

  def getTime: Long = timeSource.getTime
}





