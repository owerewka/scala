package akka.stream

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.Source

object SimpleMapping extends App {

  implicit val system = ActorSystem.create("akka-stream")
  implicit val materializer = ActorMaterializer()

  val source: Source[Int, NotUsed] = Source(1 to 100)
  source
    .grouped(5)
    .map(x => x.map(_ * 3))
    .filter(x => x.foldLeft(0)(_ + _) % 2 == 0)
    .runForeach(i => println(i))(materializer)


  //  system.terminate()
}
