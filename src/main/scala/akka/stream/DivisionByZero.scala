package akka.stream

import akka.Done
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}
import scala.concurrent.ExecutionContext.Implicits.global


import scala.concurrent.Future

object DivisionByZero extends App {

  val decider: Supervision.Decider = {
    case x =>
      println(s"Wyjebalo sie: $x")
      x match {
        case _: ArithmeticException => Supervision.Resume
        case _ => Supervision.Stop
      }
  }

  implicit val system = ActorSystem.create("akka-stream")
  implicit val materializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(decider))

  val completetion: Future[Int] =
    Source(-1 to 5)
      .map(10 / _)
      .map(x => {
        println(x)
        x
      })
      .runWith(Sink.fold(0)(_ + _))

  completetion.onComplete(sum => {
    print(sum);
    system.terminate()
  })
}
