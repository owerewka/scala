package akka.http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.{ContentTypes, HttpEntity, HttpMethods, HttpRequest}

import java.net.URLEncoder
import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.language.postfixOps

object Test extends App {

  implicit val system = ActorSystem()

  import system.dispatcher

  val source =
    """
      |object SimpleApp {
      |  val aField = 2
      |
      |  def aMethod(x: Int) = x + 1
      |
      |  def main(args: Array[String]) = {
      |    println(aMethod(aField))
      |  }
      |}""".stripMargin

  def highlightCode(myCode: String): Future[String] = {
    val responseFuture = Http().singleRequest(
      HttpRequest(
        method = HttpMethods.POST,
        uri = "http://markup.su/api/highlighter",
        entity = HttpEntity(
          ContentTypes.`application/x-www-form-urlencoded`,
          s"source=${URLEncoder.encode(myCode.trim, "UTF-8")}&language=Scala&theme=Sunburst"
        )
      )
    )

    responseFuture
      .flatMap(_.entity.toStrict(2 seconds))
      .map(_.data.utf8String)
  }


  highlightCode(source).map(println)
  //system.terminate()
}
