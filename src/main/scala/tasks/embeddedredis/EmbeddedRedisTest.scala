package tasks.embeddedredis

import java.lang.System.currentTimeMillis

import redis.clients.jedis.Jedis
import redis.embedded.RedisServer
import tasks.embeddedredis.Time.duration


object EmbeddedRedisTest extends App {

  val redisServer = new RedisServer(6379)
  redisServer.start()
  println("started")

  val jedis = new Jedis("localhost")
  duration {
    val t1 = currentTimeMillis()
    (1 to 100).foreach(i => jedis.set(s"key$i", s"value$i"))
    (0 to 101).foreach(i => println(jedis.get(s"key$i")))
  }
  redisServer.stop
  println("stopped")
}

object Time {

  def duration(block: => Unit): Unit = {
    val t1 = currentTimeMillis()
    block //call by name
    val t2 = currentTimeMillis()
    println(s"Elapset time: ${(t2 - t1)}ms")
  }

}