import scala.annotation.tailrec

sealed trait Op
case class Add(v: Int) extends Op
case class Mul(v: Int) extends Op
case class Sub(v: Int) extends Op

object Op extends App {

  println {
    res(1, List(Add(2), Mul(2), Sub(3)))
  }

  @tailrec
  def res(start: Int, ops: List[Op]): Int = ops match {
    case Nil => start
    case h :: t => h match {
      case Add(v) => res(start + v, t)
      case Mul(v) => res(start * v, t)
      case Sub(v) => res(start - v, t)
    }
  }

}










